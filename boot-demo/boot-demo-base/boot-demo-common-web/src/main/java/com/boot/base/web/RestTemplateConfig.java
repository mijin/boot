package com.boot.base.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RestTemplateConfig {

    private Map<String, RestTemplate> map = new HashMap<>();

    /**根据值去获取restTemplate
     * @param prov
     * @return
     */
    public RestTemplate getRestTemplate(String prov) {
        if (!map.containsKey(prov)) {
            RestTemplate template = new RestTemplate();
            map.put(prov, template);
        }
        return map.get(prov);
    }
}
