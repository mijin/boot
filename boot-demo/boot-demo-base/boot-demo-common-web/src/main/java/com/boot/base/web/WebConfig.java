package com.boot.base.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class WebConfig {

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate template = new RestTemplate();
        template.getMessageConverters().set(1,new StringHttpMessageConverter(java.nio.charset.StandardCharsets.US_ASCII));
        return new RestTemplate();
    }

}
