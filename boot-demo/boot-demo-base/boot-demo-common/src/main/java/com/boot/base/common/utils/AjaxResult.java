package com.boot.base.common.utils;

import java.util.HashMap;
import java.util.Objects;

/**
 * 统一结果集
 *
 * @author zongb
 */
public class AjaxResult extends HashMap<String, Object> {
    private static final long serialVersionUID = 123567890L;

    private static final String CODE_TAG = "code";

    private static final String MSG_TAG = "msg";

    private static final String DATA_TAG = "data";

    /**
     * 状态类型
     */
    public enum Type {
        /**
         * 成功
         */
        SUCCESS(0),
        /**
         * 错误
         */
        ERROR(1);
        private final int value;

        Type(int value) {
            this.value = value;
        }

        public int value() {
            return this.value;
        }
    }

    /**
     * 状态类型
     */
    private Type type;

    /**
     * 状态码
     */
    private int code;

    /**
     * 消息
     */
    private String msg;

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public AjaxResult() {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param type 状态类型
     * @param msg  消息
     */
    public AjaxResult(Type type, String msg) {
        super.put(CODE_TAG, type.value);
        super.put(MSG_TAG, msg);
    }

    /**
     * 判断是否成功
     *
     * @return
     */
    public boolean isSuccess() {
        return this.code == Type.SUCCESS.value && Objects.equals(this.get(CODE_TAG), Type.SUCCESS.value);
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static AjaxResult success() {
        return AjaxResult.success("操作成功");
    }

    /**
     * 返回成功消息
     *
     * @param msg 消息
     * @return 成功消息
     */
    public static AjaxResult success(String msg) {
        return new AjaxResult(Type.SUCCESS, msg);
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static AjaxResult error() {
        return AjaxResult.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 消息
     * @return 错误消息
     */
    public static AjaxResult error(String msg) {
        return new AjaxResult(Type.ERROR, msg);
    }


    /**
     * 根据count来计算操作结果
     *
     * @param count 大于0操作成功 否则操作失败
     * @return
     */
    public static AjaxResult ofCount(int count) {
        return count > 0 ? success() : error();
    }

    /**
     * 添加额外的数据
     *
     * @param key   key值
     * @param value value值
     */
    public AjaxResult add(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }



}

