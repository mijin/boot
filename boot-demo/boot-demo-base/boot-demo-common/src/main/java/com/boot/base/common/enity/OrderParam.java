package com.boot.base.common.enity;

import lombok.Builder;
import lombok.Data;

/**
 * 公共的订购参数
 * @author zongbo1
 */
@Builder
@Data
public class OrderParam {

    /**
     * 流水id
     */
    private Long seqId ;
    /**
     * 活动
     */
    private Long actId ;
    /**
     * 省份代码
     */
    private String provCode ;
    /**
     * 手机号
     */
    private String phone ;
    /**
     * 订购业务类型，用来区分订购业务类型，无实际意义
     * {@link OrderFromEnum#getCode()}
     */
    private String orderFrom;

    /**
     * 当前系统的产品id
     */
    private Long productId;

    /**
     * 当前系统的生效类型
     * 1-立即生效(永久有效)
     * 2-立即生效(本月有效)
     * 3-次月生效(永久有效)
     * 4-次月生效(本月有效)
     */
    private Integer effectiveType;

    /**
     * 订购编码，连接本系统与省份订购系统的标志。
     */
    private String purchaseCode;







}
