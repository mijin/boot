package com.boot.base.common.enity;

import lombok.Data;

import java.util.List;

@Data
public class TPageInfo<T> {
    private Integer pageNum;
    private Integer pageSize;
    private Integer total;
    private List<T> data;
}
