package com.boot.base.common.enity.video;

import lombok.Data;

/**
 * 视频列表
 */
@Data
public class VideoListDo {
    private Integer vod_id;
    private String vod_name;
    private String vod_tag;
    private String vod_class;
    private String vod_pic;
    private String vod_actor;
    private String vod_play_server;
    private String vod_play_url;
    private String vod_director;
    private String vod_area;
    private String vod_content;
    private String index_type_id;
    private String vod_pic_thumb;
    private String vod_score;
    private Integer vip_video_status;
    private String vod_pic_slide;
}
