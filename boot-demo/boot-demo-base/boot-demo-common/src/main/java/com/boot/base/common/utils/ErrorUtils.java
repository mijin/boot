package com.boot.base.common.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class ErrorUtils {
    /**
     * 获取异常的堆栈信息
     * @param t
     * @return
     */
    public static String getErrorMsg(Throwable t){
        StringWriter stringWriter=new StringWriter();
        t.printStackTrace(new PrintWriter(stringWriter,true));
        return stringWriter.getBuffer().toString();
    }
}
