package com.boot.base.common.newbanker.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * JDBC配置
 * 
 * @author LiuXianfa
 * @email xianfaliu@newbanker.cn
 * @date 2018-03-30 10:10:44
 */
@Getter
@Setter
//@TableName("wbs_jdbc")
public class WbsJdbcEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Integer id;
	/**
	 * 所属系统编码,默认为wbs
	 */
    private String systemCode;

	/**
	 * 企业Id
	 */
	private Integer entid;
    /**
     * 企业全称
     */
    private String entfullname;
	/**
	 * url
	 */
	private String url;
	/**
	 * 账号
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 驱动类
	 */
	private String driverclassname;
	/**
	 * 创建时间
	 */
	private Date createtime;

    /**
     * initialSize
     */
    private Integer initialsize;
    /**
     * maxActive
     */
    private Integer maxactive;
    /**
     * maxIdle
     */
    private Integer maxidle;
    /**
     * minIdle
     */
    private Integer minidle;
    /**
     * maxWait
     */
    private Integer maxwait;
    /**
     * poolPreparedStatements(false,true)
     */
    private String poolpreparedstatements;
    /**
     * maxOpenPreparedStatements
     */
    private String maxopenpreparedstatements;
    /**
     * validationQuery
     */
    private String validationquery;
    /**
     * testOnBorrow
     */
    private String testonborrow;
    /**
     * testOnReturn
     */
    private String testonreturn;
    /**
     * testWhileIdle
     */
    private String testwhileidle;
    /**
     * timeBetweenEvictionRunsMillis
     */
    private String timebetweenevictionrunsmillis;
    /**
     * minEvictableIdleTimeMillis
     */
    private String minevictableidletimemillis;
    /**
     * connectionInitSqls
     */
    private String connectioninitsqls;
    /**
     * exceptionSorter
     */
    private String exceptionsorter;
    /**
     * filters
     */
    private String filters;
    /**
     * proxyFilters
     */
    private String proxyfilters;

    /**
     * 当前配置项和db中重复时，是否强制修改
     */
    private boolean forceUpdate;

    /**
     * 输入sql语句，执行sql
     */
    private String sql;
}
