package com.boot.base.common.utils;

import java.io.File;

public abstract class DirectoryUtil {
    public static void createFilePath(String filePath) {
        createFile(new File(filePath));
    }

    public static void createFile(File file) {
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
    }
}
