package com.boot.base.common.newbanker.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 项目
 *
 * @author LiuXianfa
 * @email xianfaliu@newbanker.cn
 * @date 2018-09-10 16:53:53
 */
@Data
public class CcProjectEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Integer id;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 项目编码
     */
    private String projectCode;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建人姓名
     */
    private String createdByUsername;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Integer updatedBy;

    /**
     * 修改人姓名
     */
    private String updatedByUsername;

    /**
     * 修改时间
     */
    private Date updateTime;


}
