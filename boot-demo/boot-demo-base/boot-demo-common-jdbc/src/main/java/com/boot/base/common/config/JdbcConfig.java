package com.boot.base.common.config;

import com.boot.base.common.jdbc.TJdbcTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JdbcConfig {

    @Bean
    public TJdbcTemplate jdbcTemplate(DataSource dataSource){
        return new TJdbcTemplate(dataSource);
    }

}
