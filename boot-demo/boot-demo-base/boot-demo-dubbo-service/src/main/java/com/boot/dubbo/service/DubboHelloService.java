package com.boot.dubbo.service;

public interface DubboHelloService {

    /**
     * 操作内容
     * @param content
     * @return
     */
    String say(String content);
}
