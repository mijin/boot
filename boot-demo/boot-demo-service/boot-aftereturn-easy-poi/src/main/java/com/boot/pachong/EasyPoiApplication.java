package com.boot.pachong;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class EasyPoiApplication {



    public static void main(String[] args) {
        SpringApplication.run(EasyPoiApplication.class, args);
        log.info(EasyPoiApplication.class.getName()+"...");
    }
}
