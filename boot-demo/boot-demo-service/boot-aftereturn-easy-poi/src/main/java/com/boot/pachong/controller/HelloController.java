package com.boot.pachong.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.handler.inter.IExcelExportServer;
import com.boot.pachong.domain.MsgClient;
import com.boot.pachong.domain.MsgClientGroup;
import com.boot.pachong.util.TFileUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
public class HelloController {
    /**
     * 导出excel
     *
     * @return
     */
    @RequestMapping("/output")
    public void output(HttpServletRequest request, HttpServletResponse response) {
        long startTime = System.currentTimeMillis();
        Workbook workbook = ExcelExportUtil.exportBigExcel(new ExportParams(), MsgClient.class, new IExcelExportServer() {
            @Override
            public List<Object> selectListForExcelExport(Object obj, int page) {
                if (((int) obj) == page) {
                    return null;
                }
                List<Object> list = new ArrayList<Object>();
                for (int i = 0; i < 100000; i++) {
                    MsgClient client = new MsgClient();
                    client.setBirthday(new Date());
                    client.setClientName("小明" + i);
                    client.setClientPhone("18797" + i);
                    client.setCreateBy("JueYue");
                    client.setId("1" + i);
                    client.setRemark("测试" + i);
                    MsgClientGroup group = new MsgClientGroup();
                    group.setGroupName("测试" + i);
                    client.setGroup(group);
                    list.add(client);
                }
                return list;
            }
        }, 2);

        TFileUtils.downExcelFile(workbook,"订购详情",response);
        log.info("耗时约 {}ms", System.currentTimeMillis() - startTime);
    }
}
