package com.boot.pachong.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户分组表
 * 
 * @author yjl
 * 
 */
@Data
public class MsgClientGroup implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6946265640897464878L;

    // 组名
    @Excel(name = "分组")
    private String            groupName        = null;
    /**
     * 创建人
     */
    private String            createBy;

}
