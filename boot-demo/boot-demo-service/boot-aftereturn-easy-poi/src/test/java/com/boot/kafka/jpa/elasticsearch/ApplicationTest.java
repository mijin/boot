package com.boot.kafka.jpa.elasticsearch;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.handler.inter.IExcelExportServer;
import com.boot.pachong.domain.MsgClient;
import com.boot.pachong.domain.MsgClientGroup;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
//@SpringBootTest
public class ApplicationTest {

    @Test
    public void test2() throws IOException {
        Workbook workbook = ExcelExportUtil.exportBigExcel(new ExportParams(), MsgClient.class, new IExcelExportServer() {
            @Override
            public List<Object> selectListForExcelExport(Object obj, int page) {
                if (((int) obj) == page) {
                    return null;
                }
                List<Object> list = new ArrayList<Object>();
                for (int i = 0; i < 1048575; i++) {
                    MsgClient client = new MsgClient();
                    client.setBirthday(new Date());
                    client.setClientName("小明" + i);
                    client.setClientPhone("18797" + i);
                    client.setCreateBy("JueYue");
                    client.setId("1" + i);
                    client.setRemark("测试" + i);
                    MsgClientGroup group = new MsgClientGroup();
                    group.setGroupName("测试" + i);
                    client.setGroup(group);
                    list.add(client);
                }
                return list;
            }
        }, 2);
        File f = new File("D:/file/a.xlsx");
        FileOutputStream outputStream = new FileOutputStream(f);
        workbook.write(outputStream);
        outputStream.close();
     }

}
