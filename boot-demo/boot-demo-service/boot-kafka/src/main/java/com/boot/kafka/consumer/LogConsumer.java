package com.boot.kafka.consumer;

import com.boot.base.kafka.contest.KafkaConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LogConsumer {
    @KafkaListener(topics = KafkaConstant.TEST, groupId = "test-consumer-group2")
    public void log(ConsumerRecord<String, String> consumerRecord) {
        read1(consumerRecord);
    }
    @KafkaListener(topics = KafkaConstant.TEST, groupId = "test-consumer-group1")
    public void log2(ConsumerRecord<String, String> consumerRecord) {
        read1(consumerRecord);
    }

    public void read1(ConsumerRecord<String, String> consumerRecord) {
        log.info("kafka打印的信息如下：");
        //主题
        String topic = consumerRecord.topic();
        String key = consumerRecord.key();
        //消息内容
        String valus = consumerRecord.value();
        //分区
        int partition = consumerRecord.partition();
        //时间戳
        long timestamp = consumerRecord.timestamp();
        log.info("topic:" + topic);
        log.info("key:" + key);
        log.info("values:" + valus);
        log.info("partition:" + partition);
        log.info("timestamp:" + timestamp);
    }
}
