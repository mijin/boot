package com.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkApplication.class, args);
    }
}
