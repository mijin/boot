package com.boot.kafka.consumer;

import com.boot.base.kafka.contest.KafkaConstant;
import com.boot.base.common.utils.DownUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j
@Component
public class ImgConsumer {
    /**下载图片
     * @param consumerRecord
     */
    @KafkaListener(topics = KafkaConstant.IMG_TOPIC, groupId = "test-consumer-group2")
    public void consumer(ConsumerRecord<String, String> consumerRecord) {
        boolean flag = false;
        final long start = System.currentTimeMillis();
        final String value = consumerRecord.value();
        final String dir = (LocalDateTime.now().getMinute() + "").substring(0, 1);
        final String name = UUID.randomUUID().toString();
        try {
//            DownUtils.download(value, String.format("/data3/imgs/%s/%s.jpg",dir,name));
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("{}通道消费图片 {},状态{},图片名称{},耗时约 {}ms",KafkaConstant.IMG_TOPIC,value,flag,name,System.currentTimeMillis()-start);
    }


}
