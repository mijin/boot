package com.boot.kafka.task;

import com.boot.base.kafka.contest.KafkaConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppRun implements ApplicationRunner {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private JdbcTemplate localJdbc;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        final List<String> strings = localJdbc.queryForList("SELECT  img_url  FROM  picxxxx_pics   limit 3;", String.class);
        for (int index = 0; index < strings.size(); index++) {
            final String s = strings.get(index);
            kafkaTemplate.send(KafkaConstant.IMG_TOPIC, s);
        }
    }
}
