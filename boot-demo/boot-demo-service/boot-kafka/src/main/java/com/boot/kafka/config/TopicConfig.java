package com.boot.kafka.config;

import com.boot.base.kafka.contest.KafkaConstant;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

/**
 * spring-kafka默认会自动创建topic，如果需要自定义replicas、partitions，可以在这里定义
 *
 * @author zongbo1
 */
//@Configuration
public class TopicConfig {

    @Bean
    public NewTopic test() {
        return TopicBuilder.name(KafkaConstant.TEST)
                .replicas(1)
                .partitions(1)
                .build();
    }
}
