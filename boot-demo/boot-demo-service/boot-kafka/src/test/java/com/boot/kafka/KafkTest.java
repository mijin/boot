package com.boot.kafka;


import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Test;

import java.time.Duration;
import java.util.*;

@Slf4j
public class KafkTest {




    @Test
    public void test01() {
        //        1.创建kafka生产者信息
        Properties props = new Properties();
//kafka 集群，broker-list
        props.put("bootstrap.servers", "localhost:39005");
//        ack应答级别
        props.put("acks", "all");
        //重试次数
        props.put("retries", 3);
        //批次大小
        props.put("batch.size", 16384);
        //等待时间
        props.put("linger.ms", 1);

        //RecordAccumulator 缓冲区大小
        props.put("buffer.memory", 33554432);
//        序列化
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

//        发送数据
        for (int index = 0; index < 10; index++) {
            String val = "atguigu---" + index;
            log.info(val );
            producer.send(new ProducerRecord<String, String>(this.getClass().getName(), val), new Callback() {
                @Override
                public void onCompletion(RecordMetadata metadata, Exception exception) {
                    log.info("topic:{},offset:{},partition:{},timestamp:{}",metadata.topic(),metadata.offset(),metadata.partition(),metadata.timestamp());
                }
            });
        }
        producer.close();

    }


    @Test
    public void test02() {
        Properties props = new Properties();

        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:39005");
//        开启自动提交
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);
//        自动提交延时
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG,"1000");


        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

//消费者组
        props.put(ConsumerConfig.GROUP_ID_CONFIG,"bigdata");


        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);

//        订阅主题
        consumer.subscribe(Collections.singletonList(this.getClass().getName()));

        Map<String, List<PartitionInfo>> stringListMap = consumer.listTopics();

        ConsumerRecords<String, String> poll = consumer.poll(Duration.ofSeconds(10));

        for (ConsumerRecord<String, String> record : poll) {
            log.info(record.value());
        }


    }


}