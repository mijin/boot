package com.boot.kafka;


import cn.hutool.core.util.ZipUtil;
import com.boot.base.common.enity.video.VideoListDo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j

public class KafkUtilsTest {


    @Test
    public void test01() {
        final File file = new File("/data3/imgs");
//        final File file = new File("/hpegit");
        final File[] files = file.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                ZipUtil.zip(f.getPath(), ("/data3/zips/" + f.getName() + ".zip"));
                log.info("{} 压缩完成", f.getName());
            }
        }
    }

    @Test
    public void test02() {
        PropertyDescriptor[] descriptors = BeanUtils.getPropertyDescriptors(VideoListDo.class);
        List<String> fields = new ArrayList<>(descriptors.length);
        for (PropertyDescriptor descriptor : descriptors) {
            final String field = String.format("`%s` varchar(1000) DEFAULT NULL,", descriptor.getName());
            fields.add(field);
         }
        final String join = String.join("\n", fields.toArray(new String[]{}));
        log.info(join);
    }

}