package com.boot.kafka;


import com.boot.KafkApplication;
import com.boot.base.kafka.contest.KafkaConstant;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = KafkApplication.class)
public class KafkApplicationTest {


    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private JdbcTemplate localJdbc;

    @Test
    public void test01() {
        for (int index = 0; index < 10; index++) {
            kafkaTemplate.send(KafkaConstant.TEST, "cc0000");
            log.info("{}", index);
        }
    }

    @Test
    public void test02() {
        final List<String> strings = localJdbc.queryForList("SELECT  img_url  FROM  picxxxx_pics  ;", String.class);
        for (int index = 0; index < strings.size(); index++) {
            final String s = strings.get(index);
            kafkaTemplate.send(KafkaConstant.IMG_TOPIC, s);
            log.info("第{}张图片");
        }

    }
}