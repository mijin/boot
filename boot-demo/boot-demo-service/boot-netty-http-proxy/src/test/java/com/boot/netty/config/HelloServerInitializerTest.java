package com.boot.netty.config;

import com.boot.netty.handler.ToServerHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import junit.framework.TestCase;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Log
public class HelloServerInitializerTest {


    /**
     * 本地文件写数据
     */
    @Test
    public void chanel01() throws IOException {
        String str = "Abc@1234";

//        创建一个输出流
        final FileOutputStream fileOutputStream = new FileOutputStream("d://a.txt");
//        通过输出流获取文件chanel
        final FileChannel channel = fileOutputStream.getChannel();

//        创建一个缓冲区
        final ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

        final ByteBuffer put = byteBuffer.put(str.getBytes());

//      对buffer flip
        byteBuffer.flip();

        channel.write(byteBuffer);

//        关闭流
        fileOutputStream.close();

        log.info("写入完成");
    }

    @Test
    public void chanel02() throws IOException, InterruptedException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.remoteAddress("127.0.0.1",8085);
        bootstrap.group(new NioEventLoopGroup())
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new ChannelInitializer() {
                    @Override
                    protected void initChannel(Channel channel) throws Exception {
                        channel.pipeline().addLast("toServerhandler", new ChannelInboundHandlerAdapter(){
                        });
                    }
                })
                .bind(6666)
                .sync();

    }


    /**获取目标channel
     * @return
     */
    Channel getTargetChannel(){
        return null;
    }
}