package com.boot.netty.cache;

import io.netty.channel.Channel;

import java.util.HashMap;

public class ChannelCache extends HashMap<Channel, Channel> {


    /**
     * 操作二级代理
     *
     * @param key
     * @return
     */
    @Override
    public Channel get(Object key) {
        Channel channel = super.get(key);
        return channel;
    }
}
