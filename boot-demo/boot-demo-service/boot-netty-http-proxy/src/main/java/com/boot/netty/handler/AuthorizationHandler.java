package com.boot.netty.handler;

public abstract class AuthorizationHandler {
    private static boolean initflag = false;
    private static String ausername;
    private static String apassword;


    /**
     * @param username
     * @param password
     */
    public static void init(String username, String password) {
        if (initflag) {
            throw new RuntimeException("已经初始化过");
        } else {
            AuthorizationHandler.ausername = username;
            AuthorizationHandler.apassword = password;
            initflag=true;
        }
    }


    /**
     * 校验
     *
     * @param username
     * @param password
     * @return
     */
    public static boolean authorization(String username, String password) {
        return (apassword.equals(username) && apassword.equals(password));
    }

    public static boolean authorization(String usernameAndPassword) {
        String[] split = usernameAndPassword.split(":");
        return authorization(split[0], split[0]);
    }

    public static boolean needAuthorization() {
         return initflag;
    }



}
