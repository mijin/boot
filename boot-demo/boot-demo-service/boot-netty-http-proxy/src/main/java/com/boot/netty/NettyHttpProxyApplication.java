package com.boot.netty;

import com.boot.netty.handler.AuthorizationHandler;
import com.boot.netty.handler.NettyProxyServerHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Base64;
import java.util.Properties;

@Slf4j
public class NettyHttpProxyApplication {

    private static int PORT = 8081;
    private static boolean PROXY_FLAG = false;
    private static String username;
    private static String password;

    public static void main(String[] args) throws InterruptedException {
//        端口获取  优先从main参数里面取，然后再就是从环境变量里面取
        if (args != null && args.length > 0) {
            System.setProperty("PORT", args[0]);
        }

        PORT = Integer.valueOf(System.getProperty("PORT", PORT + ""));


        if (System.getProperty("username") != null && System.getProperty("password") != null) {
            AuthorizationHandler.init(System.getProperty("username"), System.getProperty("password"));
        }

        log.info("启动端口是:{},userneme,password", PORT,System.getProperty("username"),System.getProperty("password"));
        if (PROXY_FLAG) {
            http();
        } else {
            https();
        }
    }

    /**
     * 支持https代理
     */
    private static void https() throws InterruptedException {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup(8);
        NioEventLoopGroup workGroup = new NioEventLoopGroup(8);
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel channel) throws Exception {
                        channel.pipeline().addLast(new NettyProxyServerHandler());
                    }
                })
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .bind(PORT)
                .sync();

    }


    /**
     * 这里支支持http
     *
     * @throws InterruptedException
     */
    private static void http() throws InterruptedException {
        new ServerBootstrap()
                .group(new NioEventLoopGroup(), new NioEventLoopGroup(2))
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 100)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel ch) throws Exception {
                        ch.pipeline().addLast("httpCodec", new HttpServerCodec());
                        ch.pipeline().addLast("httpObject", new HttpObjectAggregator(65536));
                        ch.pipeline().addLast("serverHandle", new ChannelInboundHandlerAdapter() {

                            private String host;
                            private int port = 80;

                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                                只处理http
                                if (msg instanceof FullHttpRequest) {
                                    FullHttpRequest request = (FullHttpRequest) msg;
                                    host = request.headers().get("host");
                                    final String s = NettyHttpProxyApplication.authenticate(request.headers().get(HttpHeaderNames.PROXY_AUTHORIZATION));
                                    String[] temp = host.split(":");
                                    if (temp.length > 1) {
                                        port = Integer.parseInt(temp[1]);
                                    } else {
                                        if (request.uri().indexOf("https") == 0) {
                                            port = 443;
                                        }
                                    }

                                    log.info("$ curl -i -x $host:$port -U {} {}", s, request.uri());
                                    if ("CONNECT".equalsIgnoreCase(request.method().name())) {//HTTPS建立代理握手
                                        HttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
                                        ctx.writeAndFlush(response);
                                        ctx.pipeline().remove("httpCodec");
                                        ctx.pipeline().remove("httpObject");
                                        return;
                                    }

//连接到目标服务器
                                    new Bootstrap()
                                            .group(ctx.channel().eventLoop())
                                            .channel(ctx.channel().getClass())
                                            .handler(new HttpProxyInitializer(ch))
                                            .connect(host, port)
                                            .addListener(new ChannelFutureListener() {
                                                @Override
                                                public void operationComplete(ChannelFuture future) throws Exception {
                                                    if (future.isSuccess()) {
                                                        future.channel().writeAndFlush(msg);
                                                    } else {
                                                        ctx.channel().close();
                                                    }
                                                }
                                            });

                                }
                            }
                        });
                    }
                })
                .bind(PORT)
                .sync()
                .channel()
                .closeFuture()
                .sync();
    }

    public static final String AUTH_TYPE_BASIC = "Basic";

    public static String authenticate(String authorization) {
        String usr = "";
        String pwd = "";
        if (authorization != null && authorization.length() > 0) {
            String token = authorization.substring(AUTH_TYPE_BASIC.length() + 1);
            String decode = new String(Base64.getDecoder().decode(token));
            String[] arr = decode.split(":");
            usr = arr[0];
            if (arr.length >= 2) {
                pwd = arr[1];
            }
        }
        return String.join(":", usr, pwd);
    }

    public static class HttpProxyInitializer extends ChannelInitializer {

        private Channel clientChannel;

        public HttpProxyInitializer(Channel clientChannel) {
            this.clientChannel = clientChannel;
        }

        @Override
        protected void initChannel(Channel ch) throws Exception {
            ch.pipeline().addLast(new HttpClientCodec());
            ch.pipeline().addLast(new HttpObjectAggregator(6553600));
            ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                @Override
                public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                    FullHttpResponse response = (FullHttpResponse) msg;
                    //修改http响应体返回至客户端
                    response.headers().add("proxy", "netty");
                    clientChannel.writeAndFlush(msg);
                }
            });
        }
    }

}
