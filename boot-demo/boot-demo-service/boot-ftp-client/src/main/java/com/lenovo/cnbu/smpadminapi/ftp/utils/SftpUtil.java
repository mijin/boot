package com.lenovo.cnbu.smpadminapi.ftp.utils;

import com.jcraft.jsch.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 * 实现在linux 系统之间下载文件（通过sftp协议）
 *
 * @author zongbo
 */
public class SftpUtil {

    Session session = null;
    ChannelSftp channel = null;

    private SftpUtil() {
        // TODO Auto-generated constructor stub
    }

    /**
     * 连接sftp服务器
     *
     * @param host     远程主机ip地址
     * @param user     用户名
     * @param password 密码
     * @return
     * @throws JSchException
     */
    public static SftpUtil connect(String host, String user, String password) throws JSchException {
        return connect(host, null, user, password);
    }

    /**
     * 连接sftp服务器
     *
     * @param host     远程主机ip地址
     * @param port     sftp连接端口，null 时为默认端口
     * @param user     用户名
     * @param password 密码
     * @return
     * @throws JSchException
     */
    public static SftpUtil connect(String host, Integer port, String user, String password) throws JSchException {
        SftpUtil sftp = new SftpUtil();
        try {
            JSch jsch = new JSch();
            if (port != null) {
                sftp.session = jsch.getSession(user, host, port.intValue());
            } else {
                sftp.session = jsch.getSession(user, host);
            }
            sftp.session.setUserInfo(new UserInfo() {

                public String getPassphrase() {
                    return null;
                }

                public String getPassword() {
                    return null;
                }

                public boolean promptPassword(String arg0) {
                    return false;
                }

                public boolean promptPassphrase(String arg0) {
                    return false;
                }

                public boolean promptYesNo(String arg0) {
                    return true;// notice here
                }

                public void showMessage(String arg0) {
                }

            });

            sftp.session.setPassword(password);
            // 设置第一次登陆的时候提示，可选值:(ask | yes | no)
            sftp.session.setConfig("StrictHostKeyChecking", "no");
            // 10秒连接超时
            sftp.session.connect(10000);
            // 打开sftp通道
            sftp.openChannel();
        } catch (JSchException e) {
            System.out.println("SftpAccess 获取连接发生错误:");
            e.printStackTrace();
            throw e;
        }
        return sftp;
    }

    /**
     * 打开sftp通道
     *
     * @return
     * @throws JSchException
     */
    private SftpUtil openChannel() throws JSchException {
        try {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
        } catch (JSchException e) {
            // TODO Auto-generated catch block
            System.out.println("SftpAccess 打开sftp通道时发生错误:");
            e.printStackTrace();
            throw e;
        }
        return this;
    }

    /**
     * 下载远程单个文件
     *
     * @param srcFile 远程服务器文件路径（包括文件名）
     * @param os      输出流
     */
    public SftpUtil downloadFile(String srcFile, OutputStream os) {
        try {
            channel.get(srcFile, os);
        } catch (SftpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return this;
    }


    public String downloadFile(String srcFile) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            channel.get(srcFile, out);
        } catch (SftpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            try {
                out.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return new String(out.toByteArray());
    }

    /**
     * 下载远程单个文件
     *
     * @param srcFile        远程服务器文件路径（包括文件名）
     * @param localDirectory 本地文件目录
     */
    public SftpUtil downloadFile(String srcFile, String localDirectory) {
        return downloadFile(srcFile, localDirectory, null);
    }

    /**
     * 下载远程单个文件
     *
     * @param srcFile        远程服务器文件路径（包括文件名）
     * @param localDirectory 本地文件目录
     * @param newFileName    本地新文件名,如果为null，则使用源文件名
     */
    public SftpUtil downloadFile(String srcFile, String localDirectory, String newFileName) {

        // 创建本地文件夹
        File file = new File(localDirectory);
        if (!file.exists()) {
            file.mkdir();
        }

        String fileName = srcFile.substring(srcFile.lastIndexOf(File.separator) + 1, srcFile.length());

        String localFile = "";// 计算文件名
        if (localDirectory.endsWith(File.separator)) {
            localFile = localDirectory;
        } else {
            localFile = localDirectory + File.separator;
        }

        if (newFileName == null || "".equals(newFileName.trim())) {
            localFile += fileName;// 使用源文件名
        } else {
            localFile += newFileName;// 使用自定义文件名
        }
        // System.out.println("dowloading "+ fileName);

        try {
            // 下载文件
            channel.get(srcFile, localFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }


    /**
     * 上传单个文件
     *
     * @param localFile       要上传的文件（包括文件名）
     * @param remoteDirectory 远程文件夹目录
     * @throws Exception
     */
    public SftpUtil uploadFile(String localFile, String remoteDirectory) throws Exception {
        return uploadFile(localFile, remoteDirectory, null);
    }

    /**
     * 上传单个文件
     *
     * @param localFile       要上传的文件（包括文件名）
     * @param remoteDirectory 远程文件夹目录
     * @param newFileName     上传后的新文件名,如果为null，则使用源文件名
     * @throws Exception
     */
    public SftpUtil uploadFile(String localFile, String remoteDirectory, String newFileName) throws Exception {

        InputStream in = null;
        try {
            File file = new File(localFile);
            //新文件名
            String fileName = (newFileName != null && newFileName.length() > 0) ? newFileName : file.getName();
            in = new FileInputStream(file);
            //上传文件
            return uploadFile(in, remoteDirectory, fileName);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (in != null) in.close();
            } catch (Exception e2) {
            }
        }
    }

    /**
     * 上传单个文件，请注意关闭InputStream
     *
     * @param in              要上传的文件流
     * @param remoteDirectory 远程文件夹目录
     * @param newFileName     上传后的新文件名
     * @throws Exception
     */
    public SftpUtil uploadFile(InputStream in, String remoteDirectory, String newFileName) throws Exception {
        //创建远程文件夹
        createRemoteDirIfNeed(remoteDirectory);
        channel.cd(remoteDirectory);

        try {
            //上传文件
            channel.put(in, newFileName);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            throw e;
        } finally {
        }

        return this;
    }

    /**
     * 创建远程机器的文件夹（如果需要的话）
     *
     * @throws SftpException
     */
    public void createRemoteDirIfNeed(String remoteDir) throws SftpException {
        try {
            channel.cd(remoteDir);
        } catch (SftpException sException) {
            if (ChannelSftp.SSH_FX_NO_SUCH_FILE == sException.id) {
                channel.mkdir(remoteDir);
            }
        }

    }

    /**
     * 关闭连接
     */
    public void close() {

        if (channel != null) {
            channel.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
    }

    public ChannelSftp getChannel() {
        return channel;
    }


    /**
     * 递归根据路径创建文件夹
     *
     * @param dirs     根据 / 分隔后的数组文件夹名称
     * @param tempPath 拼接路径
     * @param length   文件夹的格式
     * @param index    数组下标
     * @return
     */
    public void mkdirDirs(String[] dirs, String tempPath, int length, int index) {
        // 以"/a/b/c/d"为例按"/"分隔后,第0位是"";顾下标从1开始
        index++;
        if (index < length) {
            // 目录不存在，则创建文件夹
            tempPath += "/" + dirs[index];
        }
        try {
            channel.cd(tempPath);
            if (index < length) {
                mkdirDirs(dirs, tempPath, length, index);
            }
        } catch (SftpException ex) {
            try {
                channel.mkdir(tempPath);
                channel.cd(tempPath);
            } catch (SftpException e) {
                e.printStackTrace();
            }
            mkdirDirs(dirs, tempPath, length, index);
        }
    }


    /**
     * @param remotePaths 要求是 /home/active/sucess
     */
    public void mkdirDirs(String... remotePaths) {
        for (String remotePath : remotePaths) {
            String[] split = remotePath.split("/");
            mkdirDirs(split, "", split.length, 0);
        }
    }

    /**
     * 移动文件
     *
     * @param oldpath
     * @param newpath
     */
    public void move(String oldpath, String newpath) {
        try {
            channel.rename(oldpath, newpath);
        } catch (SftpException e) {
            e.printStackTrace();
        }
    }


    /**查看文件夹下面的txt文件
     * @param path
     * @return
     */
    public List<String> ls(String path) {
        try {
            Vector<ChannelSftp.LsEntry> ls = channel.ls(path);
            return ls.stream().map(e -> e.getFilename()).filter(e-> e.contains(".txt")).collect(Collectors.toList());
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return null;
    }
}
