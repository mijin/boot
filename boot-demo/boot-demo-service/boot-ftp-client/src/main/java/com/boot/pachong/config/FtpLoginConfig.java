package com.boot.pachong.config;

import lombok.Data;

@Data
public class FtpLoginConfig {
    /**
     * ftp服务器地址
     */
    private String host;

    /**
     * ftp服务器端口
     */
    private int port;

    /**
     * ftp服务器用户名
     */
    private String username;

    /**
     * ftp服务器密码
     */
    private String password;
    private String accessToken;
}
