package com.boot.tinyproxy.server;

import com.boot.tinyproxy.client.utils.InternetProxyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;


@Slf4j
//@SpringBootTest
public class TinyClientApplicationTest {

    /**
     * 使用远程代理
     */
    @Test
    public void test01() {
        String url = "http://134.32.128.201:20110/oppf";
        final String username = "user";
        final String password = "CNBU_lenovo_2021";
        final String host = "192.168.20.31";
        final int port = 8081;


        HttpComponentsClientHttpRequestFactory factory = InternetProxyUtils.getFactory(host, port, username, password);
        RestTemplate restTemplate = new RestTemplate(factory);

        //发送请求
        String result = restTemplate.getForObject(url, String.class);

        log.info(result);

    }

    @Test
    public void test02() {
        String url = "http://www.baidu.com/";

        CloseableHttpClient build = HttpClientBuilder.create()
                .setProxy(new HttpHost("localhost", 8081))
                .build();

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(build);

        RestTemplate restTemplate = new RestTemplate(factory);

        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("user","CNBU_lenovo_2021"));

        //发送请求
        String result = restTemplate.getForObject(url, String.class);

        log.info(result);

    }

}