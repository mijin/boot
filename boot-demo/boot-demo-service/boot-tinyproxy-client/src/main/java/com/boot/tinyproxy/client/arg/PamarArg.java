package com.boot.tinyproxy.client.arg;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ToString
@Component
@ConfigurationProperties("tiny")
@Data
public class PamarArg {
    private String host;
    private Integer port;
    private String username;
    private String password;
    private String testUrl;
}
