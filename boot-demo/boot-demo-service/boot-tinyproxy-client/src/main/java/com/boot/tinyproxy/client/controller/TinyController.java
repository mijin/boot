package com.boot.tinyproxy.client.controller;

 import com.boot.tinyproxy.client.arg.PamarArg;
 import com.boot.tinyproxy.client.utils.InternetProxyUtils;
 import lombok.extern.slf4j.Slf4j;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.SpringBootVersion;
 import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
public class TinyController {

    @Autowired
    private PamarArg pamarArg;

    @RequestMapping("/get")
    public Object get(String phost,String pport) {
        log.info("代理开始，args:{}",pamarArg);
        String url = pamarArg.getTestUrl();
        final String username = pamarArg.getUsername();
        final String password = pamarArg.getPassword();
        final String host =pamarArg.getHost();
        final int port = pamarArg.getPort();


        HttpComponentsClientHttpRequestFactory factory = InternetProxyUtils.getFactory(host, port, username, password);
        RestTemplate restTemplate = new RestTemplate(factory);

        //发送请求
        String result = restTemplate.getForObject(url, String.class);

        return result;
    }



    @RequestMapping("/test")
    public Object test(String phost,String pport) {


        return SpringBootVersion.class.getPackage();
    }
}
