package com.boot.tinyproxy.client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class TinyClientApplication {



    public static void main(String[] args) {
        SpringApplication.run(TinyClientApplication.class, args);
        log.info(TinyClientApplication.class.getName()+"--启动成功");
     }
}
