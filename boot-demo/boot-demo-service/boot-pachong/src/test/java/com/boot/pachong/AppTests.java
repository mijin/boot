package com.boot.pachong;

import com.boot.PachongApplication;
import com.boot.base.common.enity.video.VideoListDo;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PachongApplication.class)
public class AppTests {


    static class Xpath implements PageProcessor {

        @Override
        public void process(Page page) {
            log.info("{}", page);
        }

        @Override
        public Site getSite() {
            return Site
                    .me()
                    .setSleepTime(3000)
                    .setUserAgent(
                            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");
        }
    }

    @Test
    public void test01() {
        Spider.create(new Xpath()).addUrl("http://www.yongjiuzy1.com/?m=vod-detail-id-38101.html").run();

    }

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Test
    public void zset() {
        ZSetOperations.TypedTuple<String> typedTuple1 = new DefaultTypedTuple<>("C", 16.0);
        ZSetOperations.TypedTuple<String> typedTuple2 = new DefaultTypedTuple<>("F", 7.0);
        ZSetOperations.TypedTuple<String> typedTuple3 = new DefaultTypedTuple<>("G", 5.0);
        Set<ZSetOperations.TypedTuple<String>> typedTupleSet = new HashSet<>();
        typedTupleSet.add(typedTuple1);
//        typedTupleSet.add(typedTuple2);
//        typedTupleSet.add(typedTuple3);
        redisTemplate.opsForZSet().add("typedTupleSet", typedTupleSet);
    }

    @Test
    public void test02() {
        Boolean expire = redisTemplate.expire("typedTupleSet", 1, TimeUnit.HOURS);
        log.info("--{}", expire);
    }

    @Test
    public void test03() {
        final long start = System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        final String forObject = restTemplate.getForObject("http://app.xjlb6.com/api.php/app_2/vodList", String.class);
        log.info("获取数据成功: {}ms", System.currentTimeMillis() - start);

        final JSONObject jsonObject = JSONObject.fromObject(forObject);
        final JSONArray list = jsonObject.getJSONArray("list");
        final HashOperations<String, Object, Object> forHash = redisTemplate.opsForHash();
        for (Object o : list) {
            final JSONObject object = JSONObject.fromObject(o);
            final int index = object.getInt("vod_id") % 100;
            forHash.put("video:index:" + index, object.getString("vod_id"), object.toString());
            log.info("加载到redis: {}", object.get("vod_name"));
        }
        log.info("存数据成功: {}ms", System.currentTimeMillis() - start);
    }


    @Test
    public void test04() {
        long start = System.currentTimeMillis();
        HashOperations<String, Object, Object> forHash = redisTemplate.opsForHash();
        RestTemplate restTemplate = new RestTemplate();
        for (int index = 0; index < 100; index++) {
            List<Object> values = forHash.values("video:index:" + index);
            int finalIndex = index;
            values.parallelStream().forEach(e -> {
                        try {
                            JSONObject object = JSONObject.fromObject(e);
                            final int vodId = object.getInt("vod_id");
                            String forObject = restTemplate.getForObject("http://app.xjlb5.com/api.php/app_2/lookVod?vod_id=" + vodId, String.class);
                            JSONObject jsonObject = JSONObject.fromObject(forObject);
                            if (jsonObject != null) {
                                Object info = jsonObject.get("info");
                                if (info != null) {
                                    JSONObject infoObj = JSONObject.fromObject(info);
                                    forHash.put("video:detail:index:" + finalIndex, vodId + "", infoObj.toString());
                                    log.info("加载到redis: {} , index={}, vod_id= {}", infoObj.get("vod_name"), finalIndex, vodId);
                                }
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
            );
        }
        log.info("存数据成功: {}ms", System.currentTimeMillis() - start);
    }


    @Test
    public void test05() {
        long start = System.currentTimeMillis();
        HashOperations<String, Object, Object> forHash = redisTemplate.opsForHash();
        RestTemplate restTemplate = new RestTemplate();
        for (int index = 0; index < 100; index++) {
            List<Object> values = forHash.values("video:index:" + index);
            int finalIndex = index;
            values.parallelStream().forEach(e -> {
                        try {
                            JSONObject object = JSONObject.fromObject(e);
                            final int vodId = object.getInt("vod_id");
                            final Object detail = forHash.get("video:detail:index:" + finalIndex, vodId + "");
                            object.put("detail", detail);
                            forHash.put("video:body:" + finalIndex, vodId + "", object.toString());
                            log.info("加载到redis: {} , index={}, vod_id= {}", object.get("vod_name"), finalIndex, vodId);
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
            );
        }
        log.info("存数据成功: {}ms", System.currentTimeMillis() - start);
    }
}
