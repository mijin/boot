package com.boot.pachong;

import com.boot.base.common.utils.DownUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
public class UtilsTests {
    @Test
    public void test01() {
        LocalDateTime now = LocalDateTime.now();
        log.info(now.format(DateTimeFormatter.ofPattern("HH")));
        LocalDateTime hours = now.plusHours(-10);
        log.info(hours.format(DateTimeFormatter.ofPattern("HH")));
    }

    @Test
    public void test02() {
        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject("http://localhost:8080/index?code=啊啊啊", String.class);
        log.info(forObject);

        PoolingHttpClientConnectionManager poolingConnectionManager = new PoolingHttpClientConnectionManager();
        // 连接池最大连接数
        poolingConnectionManager.setMaxTotal(200);
        // 每个主机的并发
        poolingConnectionManager.setDefaultMaxPerRoute(100);
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        //设置HTTP连接管理器
        httpClientBuilder.setConnectionManager(poolingConnectionManager);
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(httpClientBuilder.build());
        // 连接超时，毫秒
        clientHttpRequestFactory.setConnectTimeout(6000);
        // 读写超时，毫秒
        //todo: 订购接口超时问题
        clientHttpRequestFactory.setReadTimeout(10000);
        restTemplate.setRequestFactory(clientHttpRequestFactory);
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        messageConverters.removeIf(converter -> converter instanceof StringHttpMessageConverter);
        messageConverters.add(new StringHttpMessageConverter(StandardCharsets.UTF_8));
        String forObject1 = restTemplate.getForObject("http://localhost:8080/index", String.class);
        log.info(forObject1);
    }


    @SneakyThrows
    @Test
    public void test03() {
         log.info(String.join(":","1","2","3"));
        DownUtils.download("https://search.pstatic.net/common?src=https://i.imgur.com/AiR1AWn.jpg#vwid=1280&vhei=853","/data3/img/a.jpg");
    }

    @Test
    public void test04() {
        String a = "123";
        String b = "123";

        log.info("{}",a==b);

        a= new String("123");
        log.info("{}",a==b);
        b= new String("123");
        log.info("{}",a==b);

    }
}
