package com.boot.mq.rabbit.xxlconf;

 import lombok.SneakyThrows;
import org.I0Itec.zkclient.exception.ZkMarshallingError;
import org.I0Itec.zkclient.serialize.ZkSerializer;

import java.io.UnsupportedEncodingException;

/**
 * json序列化方法。
 *
 * @author LiuXianfa
 * @date 2018/5/11
 */
public class JsonZkSerializer implements ZkSerializer {

    @SneakyThrows
    @Override
    public byte[] serialize(Object data)  {
        try {
            return ((String) data).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new Exception("json序列化 失败！");
        }
    }

    @Override
    public Object deserialize(byte[] bytes) throws ZkMarshallingError {
        return new String(bytes);
    }
}
