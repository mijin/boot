package com.boot.mq.rabbit.xxlconf;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SyncConfiguartionToZk {

    /**
     * 同步到zk时，使用的key:拼接节点node的path使用
     *
     * @see XxlConfManager#keyToPath(String, String)
     * @return
     */
    String key() default "";

    /**
     * 企业表和当前配置表是一对多关系(M)还是一对一关系(S)，<br>
     * 一对多关系：使用"selectList"<br>
     * 一对一关系：使用"selectOne"<br>
     *
     * @return
     */
    String method() default "selectOne";

    /**
     * 描述信息,便于理解,程序中并无使用此字段
     *
     * @return
     */
    String desc() default "";

    interface MethodType{
        String SELECT_ONE = "selectOne";
        String SELECT_LIST = "selectList";
    }
}
