//package com.boot.mq.rabbit.xxlconf;
//
//import org.I0Itec.zkclient.ZkClient;
//import org.apache.zookeeper.ZooDefs;
//import org.apache.zookeeper.data.ACL;
//import org.apache.zookeeper.data.Id;
//import org.apache.zookeeper.data.Stat;
//
//import java.security.NoSuchAlgorithmException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import static org.apache.zookeeper.server.auth.DigestAuthenticationProvider.generateDigest;
//
///**
// * 这只是一个工具类
// *
// * @author LiuXianfa
// * @date 2018/5/10
// */
//public class ZkMainUtils {
//
//    private static final String zkAddress = "localhost:2181";
//    private static final String testNode = "/nb-conf";
//    private static final String readAuth = "read-user:123456";
//    private static final String writeAuth = "write-user:123456";
//    private static final String deleteAuth = "delete-user:123456";
//    private static final String allAuth = "admin:admin";
//    private static final String superAuth = "super-user:123456";
//    private static final String adminAuth = "admin-user:123456";
//    private static final String digest = "digest";
//
//    private static void initNode() throws NoSuchAlgorithmException {
//        ZkClient zkClient = new ZkClient(zkAddress);
//        zkClient.addAuthInfo(digest, allAuth.getBytes());
//
//        if (zkClient.exists(testNode)) {
//            zkClient.delete(testNode);
//            System.out.println("节点删除成功！");
//        }
//
//        List<ACL> acls = new ArrayList<>();
//        acls.add(new ACL(ZooDefs.Perms.ALL, new Id(digest, generateDigest(allAuth))));
//        acls.add(new ACL(ZooDefs.Perms.READ, new Id("world", "anyone")));
//        zkClient.createPersistent(testNode, "test-data", acls);
//
//        System.out.println((String) zkClient.readData(testNode));
//        System.out.println("节点创建成功！");
//        zkClient.close();
//    }
//
//    private static void readTest() {
//        ZkClient zkClient = new ZkClient(zkAddress);
//
//        try {
//            System.out.println((String) zkClient.readData(testNode));//没有认证信息，读取会出错
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//
//        try {
//            zkClient.addAuthInfo(digest, adminAuth.getBytes());
//            System.out.println((String) zkClient.readData(testNode));//admin权限与read权限不匹配，读取也会出错
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//
//        try {
//            zkClient.addAuthInfo(digest, readAuth.getBytes());
//            System.out.println((String) zkClient.readData(testNode));//只有read权限的认证信息，才能正常读取
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//
//        zkClient.close();
//    }
//
//    private static void writeTest() {
//        ZkClient zkClient = new ZkClient(zkAddress);
//
//        try {
//            zkClient.writeData(testNode, "new-data");//没有认证信息，写入会失败
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//
//        try {
//            zkClient.addAuthInfo(digest, writeAuth.getBytes());
//            zkClient.writeData(testNode, "new-data");//加入认证信息后,写入正常
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//
//        try {
//            zkClient.addAuthInfo(digest, readAuth.getBytes());
//            System.out.println((String) zkClient.readData(testNode));//读取新值验证
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//
//        zkClient.close();
//    }
//
//    private static void deleteTest() {
//        ZkClient zkClient = new ZkClient(zkAddress);
//        //zkClient.addAuthInfo(digest, deleteAuth.getBytes());
//        try {
//            //SystemEntity.out.println(zkClient.readData(testNode));
//            zkClient.addAuthInfo(digest, allAuth.getBytes());
//            zkClient.deleteRecursive(testNode);
//            System.out.println("节点删除成功！");
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//        zkClient.close();
//    }
//
//    private static void changeACLTest() {
//        ZkClient zkClient = new ZkClient(zkAddress);
//        //注：zkClient.setAcl方法查看源码可以发现，调用了readData、setAcl二个方法
//        //所以要修改节点的ACL属性，必须同时具备read、admin二种权限
//        zkClient.addAuthInfo(digest, adminAuth.getBytes());
//        zkClient.addAuthInfo(digest, readAuth.getBytes());
//        try {
//            List<ACL> acls = new ArrayList<ACL>();
//            acls.add(new ACL(ZooDefs.Perms.ALL, new Id(digest, generateDigest(adminAuth))));
//            zkClient.setAcl(testNode, acls);
//            Map.Entry<List<ACL>, Stat> aclResult = zkClient.getAcl(testNode);
//            System.out.println(aclResult.getKey());
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//        zkClient.close();
//    }
//
//
//    public static void main(String[] args) throws Exception {
//
////        initNode();
//
////        SystemEntity.out.println("---------------------");
////
////        readTest();
////
////        SystemEntity.out.println("---------------------");
////
////        writeTest();
////
////        SystemEntity.out.println("---------------------");
////
////        changeACLTest();
////
////        SystemEntity.out.println("---------------------");
////
//        deleteTest();
//
//    }
//}