//package com.boot.mq.rabbit.xxlconf;
//
//import com.alibaba.fastjson.JSON;
//import com.baomidou.mybatisplus.mapper.EntityWrapper;
//import io.renren.common.exception.RRException;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.lang.reflect.Method;
//
///**
// * 使用注解+aop方式实现:把企业域名配置同步到zk
// *
// * @author LiuXianfa
// * @email xianfaliu@newbanker.cn
// * @date 2018/3/31
// */
//@Aspect
//@Component
//public class SyncConfiguartionToZkAspect {
//    protected Logger logger = LoggerFactory.getLogger(getClass());
//
//    @Autowired
//    private XxlConfManager xxlConfManager;
//
//    @Pointcut("@annotation(io.newbanker.xxlconf.SyncConfiguartionToZk)")
//    public void pointcut() {
//    }
//
//    @Around("pointcut()")
//    public Object around(ProceedingJoinPoint point) throws Throwable {
//
//        // 步骤1：获取企业id：配置选项同步到zk时，拼接节点node的path使用
//        Integer entid = (Integer) point.getArgs()[0];
//        // key：配置选项同步到zk时，拼接节点node的path使用
//        String key = null;
//        // 配置选项同步到zk时，节点node的value
//        String data = null;
//
//        Method targetMethod = ((MethodSignature) point.getSignature()).getMethod();
//        SyncConfiguartionToZk syncZk = targetMethod.getAnnotation(SyncConfiguartionToZk.class);
//        if (syncZk == null) {
//            throw new RRException("同步企业配置到zk时失败");
//        }
//        key = syncZk.key();
//
//
//        // 步骤2：根据企业id，查询相关配置(比如wbs_doamin)的数据
//        //      步骤2-1：获得serviceImpl对象：为了调用selectList方法或者selectOne方法查询数据库。
//        Object service = point.getThis();
//        //      步骤2-2：根据syncZk.method()的值，获得selectList()方法或selectOne()方法
//        Method[] declaredMethods = service.getClass().getDeclaredMethods();
//        Method selectOneOrList = null;
//        for (Method method : declaredMethods) {
//            if (method.getName().equals(syncZk.method())) {
//                selectOneOrList = method;
//                break;
//            }
//        }
//        //      步骤2-3：使用反射，调用selectList方法或者selectOne方法查询数据库。
//        Object selectEntityFromDB = selectOneOrList.invoke(service, new EntityWrapper<>().eq("entid", entid));
//        data = JSON.toJSONString(selectEntityFromDB);
//
//
//        // 步骤3：把数据set到zk
//        logger.debug("同步企业配置到zk-开始,entid={},key={},data={}", entid, key, data);
//        xxlConfManager.set(entid.toString(), key, data);
//        logger.debug("同步企业配置到zk-成功！");
//
//        // 步骤4：执行增强方法
//        point.proceed();
//
//
//        return null;
//    }
//}
