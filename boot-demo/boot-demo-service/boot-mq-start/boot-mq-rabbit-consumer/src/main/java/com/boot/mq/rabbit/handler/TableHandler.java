package com.boot.mq.rabbit.handler;

/**
 * @author zongbo1
 */
public interface TableHandler<T> {

    /**
     * 当前handler支持的表名
     * @return
     */
    String supportTable();
    /**
     * 表数据插入
     * @param t
     */
    default void insert(T t) {}
    /**
     * 表数据删除
     * @param t
     */
    default void delete(T t) {}

    /**
     * 表数据更新
     * @param after
     */
    default void update(T t) {}
}
