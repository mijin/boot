package com.boot.mq.rabbit.xxlconf;

import static java.lang.String.format;

/**
 * @author LiuXianfa
 * @email xianfaliu@newbanker.cn
 * @date 2018/10/17 14:06
 */
public class KeyType {
    /**
     * jdbc
     */
    public static final String JDBC = "datasource";

    /**
     * ali_oss
     */
    public static final String ALI_OSS = "alioss";

    /**
     * 企业域名
     */
    public static final String DOMAIN = "domain";

    /**
     * 企业信息
     */
    public static final String INFO = "info";

    /**
     * 企业的权限
     */
    public static final String RESOURCE = "resource";
    private static final String RESOURCE_SYSID_PARTTEN = "/ent%s/resource/sysId%s";
    /**
     * 企业开通系统拥有权限列表
     * @return /ent{entId}/resource/sysId{systemId}
     *
     * @param entId
     * @param systemId
     */
    public static String getResourceSysIdSubPath(String entId, Integer systemId) {
        return format(RESOURCE_SYSID_PARTTEN, entId, systemId);
    }

    /**
     * 企业开通的系统ids
     */
    public static final String systemIds = "systemId";

    /**
     * 报表datasoutce
     * gwy_sys_resource_report
     */
    public static final String REPORT_DATASOURCE = "reportDataSource";

    /**
     * 企业的管理员
     */
    public static final String ADMIN = "admin";

    /**
     * 系统管理员
     * 一个企业指定父系统只会有一个管理员
     * /ent{entId}/{systemCode}/admin = {}
     */
    private static final String SYSTEM_ADMIN = "/ent%s/%s/admin";
    public static String getSystemAdminSubPath(String entId, String systemCode) {
        return format(SYSTEM_ADMIN, entId, systemCode);
    }

    /**
     * APP消息推送
     */
    public static final String APP_MESSAGE_PUSH = "app";

    /**
     * 短信API配置
     */
    public static final String SMSCONFIG = "smsconfig";

    /**
     * 短信模板配置
     */
    public static final String SMSTEMPLATE = "smstemplate";

    /**
     * WECHAT
     */
    public static final String WECHAT = "wechat";
    /**
     * 其他配置
     */
    public static final String PARAMS = "params";

    /**
     * 各个系统的参数配置
     * %s 代表 entId
     * %s 代表 系统Code
     * /ent{entId}/{systemCode}/params = [{},{}]
     */
    private static final String SYSTEM_PARAMS_PARTTEN = "/ent%s/%s/params";
    public static String getParamsSubPath(String entId, String systemCode) {
        return format(SYSTEM_PARAMS_PARTTEN, entId, systemCode);
    }

    /**
     * 单独参数配置
     * %s 代表 entId
     * %s 代表 系统Code
     * %s 代表 参数Code
     * /ent{entId}/{systemCode}/params/{paramCode} = value
     */
    private static final String SINGLE_SYSTEM_PARAMS_PARTTEN = "/ent%s/%s/params/%s";
    private static final String SINGLE_WBS_PARAMS_PARTTEN = "/ent%s/params/%s";

    /**
     * @return /ent{entId}/params/{paramCode}
     *
     * @param entId
     * @param paramCode
     */
    public static String getWbsSingleParamsSubPath(String entId, String paramCode) {
        return format(SINGLE_WBS_PARAMS_PARTTEN, entId, paramCode);
    }

    /**
     * @return /ent{entId}/{systemCode}/params/{paramCode}
     *
     * @param entId
     * @param systemCode
     * @param paramCode
     */
    public static String getSingleParamsSubPath(String entId, String systemCode, String paramCode) {
        return format(SINGLE_SYSTEM_PARAMS_PARTTEN, entId, systemCode, paramCode);
    }

//    /**
//     * 系统下的其他配置
//     * %s 代表 entId
//     * %s 代表 系统Code
//     * %s 代表 参数code
//     * /nb-conf/ent{entId}/{systemCode}/params/{paramCode} = {paramValue}
//     */
//    public static final String SYSTEM_PARAMS_PARAMSCODE_PARTTEN = "/ent%s/%s/params/%s";
//    public static String getParamsSubPath(String entId, String systemCode, String paramCode) {
//        return String.format(SYSTEM_PARAMS_PARAMSCODE_PARTTEN, entId, systemCode, paramCode);
//    }


    /**
     * 通过entId和表名转成zk的nodeSubPath
     *
     * @param entId 企业Id
     * @param key {@link KeyType}
     * @return subPath
     */
    public static String toSubPath(String entId, String key) {
        return format("/ent%s/%s", entId, key);
    }

    /**
     * @param entId 企业Id
     * @param systemCode
     * @param key
     * @return
     */
    public static String toSubPath(String entId, String systemCode, String key) {
        return format("/ent%s/%s/%s", entId, systemCode, key);
    }


    /**
     * /ent{entId}/wechat/{wechat_code}={data}
     * @param entId
     * @param code 小程序业务唯一编码
     * @return
     */
    public static String getEntWechatCodeSubPath(Integer entId, String code) {
        return format("/ent%s/wechat/%s", entId, code);
    }

    /**
     * /ent{entId}/wechat/{wechat_code}/{wechat_template_code}={data}
     *
     * @param entId
     * @param wechatCode
     * @param wechatTemplateCode
     * @return
     */
    public static String getWechatTemplateSubPath(String entId, String wechatCode, String wechatTemplateCode) {
        return format("/ent%s/wechat/%s/%s", entId, wechatCode, wechatTemplateCode);
    }
}
