package com.boot.dubbo.consumer;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.FlatMessage;
import com.boot.base.common.utils.JsonUtil;
import com.boot.mq.rabbit.constant.RabbitMqConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitMqConsumer {



    @RabbitListener(queues = RabbitMqConstant.DBMESSAGE)
    public void dnMessage(byte[] message) {
        String s = new String(message);
        FlatMessage flatMessage = JsonUtil.jsonToObject(s, FlatMessage.class);
        CanalEntry.EventType eventType = CanalEntry.EventType.valueOf(flatMessage.getType());



        log.info("消费的数据:{}",flatMessage);
     }
}
