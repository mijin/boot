package com.boot.mq.rabbit.xxlconf;

import java.lang.annotation.*;

/**
 * <a href="https://my.oschina.net/anxiaole/blog/1816553">解决bug：org.apache.zookeeper.KeeperException$NoAuthException: KeeperErrorCode = NoAuth for /nb-conf</a>
 *
 * @author LiuXianfa
 * @date 2018/5/22
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResetAclIfNoauth {

}
