//package com.boot.mq.rabbit.xxlconf;
//
//import org.apache.zookeeper.KeeperException;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * <a href="https://my.oschina.net/anxiaole/blog/1816553">解决bug：org.apache.zookeeper.KeeperException$NoAuthException: KeeperErrorCode = NoAuth for /nb-conf</a>
// *
// * @author LiuXianfa
// * @date 2018/5/22
// */
//@Component
//@Aspect
//public class ZkNoAuthExceptionAspect {
//    @Autowired
//    private XxlConfManager xxlConfManager;
//
//    protected Logger logger = LoggerFactory.getLogger(getClass());
//
//    @Pointcut("@annotation(io.newbanker.xxlconf.ResetAclIfNoauth)")
//    public void pointcut() {
//    }
//
//    @Around("pointcut()")
//    public Object around(ProceedingJoinPoint point) throws Throwable {
//
//        try {
//            return point.proceed();
//        } catch (Throwable e) {
//            if (e.getCause() instanceof KeeperException.NoAuthException) {
//                logger.warn(">>> zkClient 没有权限异常。zkClient重连后digest重新设置！");
//                xxlConfManager.addAuthInfo();
//            }
//            return point.proceed();
//        }
//    }
//}
