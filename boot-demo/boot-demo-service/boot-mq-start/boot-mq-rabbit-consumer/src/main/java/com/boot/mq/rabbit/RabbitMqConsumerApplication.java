package com.boot.mq.rabbit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@Slf4j
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class RabbitMqConsumerApplication {



    public static void main(String[] args) {
        SpringApplication.run(RabbitMqConsumerApplication.class, args);
        log.info(log.getName()+"启动成功。。。");
     }
}
