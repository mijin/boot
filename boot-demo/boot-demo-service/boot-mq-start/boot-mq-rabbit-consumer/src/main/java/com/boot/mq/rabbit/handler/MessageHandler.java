package com.boot.mq.rabbit.handler;

/**
 * 消息处理
 * @author zongbo1
 */
public interface MessageHandler<T> {
    /**
     * 处理消息
     * @param t
     * @return true:消息被处理  false：消息未被处理
     */
    boolean handle(T t);
}
