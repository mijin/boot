package com.boot.mq.rabbit.handler.canal;

import com.alibaba.otter.canal.protocol.FlatMessage;
import com.boot.mq.rabbit.handler.TableHandler;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import java.text.SimpleDateFormat;

/**
 * @author zongbo1
 */
public abstract class CanalTableHandler implements TableHandler<FlatMessage> {
    static ObjectMapper snakeCaseObjectMapper = new ObjectMapper();
    static {
        snakeCaseObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        snakeCaseObjectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")) ;
        snakeCaseObjectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
}
