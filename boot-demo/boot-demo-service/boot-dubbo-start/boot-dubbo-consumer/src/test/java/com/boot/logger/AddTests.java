package com.boot.logger;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import zipkin2.Endpoint;
import zipkin2.Span;
import zipkin2.codec.SpanBytesEncoder;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class AddTests {

    @Test
    public void test01() {
// All data are recorded against the same endpoint, associated with your service graph
        Endpoint localEndpoint = Endpoint.newBuilder().serviceName("tweetie").ip("localhost").build();
        Span.Builder builder = Span.newBuilder()
                .traceId("d3d200866a77cc59")
                .id("d3d200866a77cc59")
                .name("targz")
                .localEndpoint(localEndpoint)
                .timestamp(System.currentTimeMillis())
                .duration(500L)
                .putTag("compression.level", "9");
        Span span = builder.build();
 // Now, you can encode it as json
        byte[] encode = SpanBytesEncoder.JSON_V2.encode(span);
        log.info("res={}",new String(encode));
    }

    /**
     * 0位置不动，1移动到左边，-1移动到右边
     */
    @Test
    public void test02() {
        List<Integer> collect = Stream.of(1, 3,3, 2).sorted((a, b) -> {
            int compare = a.compareTo(b)  ;
            log.info("a={},b={},compre={}",a,b,compare);
            return compare;
        }).collect(Collectors.toList());
        log.info("res={}",collect);
    }



    @Test
    public void test03() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        // 当前月最大天数和当前结束日期的天数
        int maximumDay = cal.getActualMaximum(Calendar.DATE);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        log.info("maximumDay={},day={}",maximumDay,day);
    }
}
