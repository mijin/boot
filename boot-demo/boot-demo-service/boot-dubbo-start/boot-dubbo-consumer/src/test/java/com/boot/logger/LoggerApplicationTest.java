package com.boot.logger;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class LoggerApplicationTest {
    @Test
    void contextLoads() {
        boolean infoEnabled = log.isInfoEnabled();
        boolean debugEnabled = log.isDebugEnabled();
        boolean errorEnabled = log.isErrorEnabled();
        boolean traceEnabled = log.isTraceEnabled();

        log.info("00");
        log.debug("11");
    }
}