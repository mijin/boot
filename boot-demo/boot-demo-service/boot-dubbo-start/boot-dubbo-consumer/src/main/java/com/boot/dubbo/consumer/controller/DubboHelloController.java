package com.boot.dubbo.consumer.controller;

import com.boot.dubbo.service.DubboHelloService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class DubboHelloController {

    @Reference
    private DubboHelloService dubboHelloService;

    @RequestMapping("/hello")
    public Object test(String content) {
        long start = System.currentTimeMillis();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String say = dubboHelloService.say(content);
        log.info("耗费时间={}，return={}",System.currentTimeMillis()-start,say);
        return say;
    }
}
