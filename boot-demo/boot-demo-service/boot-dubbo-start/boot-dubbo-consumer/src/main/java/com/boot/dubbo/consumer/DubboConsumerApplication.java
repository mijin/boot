package com.boot.dubbo.consumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@Slf4j
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class DubboConsumerApplication {



    public static void main(String[] args) {
        SpringApplication.run(DubboConsumerApplication.class, args);
        log.info(log.getName()+"启动成功。。。");
     }
}
