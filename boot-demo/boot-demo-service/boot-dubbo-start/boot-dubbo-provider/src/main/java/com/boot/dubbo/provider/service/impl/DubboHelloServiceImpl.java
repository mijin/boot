package com.boot.dubbo.provider.service.impl;

import com.boot.dubbo.service.DubboHelloService;
import org.apache.dubbo.config.annotation.Service;

@Service
public class DubboHelloServiceImpl implements DubboHelloService {
    @Override
    public String say(String content) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return content+":123";
    }
}
