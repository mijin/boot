package com.boot.dubbo.provider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class DubboProviderApplication {



    public static void main(String[] args) {
        SpringApplication.run(DubboProviderApplication.class, args);
        log.info(log.getName()+"启动成功。。。");
     }
}
