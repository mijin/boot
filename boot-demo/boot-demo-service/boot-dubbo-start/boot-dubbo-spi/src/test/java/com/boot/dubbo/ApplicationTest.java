package com.boot.dubbo;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.CountDownLatch;

@Slf4j
class ApplicationTest {
    String zkAdders = "152.136.130.239:39002";
    String path = "/cc-conf";

    @SneakyThrows
    @Test
    void exists() {
        ZooKeeper zooKeeper = new ZooKeeper(zkAdders, 5000, null);
        Stat exists = zooKeeper.exists(path, null);

        log.info("00");
    }

    /**
     * 赋值
     */
    @SneakyThrows
    @Test
    void set() {
        Watcher watcher = new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                log.info("path:{}", event.getPath());
            }
        };
        ZooKeeper zk = new ZooKeeper(zkAdders, 5000, watcher);
        Stat stat = zk.exists(path, watcher);
        zk.setData(path, "1234506b".getBytes(), stat.getVersion());
        log.info("00");
    }

    @SneakyThrows
    @Test
    void setlist() {
        ZooKeeper zk = new ZooKeeper(zkAdders, 5000, null);

    }

    /**
     * @param path
     * @param val
     * @param zk
     */
    static void insert(String path, String val, ZooKeeper zk) {

    }


    @SneakyThrows
    @Test
    void create() {
        ZooKeeper zk = new ZooKeeper(zkAdders, 5000, null);
        zk.create(path, "123456a".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        log.info("00");
    }

    @SneakyThrows
    @Test
    void get() {
        Watcher watcher = new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                log.info("path:{}", event.getPath());
            }
        };
//        String zkAdders = "config.com:2181";
        String path = "/nb-conf";
        ZooKeeper zk = new ZooKeeper(zkAdders, 500000, watcher);
//        waitUntilConnected(zk, new CountDownLatch(1));
        Stat stat = zk.exists(path, watcher);
        byte[] data = zk.getData(path, watcher, stat);
        String val = new String(data);
        log.info(val);
    }

    /**
     * 等待
     *
     * @param zooKeeper
     * @param connectedLatch
     */
    public static void waitUntilConnected(ZooKeeper zooKeeper, CountDownLatch connectedLatch) {
        if (ZooKeeper.States.CONNECTING == zooKeeper.getState()) {
            try {
                connectedLatch.await();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}