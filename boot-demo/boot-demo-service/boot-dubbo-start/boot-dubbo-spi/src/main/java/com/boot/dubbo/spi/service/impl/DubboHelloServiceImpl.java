package com.boot.dubbo.spi.service.impl;

import com.boot.dubbo.service.DubboHelloService;
import org.apache.dubbo.config.annotation.Service;

@Service
public class DubboHelloServiceImpl implements DubboHelloService {
    @Override
    public String say(String content) {
        return content+":123";
    }
}
