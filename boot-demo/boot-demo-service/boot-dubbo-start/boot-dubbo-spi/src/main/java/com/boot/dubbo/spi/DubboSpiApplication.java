package com.boot.dubbo.spi;
import com.boot.dubbo.spi.robot.Robot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;

import java.util.ServiceLoader;

@Slf4j
//@SpringBootApplication
public class DubboSpiApplication {



    public static void main(String[] args) {
//        SpringApplication.run(DubboSpiApplication.class, args);
       new DubboSpiApplication().sayHello();
        log.info(log.getName()+"启动成功。。。{\"superAdmin\":1}");
     }

     public  void sayHello()     {
        ServiceLoader<Robot> serviceLoader = ServiceLoader.load(Robot.class);
        serviceLoader.forEach(Robot::sayHello);
    }
}
