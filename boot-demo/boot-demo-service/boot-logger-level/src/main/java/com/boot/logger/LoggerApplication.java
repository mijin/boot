package com.boot.logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@SpringBootApplication
@ComponentScan(basePackages = {"com.boot.base.web","com.boot.logger"})
public class LoggerApplication {



    public static void main(String[] args) {
        SpringApplication.run(LoggerApplication.class, args);
     }
}
