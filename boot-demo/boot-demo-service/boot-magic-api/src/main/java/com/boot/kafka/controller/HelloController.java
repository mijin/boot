package com.boot.kafka.controller;

import org.springframework.boot.SpringBootVersion;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    /**
     * @return
     */
    @RequestMapping("/info")
    public Object info(){
        return SpringBootVersion.class.getPackage();
    }
}
