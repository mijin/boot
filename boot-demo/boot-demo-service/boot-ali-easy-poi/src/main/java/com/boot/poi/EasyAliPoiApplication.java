package com.boot.poi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@SpringBootApplication
public class EasyAliPoiApplication {



    public static void main(String[] args) {
        SpringApplication.run(EasyAliPoiApplication.class, args);
        log.info(EasyAliPoiApplication.class.getName()+"...");
    }


}
