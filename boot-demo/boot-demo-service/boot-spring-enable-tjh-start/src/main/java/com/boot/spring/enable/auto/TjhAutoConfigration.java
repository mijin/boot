package com.boot.spring.enable.auto;

import com.boot.spring.enable.TjhEntity;
import com.boot.spring.enable.properties.TjhConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(TjhConfigProperties.class)
public class TjhAutoConfigration {

    @Autowired
    private TjhConfigProperties tjhConfigProperties;

    @Bean
    TjhEntity tjhEntity(){
        TjhEntity tjh = new TjhEntity();
        tjh.setKey(tjhConfigProperties.getKey());
        return tjh;
    }
}
