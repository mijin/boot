package com.boot.spring.enable;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class TjhEntity {
    private String key;
}
