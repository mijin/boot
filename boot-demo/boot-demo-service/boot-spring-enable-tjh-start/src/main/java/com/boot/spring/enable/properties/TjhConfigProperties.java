package com.boot.spring.enable.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "tjh")
public class TjhConfigProperties {
    private String key;
}
