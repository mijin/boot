package com.boot.kafka.jpa.elasticsearch;

import com.boot.base.common.utils.JsonUtil;
import com.boot.kafka.jpa.elasticsearch.domain.EsEntity;
import com.boot.kafka.jpa.elasticsearch.domain.OrderLog;
import com.boot.kafka.jpa.elasticsearch.domain.SmsLog;
import com.boot.kafka.jpa.elasticsearch.service.ElasticsearchService;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.time.DateUtils;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@SpringBootTest
public class ApplicationTest {

    @Autowired
    ElasticsearchService elasticsearchService;


    @Autowired
    RestHighLevelClient client;

    @Test
    public void main() {
        List<EsEntity> list = new ArrayList<>();
        for (int index = 0; index < 10; index++) {
            Map<String, Object> map = new HashMap<>(3);
            map.put("id", index);
            map.put("uuid", UUID.randomUUID().toString());
            map.put("date", new Date());
            EsEntity<Map<String, Object>> entity = new EsEntity<>(null, map);
            list.add(entity);
        }
        elasticsearchService.insertBatch("test_index_es", list);
    }

    @Test
    public void main2() {
        List<EsEntity> list = new ArrayList<>();
        Date date = new Date();

        for (int index = 0; index < 100; index++) {
            Map<String, Object> map = new HashMap<>(4);
            map.put("id", index);
            map.put("uuid", UUID.randomUUID().toString());
            map.put("date", DateUtils.addMinutes(date, index));
            map.put("times", DateUtils.addMinutes(date, index).getTime());
            EsEntity<Map<String, Object>> entity = new EsEntity<>(null, map);
            list.add(entity);
        }
        elasticsearchService.insertBatch("test_index_es_sort_5", list);
    }

    @Test
    public void main3() {
        List<EsEntity> list = new ArrayList<>();
        Date now = new Date();
        for (int index = 0; index < 5; index++) {
            OrderLog orderLog = new OrderLog();
//            订购时间、手机号码、网别、产品名称、订购编码、订购来源、订购结果、订购接口应答报文、
            orderLog.setOrderTime(DateUtils.addMinutes(now, index));
            orderLog.setPhone("17676767676");
            orderLog.setNetType(5);
            orderLog.setProductName("国庆七天乐");
            orderLog.setPurchaseCode("ACAC01");
            orderLog.setOrderFrom("WINDOW_ORDER");
            orderLog.setSuccess((index % 2) == 1);
            orderLog.setReturnContent("订购成功");
            orderLog.setProvCode("011");
            EsEntity<OrderLog> entity = new EsEntity<>(null, orderLog);
            list.add(entity);
        }
        elasticsearchService.insertBatch("test_index_es_sort_5", list);
    }

    /**
     * 订购日志采集
     */
    @Test
    public void main4() {
        List<EsEntity> list = new ArrayList<>();
        Date now = new Date();
        for (int index = 0; index < 10; index++) {
            OrderLog orderLog = new OrderLog();
//            时间、手机号码、短信内容、方向（下行回复）
            orderLog.setOrderTime(DateUtils.addMinutes(now, index));
            orderLog.setPhone("0D08F384B3A453D849D0EFCC6FA79B72");
            orderLog.setOrderFrom("WINDOW_ORDER");
            orderLog.setSuccess((index % 2) == 1);
            orderLog.setProvCode("011");
            orderLog.setInnerReturnMsg("请发送012订购");
            orderLog.setProductName("国庆七天乐");
            orderLog.setPurchaseCode("A1S2C3");
            orderLog.setNetType(1);
            orderLog.setReturnContent("{\"actId\":0,\"actName\":\"\",\"innerReturnMsg\":\"请发送012订购\",\"netType\":0,\"orderFrom\":\"WINDOW_ORDER\",\"orderTime\":\"2020-12-24 11:00:55\",\"phone\":\"17676767676\",\"productId\":0,\"productName\":\"\",\"productPrice\":0,\"provCode\":\"011\",\"purchaseCode\":\"\",\"returnContent\":\"\",\"seqId\":0,\"success\":false}");
            EsEntity<OrderLog> entity = new EsEntity<>(null, orderLog);
            list.add(entity);
        }
        elasticsearchService.insertBatch("log_order_20201224", list);
    }

    /**
     * 订购日志轨迹
     */
    @Test
    public void main5() {
        List<EsEntity> list = new ArrayList<>();
        Date now = new Date();
        for (int index = 0; index < 5; index++) {
            SmsLog smsLog = new SmsLog();
//            时间、手机号码、短信内容、方向（下行回复）
            smsLog.setSendTime(DateUtils.addMinutes(now, index));
            smsLog.setPhone("0D08F384B3A453D849D0EFCC6FA79B72");
            smsLog.setProvCode("010");
            if ((index % 2) != 0) {
                smsLog.setSmsContent("你要订购？");
                smsLog.setSmsType("SMS_TASK_DOWN");
            } else {
                smsLog.setSmsContent("我要订购");
                smsLog.setSmsType("USER_SMS_UP");
            }
            EsEntity<SmsLog> entity = new EsEntity<>(null, smsLog);
            list.add(entity);
        }
        elasticsearchService.insertBatch("log_sms_20201224", list);
    }

    @Test
    public void main05() {
        List<EsEntity> list = new ArrayList<>();
        Date now = new Date();
        for (int index = 0; index < 5; index++) {
            SmsLog smsLog = new SmsLog();
//            时间、手机号码、短信内容、方向（下行回复）
            smsLog.setSendTime(DateUtils.addMinutes(now, index));
            smsLog.setPhone("0D08F384B3A453D849D0EFCC6FA79B72");
            smsLog.setProvCode("011");
            smsLog.setSmsContent("回复日志");
            smsLog.setSmsType("SMS_REPLY_DOWN");

            EsEntity<SmsLog> entity = new EsEntity<>(null, smsLog);
            list.add(entity);
        }
        elasticsearchService.insertBatch("log_sms_20201225", list);
    }


    /**
     *
     */
    @Test
    public void main6() {
        List<EsEntity> list = new ArrayList<>();
        Date now = new Date();
        for (int index = 600; index < 900; index++) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", index);
            EsEntity entity = new EsEntity(null, map);
            list.add(entity);
        }
        elasticsearchService.insertBatch("log_order_page", list);
    }


    /**
     * es 分页只提供上一页下一页的办法，跳页不支持
     */
    @Test
    public void queryPage() {
        SearchSourceBuilder sourceBuilder = SearchSourceBuilder.searchSource();
        sourceBuilder.sort("id", SortOrder.ASC);
        sourceBuilder.size(10);
        List<SearchHit> list1 = elasticsearchService.queryPage(new String[]{"log_order_page"}, sourceBuilder);
        log.info("---------------------第一页");
        for (SearchHit jsonObject : list1) {
            log.info("{}", jsonObject.getSourceAsMap());
        }
//第二页需要的参数
        Object[] after2 = list1.get(list1.size() - 1).getSortValues();

        SearchSourceBuilder sourceBuilder2 = SearchSourceBuilder.searchSource();
        sourceBuilder2.sort("id", SortOrder.ASC);
        sourceBuilder2.size(10);
        sourceBuilder2.searchAfter(after2);

        List<SearchHit> list2 = elasticsearchService.queryPage(new String[]{"log_order_page"}, sourceBuilder2);
        log.info("---------------------第二页");
        for (SearchHit jsonObject : list2) {
            log.info("{}", jsonObject.getSourceAsMap());
        }

//第三页需要的参数
        Object[] after3 = list2.get(list2.size() - 1).getSortValues();
        SearchSourceBuilder sourceBuilder3 = SearchSourceBuilder.searchSource();
        sourceBuilder3.sort("id", SortOrder.ASC);
        sourceBuilder3.size(10);
        sourceBuilder3.searchAfter(after3);
        List<SearchHit> list3 = elasticsearchService.queryPage(new String[]{"log_order_page"}, sourceBuilder3);
        log.info("---------------------第三页");
        for (SearchHit jsonObject : list3) {
            log.info("{}", jsonObject.getSourceAsMap());
        }

//        回到第二页

        SearchSourceBuilder sourceBuilder2_1 = SearchSourceBuilder.searchSource();
        sourceBuilder2_1.sort("id", SortOrder.ASC);
        sourceBuilder2_1.size(10);
        sourceBuilder2_1.searchAfter(after2);

        List<SearchHit> list2_1 = elasticsearchService.queryPage(new String[]{"log_order_page"}, sourceBuilder2);
        log.info("---------------------回到第二页");
        for (SearchHit jsonObject : list2_1) {
            log.info("{}", jsonObject.getSourceAsMap());
        }

    }


    @Test
    public void query() {
        SearchSourceBuilder sourceBuilder = SearchSourceBuilder.searchSource();
        sourceBuilder.sort("sendTime.keyword", SortOrder.ASC);
        List<JSONObject> list = elasticsearchService.search("log_order_20221209", sourceBuilder, JSONObject.class);
        for (JSONObject jsonObject : list) {
            log.info("{}", jsonObject);
        }
    }

    /**
     * https://www.elastic.co/guide/en/elasticsearch/reference/current/dynamic-field-mapping.html#date-detection
     * 按照时间排序  必须要是2020/12/13 17:53:48时间格式
     */
    @Test
    public void query2() {
        SearchSourceBuilder sourceBuilder = SearchSourceBuilder.searchSource();
        sourceBuilder.sort("orderTime", SortOrder.DESC);
        List<JSONObject> list = elasticsearchService.search("log_order_20201209", sourceBuilder, JSONObject.class);
        for (JSONObject jsonObject : list) {
            log.info("{}", jsonObject);
        }
    }


    @Test
    public void test1() {
        Map map = JsonUtil.jsonToObject("{\n" +
                "            \"gatewayResult\": 1,\n" +
                "            \"innerReturnMsg\": \"\",\n" +
                "            \"phone\": \"19999991134\",\n" +
                "            \"provCode\": \"017\",\n" +
                "            \"sendResult\": 1,\n" +
                "            \"sendTime\": \"2020-12-11 11:00:04\",\n" +
                "            \"seqId\": 1607655604664,\n" +
                "            \"smsContent\": \"单元测试短信内容\",\n" +
                "            \"smsType\": \"SMS_TASK_DOWN\",\n" +
                "            \"taskId\": 1134,\n" +
                "            \"taskName\": \"单元测试任务名称1134\",\n" +
                "            \"upSmsContent\": \"\",\n" +
                "            \"userResult\": 1\n" +
                "        }", Map.class);

        List<EsEntity> list = new ArrayList<>();
        for (int index = 0; index < 100; index++) {
            map.put("smsType", "SMS_REPLY_DOWN");
            map.put("seqId", (map.get("seqId") + "1") + index);
            map.put("upSmsContent", "我要订购");
            Map e = new HashMap();
            e.putAll(map);
            EsEntity es = new EsEntity(null, e);
            list.add(es);
        }
        elasticsearchService.insertBatch("log_sms_send_20201211", list);
    }

    @Test
    public void test2() throws IOException {
        String format = new SimpleDateFormat("yyyy*").format(new Date());
        log.info(format);
    }
}
