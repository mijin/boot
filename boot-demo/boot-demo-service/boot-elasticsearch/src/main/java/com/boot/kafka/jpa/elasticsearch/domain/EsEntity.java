package com.boot.kafka.jpa.elasticsearch.domain;

public class EsEntity<T> {
    /**
     * es里面document的id
     */
    private String id;

    public EsEntity(String id, T data) {
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private T data;





}
