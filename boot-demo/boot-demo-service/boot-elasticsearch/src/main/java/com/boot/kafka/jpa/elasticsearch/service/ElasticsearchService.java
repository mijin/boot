package com.boot.kafka.jpa.elasticsearch.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.kafka.jpa.elasticsearch.domain.EsEntity;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author weicy4
 * @since 2020/0820
 */
@SuppressWarnings("ALL")
public interface ElasticsearchService {


    /**
     * Description: 判断某个index是否存在
     *
     * @param index index名
     * @return boolean
     */
    boolean indexExist(String index) throws Exception;


    /**
     * Description: 插入/更新一条记录
     *
     * @param index  index
     * @param entity 对象
     */
    void insertOrUpdateOne(String index, EsEntity entity);


    /**
     * Description: 批量插入数据
     *
     * @param index index
     * @param list  带插入列表
     */
    void insertBatch(String index, List<EsEntity> list);


    /**
     * Description: 批量删除
     *
     * @param index  index
     * @param idList 待删除列表
     */
    <T> void deleteBatch(String index, Collection<T> idList);

    /**
     * Description: 搜索
     *
     * @param index   index
     * @param builder 查询参数
     * @param c       结果类对象
     * @return java.util.ArrayList
     */
    <T> List<T> search(String index, SearchSourceBuilder builder, Class<T> c);
    <T> List<SearchHit> queryPage(String[] indexs, SearchSourceBuilder builder );


    <T> IPage<T> searchPage(String index, SearchSourceBuilder builder, Class<T> c, Page<T> page);

    /**
     * Description: 删除index
     *
     * @param index index
     * @return void
     */
    void deleteIndex(String index);
    /**
     * Description: delete by query
     *
     * @param index   index
     * @param builder builder
     */
    void deleteByQuery(String index, QueryBuilder builder);

    /**
     * 查询索引的总记录数
     *
     * @param index
     * @return
     */
    long getCountByIndex(String index);

    /**
     * 使用scroll游标遍历索引内容
     * @param index
     * @param scrollTimeOut 单位：毫秒
     * @param batchCount 每次批量获取的数量
     * @param consumer
     * @throws IOException
     */
    void scroll(String index, Long scrollTimeOut, int batchCount, Consumer<SearchHit[]> consumer) throws Exception ;
}
