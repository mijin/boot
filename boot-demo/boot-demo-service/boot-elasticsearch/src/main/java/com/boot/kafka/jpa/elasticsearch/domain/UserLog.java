package com.boot.kafka.jpa.elasticsearch.domain;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 短信收发日志
 * @author zongbo1
 */
@Builder
@Data
public class UserLog implements Serializable {
}
