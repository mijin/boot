package com.boot.kafka.jpa.elasticsearch.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订购日志
 * @author zongbo1
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class OrderLog implements Serializable {


    /**
     * 流水id
     */
    private Long seqId ;
    /**
     * 手机号
     */
    private String phone ;
    /**
     * 手机网别 0:未知   2:2G ;   3:3G ;   4:4G;  5:5G;
     */
    private int netType ;
    /**
     * 订购产品id
     */
    private Long productId;
    /**
     * 订购产品名称
     */
    private String productName ;
    /**
     * 订购产品编码
     */
    private String purchaseCode ;
    /**
     * 订购产品金额
     */
    private BigDecimal productPrice;
    /**
     * 订购业务类型，用来区分订购业务类型，
     * {@link OrderFromEnum#getCode()}
     */
    private String orderFrom;

    /**
     * 活动id【仅orderFrom={@link OrderFromEnum#WINDOW_ORDER}】时有值
     */
    private Long actId;
    /**
     * 活动名称【仅orderFrom={@link OrderFromEnum#WINDOW_ORDER}】时有值
     */
    private String actName ;
    /**
     * 省份
     */
    private String provCode ;
    /**
     * 订购结果 true为成功  false为失败
     */
    private boolean success;
    /**
     * 订购返回消息（本系统内自定义的消息，非订购接口返回）
     */
    private String innerReturnMsg;
    /**
     * 订购返回数据
     */
    private String returnContent;

    /**
     * 订购时间
     */
    private Date orderTime;

}
