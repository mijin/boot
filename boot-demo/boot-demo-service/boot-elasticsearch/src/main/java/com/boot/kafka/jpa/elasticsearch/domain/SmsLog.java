package com.boot.kafka.jpa.elasticsearch.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信收发日志
 *
 * @author zongbo1
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SmsLog implements Serializable {
    /**
     * 流水id
     */
    private Long seqId;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 省份
     */
    private String provCode;
    /**
     * 短信内容
     */
    private String smsContent;
    /**
     * 短信类型
     * {@link SmsTypeEnum}
     */
    private String smsType;
    /**
     * [省份代理返回的报文]
     * 短信类型 = {@link SmsTypeEnum#SMS_TASK_DOWN} 和 {@link SmsTypeEnum#SMS_REPLY_DOWN} 时有值
     */
    private String innerReturnMsg;

    /**
     * [上行短信内容] 可选
     * 短信类型 = {@link SmsTypeEnum#SMS_REPLY_DOWN} 时有值，值为用户上行短信
     */
    private String upSmsContent;

    /**
     * [短信任务id] 默认0
     * 短信类型 = {@link SmsTypeEnum#SMS_TASK_DOWN} 时有值
     */
    private Long taskId;
    /**
     * [短信任务名称] 默认""
     * 短信类型 = {@link SmsTypeEnum#SMS_TASK_DOWN} 时有值
     */
    private String taskName;

    /**
     * [发送结果 -1:未知  0：失败  1：成功 ]  默认-1
     * 短信类型 = {@link SmsTypeEnum#SMS_TASK_DOWN} 和 {@link SmsTypeEnum#SMS_REPLY_DOWN} 时有值
     */
    private Integer sendResult; //wangguan
    /**
     * [网关接收结果 -1:未知  0：失败  1：成功]  默认-1
     * 短信类型 = {@link SmsTypeEnum#SMS_TASK_DOWN} 和 {@link SmsTypeEnum#SMS_REPLY_DOWN} 时有值
     */
    private Integer gatewayResult;
    /**
     * [用户接收结果 -1:未知  0：失败  1：成功]  默认-1
     * 短信类型 = {@link SmsTypeEnum#SMS_TASK_DOWN} 和 {@link SmsTypeEnum#SMS_REPLY_DOWN} 时有值
     */
    private Integer userResult;
    /**
     * 短信发送时间
     */
    private Date sendTime;

    /**
     * 网关返回的sequenceId
     */
    private Integer gatewaySequenceId;

}
