-- 令牌桶限流: 支持预消费, 初始桶是满的
-- KEYS[1]  string  限流的key

-- ARGV[1]  int     桶最大容量
-- ARGV[2]  int     每次添加令牌数
-- ARGV[3]  int     令牌添加间隔(秒)
-- ARGV[4]  int     想获取令牌的数量
-- ARGV[5]  int     是否vip 0-否  1-时
local bucket_capacity = tonumber(ARGV[1])
local add_token = tonumber(ARGV[2])
local add_interval = tonumber(ARGV[3])
local expect_count = tonumber(ARGV[4])
local is_vip = tonumber(ARGV[5])
-- 当前时间（s）
local now = redis.call('TIME')[1]

-- 保存上一次更新桶的时间的key
local LAST_TIME_KEY = "{"..KEYS[1].."}_time";
-- 获取当前桶中令牌数
local token_cnt = redis.call("get", KEYS[1])
-- 最终返回值，-1代表未获取到令牌
local returnCount = -1

return 0