package com.boot.limit.factory;

public interface SmsLimitService {
    /**
     * @param provCode 省份
     * @param expectCount 想获取的令牌
     * @param isVip 是否vip
     * @return
     */
    Object consumeTokenCount(String provCode, int expectCount, boolean isVip);

}
