package com.boot.limit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 令牌桶
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class BootRedisRateLimter {
    public static void main(String[] args) {
        SpringApplication.run(BootRedisRateLimter.class, args);
    }
}
