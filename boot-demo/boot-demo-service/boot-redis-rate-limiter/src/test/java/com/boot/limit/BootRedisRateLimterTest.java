package com.boot.limit;

import com.boot.limit.factory.SmsLimitService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class BootRedisRateLimterTest {
    @Autowired
    private SmsLimitService smsLimitService;

    @Test
    public void main() throws InterruptedException {
        for (int index = 0; index < 10; index++) {
            Thread.sleep(5_00);
            Object tokenCount = smsLimitService.consumeTokenCount("110", 30, true);
            log.info("第{}次获取，获取到令牌数量为{}", index, tokenCount);
        }
    }
}