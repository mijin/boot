package com.boot.socket5.server;

import com.boot.socket5.server.compont.SocksServerOneThread;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

//@SpringBootApplication
public class Socket5ServerApplication {


    public static void main(String[] args) throws IOException {
//        SpringApplication.run(Socket5ServerApplication.class, args);
        int port = 1080;
        ServerSocket serverSocket = new ServerSocket(port, 1);
        if (null == serverSocket) {
            System.out.println("Socket Porxy exit");
        } else {
            System.out.println("Socket Porxy running on " + port);
        }
        while (true) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
                SocksServerOneThread socksServerOneThread = new SocksServerOneThread(socket, true, false, "user", "123456", true);
                Executors.newFixedThreadPool(1).submit(socksServerOneThread);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
