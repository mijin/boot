package com.boot.socket5.client;

import com.boot.socket5.client.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class Socket5ClientApplicationTest {
    @Test
    void main1() {

        String url="http://www.baidu.com/";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "text/html, */*; q=0.01");
        headers.put("Accept-Encoding", "gzip, deflate, sdch");
        headers.put("Accept-Language", "zh-CN,zh;q=0.8");
        headers.put("Referer", url);
        headers.put("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:46.0) Gecko/20100101 Firefox/46.0");

        String result= HttpClientUtil.getWithProxy(url, headers, "UTF-8");
        log.info(result);

    }

}