package com.boot.kafka.jpa.elasticsearch.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

@Configuration
public class WebConfig {
    @Value("${spring.data.elasticsearch.url}")
    private String url;
    @Bean
    RestHighLevelClient elasticsearchClient(@Value("${spring.data.elasticsearch.url}")String url) {
        ClientConfiguration configuration = ClientConfiguration.builder()
                .connectedTo(url)
                //.withConnectTimeout(Duration.ofSeconds(5))
                //.withSocketTimeout(Duration.ofSeconds(3))
                //.useSsl()
                //.withDefaultHeaders(defaultHeaders)
                //.withBasicAuth(username, password)
                // ... other options
                .build();
        RestHighLevelClient client = RestClients.create(configuration).rest();
        return client;
    }
}
