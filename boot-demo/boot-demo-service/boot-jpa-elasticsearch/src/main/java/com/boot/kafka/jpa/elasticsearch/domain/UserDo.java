package com.boot.kafka.jpa.elasticsearch.domain;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@Document(indexName = "users", type = "user")
public class UserDo {
    @Id
    private int id;
    private String username;
    private String password;
    private int age;


}