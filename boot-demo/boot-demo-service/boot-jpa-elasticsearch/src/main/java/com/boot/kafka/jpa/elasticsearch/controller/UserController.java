package com.boot.kafka.jpa.elasticsearch.controller;

import com.boot.kafka.jpa.elasticsearch.dao.UserDao;
import com.boot.kafka.jpa.elasticsearch.domain.UserDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    UserDao userDao;

    @RequestMapping("/addUser")
    public String addUserDo(String username, String password, Integer age) {
        UserDo user = new UserDo();
        user.setUsername(username);
        user.setPassword(password);
        user.setAge(age);
        return String.valueOf(userDao.save(user).getId());// 返回id做验证
    }

    @RequestMapping("/deleteUser")
    public String deleteUserDo(Integer id) {
        userDao.deleteById(id);
        return "Success!";
    }

    @RequestMapping("/updateUser")
    public String updateUserDo(Integer id, String username, String password, Integer age) {
        UserDo user = new UserDo();
        user.setId(id);
        user.setUsername(username);
        user.setPassword(password);
        user.setAge(age);
        return String.valueOf(userDao.save(user).getId());// 返回id做验证
    }

    @RequestMapping("/getUser")
    public UserDo getUserDo(Integer id) {
        return userDao.findById(id).get();
    }

    @RequestMapping("/getAllUsers")
    public Iterable<UserDo> getAllUserDos() {
        return userDao.findAll();
    }

}