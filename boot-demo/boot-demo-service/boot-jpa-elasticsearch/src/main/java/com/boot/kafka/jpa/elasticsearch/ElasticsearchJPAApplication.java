package com.boot.kafka.jpa.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication( exclude = {DataSourceAutoConfiguration.class})
public class ElasticsearchJPAApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchJPAApplication.class, args);
    }
}
