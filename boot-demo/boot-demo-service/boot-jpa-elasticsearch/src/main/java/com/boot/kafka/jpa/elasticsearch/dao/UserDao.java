package com.boot.kafka.jpa.elasticsearch.dao;

import com.boot.kafka.jpa.elasticsearch.domain.UserDo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UserDao  extends ElasticsearchRepository<UserDo, Integer> {
}