package com.boot.kafka.jpa.elasticsearch.dao;

import com.boot.kafka.jpa.elasticsearch.ApplicationTest;
import com.boot.kafka.jpa.elasticsearch.domain.UserDo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@Slf4j
class UserDaoTest  extends ApplicationTest {
    @Autowired
    UserDao userDao;

    @Test
    public void test() {
        final PageRequest pageRequest = PageRequest.of(1, 10);
        final Page<UserDo> all1 = userDao.findAll(pageRequest);
        for (UserDo userDo : all1) {
            log.info("{}",userDo);
        }
    }

}