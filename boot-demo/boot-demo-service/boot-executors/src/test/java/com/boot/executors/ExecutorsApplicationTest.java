package com.boot.executors;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
//@SpringBootTest
public class ExecutorsApplicationTest {

    /**
     * 定时线程
     */
    @Test
    public void test01() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
        /**
         *initiaDelayu 线程池初始化后，多久执行第一次
         */
        scheduledExecutorService.scheduleWithFixedDelay(new Task(), 0, 2, TimeUnit.SECONDS);
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test02() {
        Queue<String> queue = new LinkedList<String>();
        //添加元素
        queue.offer("a");
        queue.offer("b");
        queue.offer("c");
        queue.offer("d");
        queue.offer("e");
        for (String q : queue) {
            log.info(q);
        }
//        消费一个
        String poll = queue.poll();
        log.info("poll={}", poll);
        for (String q : queue) {
            log.info(q);
        }
    }

    class Task implements Runnable {

        private long lastTime = System.currentTimeMillis();

        @Override
        public void run() {
            log.info("current Time {}s", (System.currentTimeMillis() - lastTime) / 1000);
            lastTime = System.currentTimeMillis();
            log.info(Thread.currentThread().getName() + "正在执行");
            try {
                int lo = 1 / 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}