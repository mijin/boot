spring 注入多个实例
未实现
通过yml配置文件去生成实体类，因为参数无法注入，猜想是@Value参数顺序导致的
如果某一个Bean实现了BeanDefinitionRegistryPostProcessor或者ImportBeanDefinitionRegistrar接口，
那我们在这个类中使用@Autowired或者@Value注解，我们会发现失效了。
原因是，spring容器执行接口的方法时，此时还没有去解析@Autowired或者@Value注解。
如果我们要使用获取配置文件属性，可以通过原始方式，直接用IO读取配置文件，
然后得到Properties对象，然后再获取配置值。
————————————————
版权声明：本文为CSDN博主「lichuangcsdn」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/lichuangcsdn/article/details/89930945