package com.boot.spring.entity;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class UserEntity {
    private Integer id;
    private String name;
}
