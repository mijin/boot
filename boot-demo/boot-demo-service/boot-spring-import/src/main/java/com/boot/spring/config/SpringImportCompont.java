package com.boot.spring.config;

import com.boot.spring.entity.SingleEntity;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class SpringImportCompont implements ImportBeanDefinitionRegistrar {


    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        Integer max = 5;
        for (int index = 0; index <= max; index++) {
            BeanDefinition beanDefinition = new GenericBeanDefinition();
            beanDefinition.setBeanClassName(SingleEntity.class.getName());
            MutablePropertyValues values = beanDefinition.getPropertyValues();
            values.addPropertyValue("id", index);
            //这里注册bean
            registry.registerBeanDefinition(SingleEntity.class.getName()+index, beanDefinition );
        }
    }
}
