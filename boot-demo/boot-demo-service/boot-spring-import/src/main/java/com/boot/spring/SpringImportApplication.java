package com.boot.spring;
import com.boot.spring.config.SpringImportCompont;
import com.boot.spring.enable.TjhEntity;
import com.boot.spring.entity.SingleEntity;
import com.boot.spring.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(value = {"com.boot.base.web", "com.boot.spring"})
@Import(SpringImportCompont.class)
public class SpringImportApplication implements InitializingBean {

    @Autowired
    private  List<SingleEntity>  list;


    @Bean(name = "users",value = "users")
    @ConfigurationProperties(prefix = "tjh.prop")
    Map<Integer,UserEntity> users(){
        return new HashMap<>();
    }

    @Autowired
    TjhEntity tjhEntity;

    public static void main(String[] args) {
        SpringApplication.run(SpringImportApplication.class, args);
        log.info(SpringImportApplication.class.getName()+"2...");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (SingleEntity singleEntity : list) {
            log.info("{}",singleEntity.getId());
        }
        for (Map.Entry<Integer, UserEntity> entry : users().entrySet()) {
            log.info("key={},val={},valClz={}",entry.getKey(),entry.getValue(),entry.getValue().getClass().getName());
        }
        log.info("tjhEntity={}",tjhEntity);
    }
}
