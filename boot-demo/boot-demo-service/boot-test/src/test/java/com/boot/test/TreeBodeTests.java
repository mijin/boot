package com.boot.test;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 二叉树遍历
 */
@Slf4j
public class TreeBodeTests {

    @Test
    public void test() {
        List<Integer> list = Arrays.asList(1, 3, 5, -7, -9);
        TreeNode root = new TreeNode(0);
        for (Integer code : list) {
            root.addNode(new TreeNode(code) );
        }
        log.info("{}",root);
    }

    @ToString
    static class TreeNode {
        private Integer data;
        TreeNode left;
        TreeNode right;

        public TreeNode(Integer data) {
            this.data = data;
        }

        /**
         * 添加节点
         *
         * @param node
         */
        public void addNode(TreeNode node) {
            //小于父节点
            if (node.data < this.data) {
                if (this.left != null) {
                    this.left.addNode(node);
                } else {
                    this.left = node;
                }
            }
            //大于等于父节点
            if (node.data >= this.data) {
                if (this.right != null) {
                    this.right.addNode(node);
                } else {
                    this.right = node;
                }
            }
        }
    }


}
