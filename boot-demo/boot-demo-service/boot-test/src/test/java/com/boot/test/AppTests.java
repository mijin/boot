package com.boot.test;

import com.boot.base.web.UploadCompont;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
public class AppTests {

    @Autowired
    private UploadCompont uploadCompont;

    @Test
    public void test06() {

    }

    @Test
    public void test02() {
        Queue<String> queue = new LinkedList<String>();
        //添加元素
        queue.offer("a");
        queue.offer("b");
        queue.offer("c");
        queue.offer("d");
        queue.offer("e");

        String s = null;
        while ((s = queue.poll()) != null) {
            log.info("元素：{}", s);
        }
    }

    @Test
    public void test03() {
        Queue<String> queue = getFits();

        String s = null;
        while ((s = queue.poll()) != null) {
            String title = getTitle(s);
            log.info("新名字：{}", title);
        }
    }



    /**
     * 优化空间，可以使用线程池+多线程发送
     */
    @SneakyThrows
    @Test
    public void test01() {
        long millis = System.currentTimeMillis();
        Integer errorTimes = 0;
        String url = "http://www.imxingzhe.com/api/v4/upload_fits";
        Integer device = 10;
        Integer sport = 3;
        String cookie = "Hm_lvt_7b262f3838ed313bc65b9ec6316c79c4=1637826687; rd=pce5; csrftoken=F0D2IHKX3yUlWPeLxlsVnQSUphgzmoM0; sessionid=7ozfd6huelfai8ky815n6iylq4r1hkec; Hm_lpvt_7b262f3838ed313bc65b9ec6316c79c4=1637826711";
        RestTemplate restTemplate = new RestTemplate();

        Queue<String> queue = getFits();
        int sum = queue.size();
        String path = null;
        while ((path = queue.poll()) != null) {
            String title = getTitle(path);
            long start = System.currentTimeMillis();
            String xinzheupload = uploadCompont.xinzheupload(restTemplate, url, title, device, sport, path, cookie);
            if (xinzheupload != null) {
                log.info("path={},title={} 上传文件返回值：{}", path,title, xinzheupload);
                FileUtils.forceDeleteOnExit(new File(path));
            } else {
//                上传失败，再次放入列队里面
                errorTimes++;
                queue.offer(path);
                log.error("path={} 上传失败，再次放入列队里面", path);
            }
            log.info("总数={},剩余={},本次上传耗时={}ms", sum, queue.size(), System.currentTimeMillis() - start);
        }
        log.info("上传文件总数={},上传错误次数={},全程耗时={}ms", sum, errorTimes,System.currentTimeMillis() - millis);
    }


    /**
     * 获取要上传的fit文件
     *
     * @return
     */
    Queue<String> getFits() {
        File file = new File("/Users/tjh/Desktop/buf");
        List<String> list = Arrays.asList(file.listFiles()).stream().filter(e -> e.getName().contains(".fit")).map(e -> e.getPath()).collect(Collectors.toList());
        return new LinkedList<>(list);
    }

    /**
     * 根据路径获取名称
     *
     * @param path
     * @return
     */
    String getTitle(String path) {
        File file = new File(path);
        if (file.getName().length() < 32) {
            log.info("非法文件={}", file.getName());
        }
        String souce = file.getName().substring(12, 31);
        try {
            Date parse = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").parse(souce);
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(parse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return souce;
    }
}
