package com.boot.test;

import com.boot.test.stream.KiteFunction;
import com.boot.test.util.DateUtils;
import com.boot.test.util.Mp4ParserUtils;
import com.boot.test.util.SayService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class UtilsTests {
    @Test
    public void test02() {
        KiteFunction<LocalDateTime, String, String> functionDateFormat = DateUtils::DateFormat;
        String yyyy = functionDateFormat.run(LocalDateTime.now(), "yyyyMMddHHmmss");
        log.info(yyyy);


        KiteFunction<LocalDateTime, String, String> functionDateFormat2 = (LocalDateTime dateTime, String partten) -> {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(partten);
            return dateTime.format(dateTimeFormatter);
        };
        String yyyy2 = functionDateFormat2.run(LocalDateTime.now(), "yyyyMMddHHmmss");
        log.info(yyyy2);


    }


    @Test
    public void test01() {
        String a = ("a");
        String b = "b";

        SayService sayService = msg -> {
            log.info(msg);
        };

        SayService sayService1 = this::cc;
        sayService1.msg("11");

    }

    @Test
    public void test03() throws IOException {
        URL url = new URL("https://img-blog.csdnimg.cn/20191204234604892.png");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//设置超时间为3秒
        conn.setConnectTimeout(3 * 1000);
//得到输入流
        InputStream inputStream = conn.getInputStream();

        String str = imageToBase64Str(inputStream);
        log.info(str);


    }

    /**
     * 有点像递归
     *
     * @throws IOException
     */
    @Test
    public void test04() throws IOException {
        List<BigDecimal> list = new ArrayList<>();
        for (int index = 0; index < 5; index++) {
            list.add(new BigDecimal(index));
        }
        BigDecimal reduce = list.stream().reduce(BigDecimal.ZERO, (a, b) -> {
                    log.info("a={},b={}", a, b);
                    BigDecimal add = a.add(b);
                    return add;
                }
        );
        log.info("reduce={}", reduce);
    }

    @Test
    public void test05() throws IOException {
        List<SuperAndSon> list = new ArrayList<>();
        for (int index = 0; index < 5; index++) {
            list.add(new SuperAndSon(index + 1, index, new ArrayList<>()));
        }
    }

    private void get(Integer pid, List<SuperAndSon> list) {
        for (SuperAndSon andSon : list) {
            if (pid.equals(andSon.getId())) {
            }
        }
    }


    @Data
    @AllArgsConstructor
    static class SuperAndSon {
        private Integer id;
        private Integer pid;
        private List<SuperAndSon> list;
    }

    public static String imageToBase64Str(InputStream inputStream) {
        byte[] data = null;
        try {
            data = new byte[inputStream.available()];
            inputStream.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // 加密
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }


    public void cc(String a) {
        log.info(a);
    }


    /**
     * 合并视频
     */
    @Test
    public void mergeVideo() {
        long start = System.currentTimeMillis();
        List<String> strings = Arrays.asList("ANMR0009.mp4", "ANMR0010.mp4", "ANMR0011.mp4", "ANMR0012.mp4", "ANMR0013.mp4");
        List<String> collect = strings.stream().map(str -> ("F:\\DCIM\\100MEDIA\\" + str)).collect(Collectors.toList());
        File file = new File("E:\\video\\d.mp4");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Mp4ParserUtils.mergeVideo(collect, file);
        log.info("耗时约：{}", System.currentTimeMillis() - start);
    }


    /**
     * 行者上传fit文件
     */
    @Test
    public void fitUpload() {
        final String filePath = "F:";
        final String fileName = "testFile.txt";
        final String url = "http://localhost:8080/file/upload";

        RestTemplate restTemplate = new RestTemplate();

        //设置请求头
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("multipart/form-data");
        headers.setContentType(type);

        //设置请求体，注意是LinkedMultiValueMap
        FileSystemResource fileSystemResource = new FileSystemResource(filePath + "/" + fileName);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", fileSystemResource);
        form.add("filename", fileName);

        HttpEntity<MultiValueMap<String, Object>> files = new HttpEntity<>(form, headers);

        String s = restTemplate.postForObject(url, files, String.class);


    }

    /**
     * @param out 输出
     * @param ins 输入
     */
    public static void merge(File out, List<File> ins) {
        try {
            RandomAccessFile target = new RandomAccessFile(out, "rw");

            for (File in : ins) {
                RandomAccessFile src = new RandomAccessFile(in, "r");
                byte[] bytes = new byte[10240];//每次读取字节数
                int len = -1;
                while ((len = src.read(bytes)) != -1) {
                    target.write(bytes, 0, len);//循环赋值
                }
                log.info("{}文件合并完成", in.getName());
                src.close();
            }
            target.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test06() {
        log.info("val={}",1&2);
        log.info("val={}",2&2);
        log.info("val={}",1&1);
        log.info("val={}",110 & 100);
    }


    @Test
    public void test07() {
        Integer init = 0;
        long l = System.currentTimeMillis();
        for (int index = 0; index < 1000000000; index++) {
            int i = index & init;
        }
        log.info("消耗时间&={}",System.currentTimeMillis()-l);

        l = System.currentTimeMillis();
        for (int index = 0; index < 1000000000; index++) {
            int i =  init.compareTo(index);
        }
        log.info("消耗时间={}",System.currentTimeMillis()-l);

        log.info("val={}",1&2);
        log.info("val={}",2&2);
        log.info("val={}",1&1);
        log.info("val={}",110 & 100);
    }
}
