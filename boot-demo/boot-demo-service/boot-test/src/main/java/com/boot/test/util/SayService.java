package com.boot.test.util;

@FunctionalInterface
public interface SayService {

    void msg(String msg);
}
