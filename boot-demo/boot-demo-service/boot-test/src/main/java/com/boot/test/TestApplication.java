package com.boot.test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.boot.base.web")
public class TestApplication {



    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
        System.out.println(TestApplication.class.getName()+"2...");
    }
}
