package com.boot.test.stream;

@FunctionalInterface
public interface KiteFunction<T, R, S> {

    /**模拟
     * @param t
     * @param s
     * @return
     */
    R run(T t,S s);

}
