package com.boot.test.controller;

import com.boot.base.common.utils.AjaxResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HelloController {
    @RequestMapping("/index")
    public Object index(String code) {
        return System.currentTimeMillis() + "" + System.currentTimeMillis() + code;
    }


    /**
     * 获取图书列表
     *
     * @param code
     * @return
     */
    @RequestMapping("/book/list")
    public Object list(String code) {
        List<Map<String, Object>> list = new ArrayList<>();

        for (int index = 1; index < 10; index++) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", index);
            map.put("name", "199" + index + "编年史");
            map.put("image", "https://cdn.v2ex.com/avatar/6ebb/69ff/8042_normal.png");
            list.add(map);
        }

        return AjaxResult.success().add("data", list);
    }

    /**图书详情
     * @param id
     * @return
     */
    @RequestMapping("/book/detail")
    public Object detail(String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("name", "199" + id + "编年史");
        map.put("content", "https://cdn.v2ex.com/avatar/6ebb/69ff/8042_normal.png");
        return AjaxResult.success().add("data", map);
    }
}
