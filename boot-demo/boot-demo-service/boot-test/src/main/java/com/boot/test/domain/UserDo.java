package com.boot.test.domain;

import lombok.Data;

import java.util.Objects;

@Data
public class UserDo {
    private int id;
    private String name;

    public UserDo(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDo userDo = (UserDo) o;
        return id == userDo.id ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
