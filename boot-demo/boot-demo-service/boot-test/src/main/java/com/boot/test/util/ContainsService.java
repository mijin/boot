package com.boot.test.util;

@FunctionalInterface
public interface ContainsService {
    /**
     * a 里面是否有 b
     * @param a
     * @param b
     * @return
     */
    boolean contains(String a, String b);
}
