package com.boot.tomcatdemo.controller;

import org.apache.catalina.util.ServerInfo;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
public class VersionController {

    @RequestMapping("/spring/version.mis")
    public Object spring() {
        return SpringVersion.class.getPackage();
    }

    @RequestMapping("/tomcat/version.mis")
    public Object tomcat() {
        Map<String,Object> map = new HashMap<>();
        map.put("Serverversion",ServerInfo.getServerInfo());
        map.put("Serverbuilt",ServerInfo.getServerBuilt());
        map.put("Servernumber",ServerInfo.getServerNumber());
        map.put("OSName",System.getProperty("os.name"));
        map.put("OSVersion",System.getProperty("os.version"));
        map.put("Architecture",System.getProperty("os.arch"));
        map.put("JVMVersion",System.getProperty("java.runtime.version"));
        map.put("JVMVendor",System.getProperty("java.vm.vendor"));
        map.put("package", ServerInfo.class.getPackage());
        return map;
    }
}
