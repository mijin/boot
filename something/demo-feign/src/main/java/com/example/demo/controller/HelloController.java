package com.example.demo.controller;

import com.example.demo.intfaces.FeignHttp;
import com.example.demo.intfaces.UserFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootVersion;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloController {

    @Autowired
    private FeignHttp feignHttp;
    @GetMapping("/get")
    public Object get(){
        log.error("获取数据");
        return SpringBootVersion.class.getPackage();
    }

    @GetMapping("/feign")
    public Object feign(){
        return feignHttp.get();
    }


}
