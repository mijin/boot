package com.example.demo.intfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "userService",url = "${ip}:${port}/${base_url}")
@FeignClient(name = "userService",url = "10.112.52.172:8856/webservice/client")
public interface UserFeign {

    @GetMapping("/user/{userId}")
    String getUserInfo (@PathVariable String userId);
}


