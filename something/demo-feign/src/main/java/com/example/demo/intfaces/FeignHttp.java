package com.example.demo.intfaces;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "1userService",url = "http://localhost:8080")
public interface FeignHttp {

    @GetMapping("/get")
    String get();
}
