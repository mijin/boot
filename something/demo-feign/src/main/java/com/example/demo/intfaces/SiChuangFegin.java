package com.example.demo.intfaces;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "siChuangService",url = "http://localhost:8080")
public interface SiChuangFegin {

    @PostMapping("/ability/210001")
    String get();
}
