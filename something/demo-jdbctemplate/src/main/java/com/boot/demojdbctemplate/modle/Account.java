package com.boot.demojdbctemplate.modle;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Account {
    public Account(Long id) {
        this.id = id;
    }
    public Account( ) {
    }
    private Long id;
    private String name;
    private BigDecimal banlance;
    private Date createDate ;
    private String remark ;
}
