package com.boot.demojdbctemplate.service.impl;

import com.boot.demojdbctemplate.dao.AccountDao;
import com.boot.demojdbctemplate.modle.Account;
import com.boot.demojdbctemplate.service.AccountService;
import com.boot.demojdbctemplate.service.abs.AbsAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public class AccountServiceImpl extends AbsAccount implements AccountService  {
    @Autowired
    private AccountDao accountDao;


    @Transactional
    @Override
    public int save(Account account)   {

        super.asCall(  account);
        return 1;
    }

    @Override
    public int update(Account account) {
        return accountDao.update(account);
    }

    @Override
    public List<Account> list() {
        return accountDao.list();
    }

    @Override
    public Account queryForObject(Account account) {
        return accountDao.queryForObject(account);
    }

    /**
     * 转账
     *
     * @param sendId  发送人
     * @param accept  收账人
     * @param decimal 金额
     * @return
     */
    @Transactional
    @Override
    public List<Account> transfer(Long sendId, Long accept, BigDecimal decimal) {

        Account account = accountDao.queryForObject(new Account(sendId));
        account.setBanlance( account.getBanlance().subtract(decimal));
        accountDao.update(account);

        Account a = accountDao.queryForObject(new Account(accept));
        a.setBanlance( a.getBanlance().add(decimal));
        accountDao.update(a);

        if(true){
            throw  new RuntimeException("必然发生异常");
        }


        return accountDao.list();
    }

    @Override
    public void call(Account account) {
        log.info("{}","回调");
        accountDao.save(account);
        int num= 1/0;
    }
}
