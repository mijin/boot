package com.boot.demojdbctemplate.service;

import com.boot.demojdbctemplate.modle.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {
    int save(Account account) ;

    int update(Account account);


    List<Account> list();

    Account queryForObject(Account account);


    /**
     * 转账
     * @param sendId 发送人
     * @param accept 收账人
     * @param decimal 金额
     * @return
     */
    List<Account> transfer(Long sendId, Long accept, BigDecimal decimal);
}
