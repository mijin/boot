package com.boot.demojdbctemplate.utils;

import com.boot.demojdbctemplate.modle.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class RowMappers {
    public final static RowMapper<Account> AccountRowMapper = new BeanPropertyRowMapper<Account>(Account.class);

}
