package com.boot.demojdbctemplate.utils;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.*;

@Slf4j
public class FieldUtil {

    /**
     * 反射获取参数
     * @param obj
     * @return
     */
    public static Object[] params(Object obj)  {
        Map<String,Object> map = new TreeMap<>();
        Class classz =  obj.getClass();
        for (Field f: classz.getDeclaredFields() ) {
            f.setAccessible(true); // 设置私有属性是可以访问的
            Object o = null;

            try {
                o = f.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if(o!=null){
                map.put(f.getName(),o);
            }
        }
        return   map.values().toArray();
    }
}
