package com.boot.demojdbctemplate.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:other.properties")
public class Properties {
    @Autowired
    private Environment env;



    public Environment getEnv() {
        return env;
    }

}
