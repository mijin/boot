package com.boot.demojdbctemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class DemoJdbctemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoJdbctemplateApplication.class, args);
    }

}

