package com.boot.demojdbctemplate.controller;

import com.boot.demojdbctemplate.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class AccountController {
    @Autowired
    private AccountService accountService;

    @RequestMapping("/z")
    public Object tr(){
     return     accountService.transfer(1L,2L,new BigDecimal(50));
    }
    @RequestMapping("/list")
    public Object list(){
     return     accountService.list();
    }
}
