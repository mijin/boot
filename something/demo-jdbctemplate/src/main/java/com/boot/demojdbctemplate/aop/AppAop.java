package com.boot.demojdbctemplate.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;

@Slf4j
@Aspect
@Component
class AppAop {

    @Pointcut("execution(* org.springframework.jdbc.core.JdbcTemplate.*(..))")
    public void pointCutAt1() {
    }


    @Around("pointCutAt1()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs();
        log.info("sql:{}", args[0]);
        for (Object arg : args) {
            if (arg.getClass().isArray()) {
                log.info("data:{}", StringUtils.arrayToCommaDelimitedString(Arrays.asList(arg).toArray()));
            }
        }
        return pjp.proceed();
    }
}
