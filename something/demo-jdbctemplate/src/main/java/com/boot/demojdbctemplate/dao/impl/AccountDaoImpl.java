package com.boot.demojdbctemplate.dao.impl;

import com.boot.demojdbctemplate.dao.AccountDao;
import com.boot.demojdbctemplate.jdbc.JDBCTemplate;
import com.boot.demojdbctemplate.modle.Account;
import com.boot.demojdbctemplate.utils.FieldUtil;
import com.boot.demojdbctemplate.utils.RowMappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDaoImpl implements AccountDao {
    @Autowired
    private JDBCTemplate jdbcTemplate;
    @Override
    public int save(Account a)   {
        String sql = "INSERT INTO `account` (`banlance`, `create_date`, `name`, `remark`) VALUES (?,?,?,?)";
        return jdbcTemplate.update(sql,FieldUtil.params(a));
    }

    @Override
    public int update(Account a) {
        String sql = "UPDATE `account` SET `banlance`=? WHERE (`id`=?)";
        return jdbcTemplate.update(sql,new Object[]{a.getBanlance(),a.getId() });
    }

    @Override
    public List<Account> list() {

        return jdbcTemplate.getList("select  *  from account", Account.class);
    }

    @Override
    public Account queryForObject(Account account) {
         return jdbcTemplate.queryForObject("select  *  from account where id = ?" ,RowMappers.AccountRowMapper,account.getId());
    }
}
