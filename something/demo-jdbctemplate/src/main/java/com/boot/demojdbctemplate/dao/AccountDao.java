package com.boot.demojdbctemplate.dao;

import com.boot.demojdbctemplate.modle.Account;

import java.util.List;

public interface AccountDao {

    int save(Account account)  ;

    int update(Account account);

    List<Account> list();

    Account queryForObject(Account account);
}
