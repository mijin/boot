package com.boot.demojdbctemplate.service;

import com.boot.demojdbctemplate.DemoJdbctemplateApplicationTests;
import com.boot.demojdbctemplate.config.DatasourceConfig;
import com.boot.demojdbctemplate.config.Properties;
import com.boot.demojdbctemplate.modle.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
@Component
public class AccountServiceTest extends DemoJdbctemplateApplicationTests {
    @Autowired
    private AccountService accountService;

    @Test
    public void save() {
        Account a = new Account();
        a.setName("789789");
        a.setBanlance(BigDecimal.ONE);
        a.setCreateDate(new Date());
        a.setRemark("mm");

        accountService.save(a);
    }

    @Test
    public void update() {
        Account a = new Account();
        a.setBanlance(BigDecimal.ONE);
        a.setId(2L);
        accountService.update(a);
    }

    @Test
    public void list() {
        log.info("list:{}", accountService.list());
    }

    @Test
    public void queryForObject() {
        Account a = new Account(2L);
        log.info("queryForObject:{}", accountService.queryForObject(a));
    }


    @Test
    public void transfer() {
        log.info("queryForObject:{}", accountService.transfer(1L, 2L, new BigDecimal(50)));
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void test1() {
        long start = System.currentTimeMillis();

        StringBuilder sb = new StringBuilder("INSERT INTO `account` VALUES ");
        List<String> list = new ArrayList<>();
        for (int index = 0; index < 100; index++) {
            if (index < 99) {
                sb.append("(NULL, '1.00', '2019-12-19 03:53:16', '789789', 'mm') ,");
            }else {
                sb.append("(NULL, '1.00', '2019-12-19 03:53:16', '789789', 'mm')  ");
            }
        }
        jdbcTemplate.update(sb.toString());
        log.info("时间:{}",System.currentTimeMillis() - start );
    }

    @Autowired
    Properties properties;
    @Test
    public void test2() {
        log.info("时间:{}",properties.getEnv() );
    }
}