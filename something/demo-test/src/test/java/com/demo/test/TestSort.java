package com.demo.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSort {
	  Logger log = LoggerFactory.getLogger(this.getClass());

		
		@Test
		public void test1() {
			int[] array = { 2, 8, 5, 6, 10, 5, 4, 6, 11, 15, 3 };
			quickSort(array,0, array.length-1);
			for (int s : array) {
				log.info("{}",s);
			}
		}



		public static void quickSort(int[] arr,int low, int high) {
			if(low<high) {
				int partition = partition(arr,low,high);
				quickSort(arr,low, partition-1);
				quickSort(arr,partition+1, high);
			}
			
		}
		public static int partition(int[] arr,int low,int high) {
			while(low<high) {
				while(arr[high]>=arr[low]&&low<high){
					high--;
				}
				Swap(arr,high,low);
				while(arr[low]<=arr[high]&&low<high) {
					low++;
				}
				Swap(arr,high,low);
			}
			return low;
		}
		public static void Swap(int[] arr,int high,int low) {
			int temp = arr[low];
			arr[low] =arr[high];
			arr[high] = temp;
		}
}
