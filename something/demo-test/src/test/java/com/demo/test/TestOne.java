package com.demo.test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestOne {
    Logger log = LoggerFactory.getLogger(this.getClass());

	
	@Test
	public void test1() {
		LocalDateTime d =LocalDateTime.now();
		log.info("{}",d);
	}
	@Test
	public void test2() {
		Date d = new Date(Long.parseLong("1209050f3133", 16));
		LocalDateTime time = d.toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
 		log.info("{}",time);
	}
}
