package com.demo.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSort2 {
	    private static Logger log = LoggerFactory.getLogger(TestSort2.class);

		
		@Test
		public void test1() {
			int[] array = { 2, 8, 5, 6, 10, 5, 4, 6, 11, 15, 3 };
			sort(array,0,array.length-1);
			for (int s : array) {
				log.info("{}",s);
			}
		}
		
		public static void sort(int[] arr,int low, int high) {
			if( low<high) {
				int flag = getFlag(arr,low,high);
				log.info("==========={}",flag);
				 sort(arr,  low,   flag-1);
				 sort(arr,  flag+1,   high);
			}
		}

		private static int getFlag(int[] arr, int low, int high) {
			while (low < high) {
				while (arr[high] >= arr[low]  &&  low < high) {
					high--;
				}
				Replace(arr,high,low);
				while (arr[low] <= arr[high] && low < high) {
					low++;
				}
				Replace(arr,high,low);
			}
			return low;
		}

		private static void Replace(int[] arr,int high,int low) {
			// TODO Auto-generated method stub
			int temp = arr[low];
			arr[low] =arr[high];
			arr[high] = temp;
		}
		
		
		

}
