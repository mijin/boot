package com.boot;

import com.boot.task.ReturnAsyncTask;
import com.boot.task.VoidAsyncTask;
import com.boot.task.SynchronizationTask;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Future;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAsync //异步需要在启动类上面加上此注解
public class DemoAsyncApplicationTests {
    @Autowired
    private SynchronizationTask synchronizationTask;
    @Autowired
    private VoidAsyncTask voidAsyncTask;
    @Autowired
    private ReturnAsyncTask returnAsyncTask;

    /**
     * @throws Exception
     * 同步调用
     */
    @Test
    public void synchronizationTask() throws Exception {
        long start = System.currentTimeMillis();
        synchronizationTask.first();
        synchronizationTask.second();
        synchronizationTask.thirdly();
        long end = System.currentTimeMillis();
        log.info("同步总时间：{}", (end - start));
    }
    /**
     * @throws Exception
     * 异步调用
     * 主程序在启用之后是没有管理方法是否已经执行完毕，所以异步是没有返回值的，log也就不会被打印出来
     */
    @Test
    public void asyncTask() throws Exception {
        long start = System.currentTimeMillis();
        voidAsyncTask.first();
        voidAsyncTask.second();
        voidAsyncTask.thirdly();
        long end = System.currentTimeMillis();
        log.info("异步总时间：{}", (end - start));
    }

    /**
     * 有返回值的异步调用
     * @throws Exception
     */
    @Test
    public void returnAsyncTask() throws Exception {
        long start = System.currentTimeMillis();
        Future<Long> first = returnAsyncTask.first();
        Future<Long> second = returnAsyncTask.second();
        Future<Long> thirdly = returnAsyncTask.thirdly();

        while(true) {
            if(first.isDone() && second.isDone() && thirdly.isDone()) {
                // 三个任务都调用完成，退出循环等待
                break;
            }
            Thread.sleep(1000);
        }


        long end = System.currentTimeMillis();
        log.info("异步总时间：{}", (first.get()+second.get()+thirdly.get()));
        log.info("returnAsyncTask 耗时：{}", (end - start));
    }

}
