package com.boot.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class SynchronizationTask {
    public static final Random random = new Random();

    public void first() throws Exception {
        log.info("first start");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("firse end，date：{}", (end - start));
    }

    public void second() throws Exception {
        log.info("second start");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("second end，date：{}", (end - start));
    }

    public void thirdly() throws Exception {
        log.info("thirdly start");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("thirdly end，date：{}", (end - start));
    }
}
