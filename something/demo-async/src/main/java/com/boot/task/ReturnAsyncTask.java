package com.boot.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.Future;

@Slf4j
@Component
public class ReturnAsyncTask {
    public static final Random random = new Random();


    /**
     * @return 执行该程序所需时间
     * @throws Exception
     */
    @Async
    public Future<Long> first() throws Exception {
        log.info("first start");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("firse end，date：{}", (end - start));
        return new AsyncResult<>(end - start);
    }

    @Async
    public Future<Long> second() throws Exception {
        log.info("second start");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("second end，date：{}", (end - start));
        return new AsyncResult<>(end - start);
    }

    @Async
    public Future<Long> thirdly() throws Exception {
        log.info("thirdly start");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("thirdly end，date：{}", (end - start));
        return new AsyncResult<>(end - start);
    }
}
