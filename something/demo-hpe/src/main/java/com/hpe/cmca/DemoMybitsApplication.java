package com.hpe.cmca;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@SpringBootApplication
@MapperScan(basePackages = {"com.hpe.cmca.mapper","com.hpe.cmca.interfaces"})
public class DemoMybitsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMybitsApplication.class, args);
    }
}
