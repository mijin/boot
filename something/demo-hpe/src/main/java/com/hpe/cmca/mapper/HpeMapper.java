package com.hpe.cmca.mapper;

import java.util.List;
import java.util.Map;

public interface HpeMapper {

    List<Map<String, Object>> td(Map<String, Object> map);

    List<Map<String, Object>> mysql(Map<String, Object> map);
}
