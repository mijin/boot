package com.hpe.cmca.pojo;

/*
涉敏操作-审计清单
 3.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、操作日志ID、操作类型名称、操作内容、所在地市或部门
 4.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、操作日志ID、操作类型名称、操作内容、所在地市或部门
 5.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、操作日志ID、操作类型名称、操作内容、敏感数据名称、敏感数据范围、敏感数据级别、所在地市或部门
 6.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、操作日志ID、操作类型名称、操作内容、敏感数据名称、敏感数据范围、敏感数据级别、所在地市或部门
 */
public class XXAQ130305And130306Do extends XXAQAbsDetailsDo {
    private String audTrm;
    private String masterAccount;
    private String masterAccountType;
    private String subAccount;
    private String sysDomain;
    private String sysNm;
    private String logId;
    private String toDoTypeNm;
    private String toDoContent;



    //    审计点 5.6
    private String senNm;
    private String senRange;
    private String senType;
    private String senlevel;

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }

    public String getMasterAccount() {
        return masterAccount;
    }

    public void setMasterAccount(String masterAccount) {
        this.masterAccount = masterAccount;
    }

    public String getMasterAccountType() {
        return masterAccountType;
    }

    public void setMasterAccountType(String masterAccountType) {
        this.masterAccountType = masterAccountType;
    }

    public String getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }

    public String getSysNm() {
        return sysNm;
    }

    public void setSysNm(String sysNm) {
        this.sysNm = sysNm;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getToDoTypeNm() {
        return toDoTypeNm;
    }

    public void setToDoTypeNm(String toDoTypeNm) {
        this.toDoTypeNm = toDoTypeNm;
    }

    public String getToDoContent() {
        return toDoContent;
    }

    public void setToDoContent(String toDoContent) {
        this.toDoContent = toDoContent;
    }

    public String getSenNm() {
        return senNm;
    }

    public void setSenNm(String senNm) {
        this.senNm = senNm;
    }

    public String getSenRange() {
        return senRange;
    }

    public void setSenRange(String senRange) {
        this.senRange = senRange;
    }

    public String getSenType() {
        return senType;
    }

    public void setSenType(String senType) {
        this.senType = senType;
    }

    public String getSenlevel() {
        return senlevel;
    }

    public void setSenlevel(String senlevel) {
        this.senlevel = senlevel;
    }
}
