package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/**
 *  账号权限管理违规情况统计表
 *   金库审批管理违规1302
 */
public   class XXAQRank1302Do extends XXAQAbsRankDo {
    private Integer rn;
    private String prvdName;
    private String cityName;
    private BigDecimal senRecNum; //涉敏操作日志数量
    private BigDecimal notVauRecNum;
    private BigDecimal notValRecPer;
    private BigDecimal vauFacRecNum;
    private BigDecimal senRecOutNum;//涉敏操作超出授权时段的日志数量（条）
    private BigDecimal senRecPer;
    private BigDecimal timeRecNum;
    private BigDecimal timeOverRecNum;

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public String getPrvdName() {
        return prvdName;
    }

    public void setPrvdName(String prvdName) {
        this.prvdName = prvdName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public BigDecimal getSenRecNum() {
        return senRecNum;
    }

    public void setSenRecNum(BigDecimal senRecNum) {
        this.senRecNum = senRecNum;
    }

    public BigDecimal getNotVauRecNum() {
        return notVauRecNum;
    }

    public void setNotVauRecNum(BigDecimal notVauRecNum) {
        this.notVauRecNum = notVauRecNum;
    }

    public BigDecimal getNotValRecPer() {
        return notValRecPer;
    }

    public void setNotValRecPer(BigDecimal notValRecPer) {
        this.notValRecPer = notValRecPer;
    }

    public BigDecimal getVauFacRecNum() {
        return vauFacRecNum;
    }

    public void setVauFacRecNum(BigDecimal vauFacRecNum) {
        this.vauFacRecNum = vauFacRecNum;
    }

    public BigDecimal getSenRecOutNum() {
        return senRecOutNum;
    }

    public void setSenRecOutNum(BigDecimal senRecOutNum) {
        this.senRecOutNum = senRecOutNum;
    }

    public BigDecimal getSenRecPer() {
        return senRecPer;
    }

    public void setSenRecPer(BigDecimal senRecPer) {
        this.senRecPer = senRecPer;
    }

    public BigDecimal getTimeRecNum() {
        return timeRecNum;
    }

    public void setTimeRecNum(BigDecimal timeRecNum) {
        this.timeRecNum = timeRecNum;
    }

    public BigDecimal getTimeOverRecNum() {
        return timeOverRecNum;
    }

    public void setTimeOverRecNum(BigDecimal timeOverRecNum) {
        this.timeOverRecNum = timeOverRecNum;
    }
}
