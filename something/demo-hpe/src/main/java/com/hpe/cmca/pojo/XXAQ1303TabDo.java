package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/*
涉敏操作-省公司系统
"排序规则：
先按照“厂商人员违规进行管理员操作的次数（次）”降序；
再按照“厂商人员违规进行涉敏操作的日志数量（条）”降序；
再按照“违规删改系统日志的次数（次）”降序；
再按照“越权进行涉敏操作的日志数量（条）”降序。"
 */
public class XXAQ1303TabDo extends XXAQAbsTabDo {
    private BigDecimal adminTodoCnt; //
    private BigDecimal sensitizationLogCnt; //
    private BigDecimal sysLogCnt; //
    private BigDecimal ultraViresSysLogCnt; //

    public BigDecimal getAdminTodoCnt() {
        return adminTodoCnt;
    }

    public void setAdminTodoCnt(BigDecimal adminTodoCnt) {
        this.adminTodoCnt = adminTodoCnt;
    }

    public BigDecimal getSensitizationLogCnt() {
        return sensitizationLogCnt;
    }

    public void setSensitizationLogCnt(BigDecimal sensitizationLogCnt) {
        this.sensitizationLogCnt = sensitizationLogCnt;
    }

    public BigDecimal getSysLogCnt() {
        return sysLogCnt;
    }

    public void setSysLogCnt(BigDecimal sysLogCnt) {
        this.sysLogCnt = sysLogCnt;
    }

    public BigDecimal getUltraViresSysLogCnt() {
        return ultraViresSysLogCnt;
    }

    public void setUltraViresSysLogCnt(BigDecimal ultraViresSysLogCnt) {
        this.ultraViresSysLogCnt = ultraViresSysLogCnt;
    }
}
