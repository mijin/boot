package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/**
 *    账号权限管理违规1301

 */
public   class XXAQRank1301Do extends XXAQAbsRankDo{
    private Integer rn;
    private String prvdName;
    private String cityName;
    private BigDecimal rightAccNum;
    private BigDecimal notRightAccNum;
    private BigDecimal notRightAccPer;
    private BigDecimal senAccNum;
    private BigDecimal notSenAccNum;
    private BigDecimal notSenAccPer;

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public String getPrvdName() {
        return prvdName;
    }

    public void setPrvdName(String prvdName) {
        this.prvdName = prvdName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public BigDecimal getRightAccNum() {
        return rightAccNum;
    }

    public void setRightAccNum(BigDecimal rightAccNum) {
        this.rightAccNum = rightAccNum;
    }

    public BigDecimal getNotRightAccNum() {
        return notRightAccNum;
    }

    public void setNotRightAccNum(BigDecimal notRightAccNum) {
        this.notRightAccNum = notRightAccNum;
    }

    public BigDecimal getNotRightAccPer() {
        return notRightAccPer;
    }

    public void setNotRightAccPer(BigDecimal notRightAccPer) {
        this.notRightAccPer = notRightAccPer;
    }

    public BigDecimal getSenAccNum() {
        return senAccNum;
    }

    public void setSenAccNum(BigDecimal senAccNum) {
        this.senAccNum = senAccNum;
    }

    public BigDecimal getNotSenAccNum() {
        return notSenAccNum;
    }

    public void setNotSenAccNum(BigDecimal notSenAccNum) {
        this.notSenAccNum = notSenAccNum;
    }

    public BigDecimal getNotSenAccPer() {
        return notSenAccPer;
    }

    public void setNotSenAccPer(BigDecimal notSenAccPer) {
        this.notSenAccPer = notSenAccPer;
    }
}
