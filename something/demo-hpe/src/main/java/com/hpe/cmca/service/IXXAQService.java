package com.hpe.cmca.service;

import com.hpe.cmca.pojo.XXAQParamData;

import java.util.List;
import java.util.Map;

public interface IXXAQService {

    /**
     * 地图
     *
     * @return
     */
    List<Map<String, Object>> map(XXAQParamData data);


    /**
     * 卡片
     *
     * @param data
     * @return
     */
    Map<String, Object> bottom(XXAQParamData data);

    /**
     * 柱图，折线图，饼图
     *
     * @param data
     * @return
     */
    Map<String, Object> concern(XXAQParamData data);

    /**
     * 省级别表格
     *
     * @return
     */
    List<Map<String, Object>> tab(XXAQParamData data);

    /**
     * 清单
     *
     * @return
     */
    List<Map<String, Object>> detail(XXAQParamData data);

    /**
     * 排名汇总
     *
     * @return
     */
    List<Map<String, Object>> rank(XXAQParamData data);


    /**
     * 重点关注用户
     *
     * @param data
     * @return
     */
    List<Map<String, Object>> point(XXAQParamData data);
}
