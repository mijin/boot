package com.hpe.cmca.util.compont;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public   abstract class WebUtils extends org.springframework.web.util.WebUtils {

    /**
     * 获取request
     */
    public static HttpServletRequest getRequest()
    {
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取session
     */
    public static HttpSession getSession()
    {
        return getRequest().getSession();
    }

    public static ServletRequestAttributes getRequestAttributes()
    {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }


    /**
     * 获取session所有key
     * @return
     */
    public static List<String> sessionKeys(){
        List<String> result = new ArrayList<>();
        Enumeration<String> attributeNames = getSession().getAttributeNames();
        while (attributeNames.hasMoreElements()){
             result.add(attributeNames.nextElement());
        }
        return result;
    }
}
