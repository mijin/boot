package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/**
 *    涉敏操作管理违规1303

 */
public   class XXAQRank1303Do extends XXAQAbsRankDo {
    private Integer rn;
    private String prvdName;
    private String cityName;
    private BigDecimal senRecNum; //涉敏操作日志数量
    private BigDecimal illAccNum;
    private BigDecimal illRecordTime;
    private BigDecimal illAccTotalNum;
    private BigDecimal illAccTotalTime;
    private BigDecimal facAccNum;
    private BigDecimal facRecordNum;
    private BigDecimal facByAccNum;
    private BigDecimal facByAccTime;
    private BigDecimal facIllAccNum;
    private BigDecimal facIllRecNum;
    private BigDecimal facSenRecPer;
    private BigDecimal noPersonAccNum;
    private BigDecimal noPersonRecPer;

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public String getPrvdName() {
        return prvdName;
    }

    public void setPrvdName(String prvdName) {
        this.prvdName = prvdName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public BigDecimal getSenRecNum() {
        return senRecNum;
    }

    public void setSenRecNum(BigDecimal senRecNum) {
        this.senRecNum = senRecNum;
    }

    public BigDecimal getIllAccNum() {
        return illAccNum;
    }

    public void setIllAccNum(BigDecimal illAccNum) {
        this.illAccNum = illAccNum;
    }

    public BigDecimal getIllRecordTime() {
        return illRecordTime;
    }

    public void setIllRecordTime(BigDecimal illRecordTime) {
        this.illRecordTime = illRecordTime;
    }

    public BigDecimal getIllAccTotalNum() {
        return illAccTotalNum;
    }

    public void setIllAccTotalNum(BigDecimal illAccTotalNum) {
        this.illAccTotalNum = illAccTotalNum;
    }

    public BigDecimal getIllAccTotalTime() {
        return illAccTotalTime;
    }

    public void setIllAccTotalTime(BigDecimal illAccTotalTime) {
        this.illAccTotalTime = illAccTotalTime;
    }

    public BigDecimal getFacAccNum() {
        return facAccNum;
    }

    public void setFacAccNum(BigDecimal facAccNum) {
        this.facAccNum = facAccNum;
    }

    public BigDecimal getFacRecordNum() {
        return facRecordNum;
    }

    public void setFacRecordNum(BigDecimal facRecordNum) {
        this.facRecordNum = facRecordNum;
    }

    public BigDecimal getFacByAccNum() {
        return facByAccNum;
    }

    public void setFacByAccNum(BigDecimal facByAccNum) {
        this.facByAccNum = facByAccNum;
    }

    public BigDecimal getFacByAccTime() {
        return facByAccTime;
    }

    public void setFacByAccTime(BigDecimal facByAccTime) {
        this.facByAccTime = facByAccTime;
    }

    public BigDecimal getFacIllAccNum() {
        return facIllAccNum;
    }

    public void setFacIllAccNum(BigDecimal facIllAccNum) {
        this.facIllAccNum = facIllAccNum;
    }

    public BigDecimal getFacIllRecNum() {
        return facIllRecNum;
    }

    public void setFacIllRecNum(BigDecimal facIllRecNum) {
        this.facIllRecNum = facIllRecNum;
    }

    public BigDecimal getFacSenRecPer() {
        return facSenRecPer;
    }

    public void setFacSenRecPer(BigDecimal facSenRecPer) {
        this.facSenRecPer = facSenRecPer;
    }

    public BigDecimal getNoPersonAccNum() {
        return noPersonAccNum;
    }

    public void setNoPersonAccNum(BigDecimal noPersonAccNum) {
        this.noPersonAccNum = noPersonAccNum;
    }

    public BigDecimal getNoPersonRecPer() {
        return noPersonRecPer;
    }

    public void setNoPersonRecPer(BigDecimal noPersonRecPer) {
        this.noPersonRecPer = noPersonRecPer;
    }
}
