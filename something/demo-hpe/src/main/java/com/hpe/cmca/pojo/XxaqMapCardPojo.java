package com.hpe.cmca.pojo;

import java.io.Serializable;

public class XxaqMapCardPojo implements Serializable {
	
	private Integer prvdId;
	private  String prvdName;
	private  String cityName;
	private Integer rn ;
	private Double ErrRightNums;    //违规授于涉敏操作权限的厂商数量
	private Double ErrRightNumsAmplitude;    //违规授于涉敏操作权限的厂商数量增幅
	private Double ErrRightNumsPers;//违规授于涉敏操作权限的厂商数量占比
	private Double ErrRightNumsPersAmplitude;//违规授于涉敏操作权限的厂商数量占比增幅

	private Double ErrorLogsNums;   //厂商违规涉敏操作的敏感信息日志数量
	private Double ErrorLogsNumsAmplitude;   //厂商违规涉敏操作的敏感信息日志数量增幅
	private Double ErrorLogsNumsPers;//厂商违规涉敏操作的敏感信息日志数量占比
	private Double ErrorLogsNumsPersAmplitude;//厂商违规涉敏操作的敏感信息日志数量占比增幅

	private Double ExceSenLogsNums;  //越权进行涉敏操作的敏感信息日志数量
	private Double ExceSenLogsNumsAmplitude;  //越权进行涉敏操作的敏感信息日志数量增幅
	private Double ExceSenLogsNumsPers;  //越权进行涉敏操作的敏感信息日志数量占比
	private Double ExceSenLogsNumsPersAmplitude;  //越权进行涉敏操作的敏感信息日志数量占比增幅





	private Double ErrCoffSenLogNums;    //涉敏操作未经金库的敏感信息日志数量
	private Double ErrCoffSenLogNumsAmp;    //涉敏操作未经金库的敏感信息日志数量增幅
	private Double ErrCoffSenLogNumsPers;   //涉敏操作未经金库的敏感信息日志数量占比
	private Double ErrCoffSenLogNumsPersAmp;   //涉敏操作未经金库的敏感信息日志数量占比增幅


	private Double SenCoffExamLogNums;    //厂商作为金库授权人的审批日志数量
	private Double SenCoffExamLogNumsAmp;    //厂商作为金库授权人的审批日志数量增幅
	private Double SenCoffExamLogNumsPers;//厂商作为金库授权人的审批日志数量占比
	private Double SenCoffExamLogNumsPersAmp;//厂商作为金库授权人的审批日志数量占比增幅


	private Double InvokTimeSenLogNums;//涉敏操作超出授权时段的敏感信息日志数量
	private Double InvokTimeSenLogNumsAmp;//涉敏操作超出授权时段的敏感信息日志数量增幅
	private Double InvokTimeSenLogNumsPers;//涉敏操作超出授权时段的敏感信息日志数量占比
	private Double InvokTimeSenLogNumsPersAmp;//涉敏操作超出授权时段的敏感信息日志数量占比增幅
	private  Double errFirmidCnt;
	private Double errFirmlogCnt;
	private Double errGranklogCnt;
	private Double treaErrLogCnt;
	private Double errTreafirmCnt;
	private Double errTimelogCnt;

	public Double getTreaErrLogCnt() {
		return treaErrLogCnt;
	}

	public Double getErrTreafirmCnt() {
		return errTreafirmCnt;
	}

	public Double getErrTimelogCnt() {
		return errTimelogCnt;
	}

	public void setTreaErrLogCnt(Double treaErrLogCnt) {
		this.treaErrLogCnt = treaErrLogCnt;
	}

	public void setErrTreafirmCnt(Double errTreafirmCnt) {
		this.errTreafirmCnt = errTreafirmCnt;
	}

	public void setErrTimelogCnt(Double errTimelogCnt) {
		this.errTimelogCnt = errTimelogCnt;
	}

	public void setErrFirmidCnt(Double errFirmidCnt) {
		this.errFirmidCnt = errFirmidCnt;
	}

	public void setErrFirmlogCnt(Double errFirmlogCnt) {
		this.errFirmlogCnt = errFirmlogCnt;
	}

	public void setErrGranklogCnt(Double errGranklogCnt) {
		this.errGranklogCnt = errGranklogCnt;
	}

	public Double getErrFirmidCnt() {
		return errFirmidCnt;
	}

	public Double getErrFirmlogCnt() {
		return errFirmlogCnt;
	}

	public Double getErrGranklogCnt() {
		return errGranklogCnt;
	}

	public Integer getPrvdId() {
		return prvdId;
	}

	public String getPrvdName() {
		return prvdName;
	}

	public String getCityName() {
		return cityName;
	}

	public Integer getRn() {
		return rn;
	}

	public Double getErrRightNums() {
		return ErrRightNums;
	}

	public Double getErrRightNumsAmplitude() {
		return ErrRightNumsAmplitude;
	}

	public Double getErrRightNumsPers() {
		return ErrRightNumsPers;
	}

	public Double getErrRightNumsPersAmplitude() {
		return ErrRightNumsPersAmplitude;
	}

	public Double getErrorLogsNums() {
		return ErrorLogsNums;
	}

	public Double getErrorLogsNumsAmplitude() {
		return ErrorLogsNumsAmplitude;
	}

	public Double getErrorLogsNumsPers() {
		return ErrorLogsNumsPers;
	}

	public Double getErrorLogsNumsPersAmplitude() {
		return ErrorLogsNumsPersAmplitude;
	}

	public Double getExceSenLogsNums() {
		return ExceSenLogsNums;
	}

	public Double getExceSenLogsNumsAmplitude() {
		return ExceSenLogsNumsAmplitude;
	}

	public Double getExceSenLogsNumsPers() {
		return ExceSenLogsNumsPers;
	}

	public Double getExceSenLogsNumsPersAmplitude() {
		return ExceSenLogsNumsPersAmplitude;
	}

	public Double getErrCoffSenLogNums() {
		return ErrCoffSenLogNums;
	}

	public Double getErrCoffSenLogNumsAmp() {
		return ErrCoffSenLogNumsAmp;
	}

	public Double getErrCoffSenLogNumsPers() {
		return ErrCoffSenLogNumsPers;
	}

	public Double getErrCoffSenLogNumsPersAmp() {
		return ErrCoffSenLogNumsPersAmp;
	}

	public Double getSenCoffExamLogNums() {
		return SenCoffExamLogNums;
	}

	public Double getSenCoffExamLogNumsAmp() {
		return SenCoffExamLogNumsAmp;
	}

	public Double getSenCoffExamLogNumsPers() {
		return SenCoffExamLogNumsPers;
	}

	public Double getSenCoffExamLogNumsPersAmp() {
		return SenCoffExamLogNumsPersAmp;
	}

	public Double getInvokTimeSenLogNums() {
		return InvokTimeSenLogNums;
	}

	public Double getInvokTimeSenLogNumsAmp() {
		return InvokTimeSenLogNumsAmp;
	}

	public Double getInvokTimeSenLogNumsPers() {
		return InvokTimeSenLogNumsPers;
	}

	public Double getInvokTimeSenLogNumsPersAmp() {
		return InvokTimeSenLogNumsPersAmp;
	}

	public void setPrvdId(Integer prvdId) {
		this.prvdId = prvdId;
	}

	public void setPrvdName(String prvdName) {
		this.prvdName = prvdName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public void setRn(Integer rn) {
		this.rn = rn;
	}

	public void setErrRightNums(Double errRightNums) {
		ErrRightNums = errRightNums;
	}

	public void setErrRightNumsAmplitude(Double errRightNumsAmplitude) {
		ErrRightNumsAmplitude = errRightNumsAmplitude;
	}

	public void setErrRightNumsPers(Double errRightNumsPers) {
		ErrRightNumsPers = errRightNumsPers;
	}

	public void setErrRightNumsPersAmplitude(Double errRightNumsPersAmplitude) {
		ErrRightNumsPersAmplitude = errRightNumsPersAmplitude;
	}

	public void setErrorLogsNums(Double errorLogsNums) {
		ErrorLogsNums = errorLogsNums;
	}

	public void setErrorLogsNumsAmplitude(Double errorLogsNumsAmplitude) {
		ErrorLogsNumsAmplitude = errorLogsNumsAmplitude;
	}

	public void setErrorLogsNumsPers(Double errorLogsNumsPers) {
		ErrorLogsNumsPers = errorLogsNumsPers;
	}

	public void setErrorLogsNumsPersAmplitude(Double errorLogsNumsPersAmplitude) {
		ErrorLogsNumsPersAmplitude = errorLogsNumsPersAmplitude;
	}

	public void setExceSenLogsNums(Double exceSenLogsNums) {
		ExceSenLogsNums = exceSenLogsNums;
	}

	public void setExceSenLogsNumsAmplitude(Double exceSenLogsNumsAmplitude) {
		ExceSenLogsNumsAmplitude = exceSenLogsNumsAmplitude;
	}

	public void setExceSenLogsNumsPers(Double exceSenLogsNumsPers) {
		ExceSenLogsNumsPers = exceSenLogsNumsPers;
	}

	public void setExceSenLogsNumsPersAmplitude(Double exceSenLogsNumsPersAmplitude) {
		ExceSenLogsNumsPersAmplitude = exceSenLogsNumsPersAmplitude;
	}

	public void setErrCoffSenLogNums(Double errCoffSenLogNums) {
		ErrCoffSenLogNums = errCoffSenLogNums;
	}

	public void setErrCoffSenLogNumsAmp(Double errCoffSenLogNumsAmp) {
		ErrCoffSenLogNumsAmp = errCoffSenLogNumsAmp;
	}

	public void setErrCoffSenLogNumsPers(Double errCoffSenLogNumsPers) {
		ErrCoffSenLogNumsPers = errCoffSenLogNumsPers;
	}

	public void setErrCoffSenLogNumsPersAmp(Double errCoffSenLogNumsPersAmp) {
		ErrCoffSenLogNumsPersAmp = errCoffSenLogNumsPersAmp;
	}

	public void setSenCoffExamLogNums(Double senCoffExamLogNums) {
		SenCoffExamLogNums = senCoffExamLogNums;
	}

	public void setSenCoffExamLogNumsAmp(Double senCoffExamLogNumsAmp) {
		SenCoffExamLogNumsAmp = senCoffExamLogNumsAmp;
	}

	public void setSenCoffExamLogNumsPers(Double senCoffExamLogNumsPers) {
		SenCoffExamLogNumsPers = senCoffExamLogNumsPers;
	}

	public void setSenCoffExamLogNumsPersAmp(Double senCoffExamLogNumsPersAmp) {
		SenCoffExamLogNumsPersAmp = senCoffExamLogNumsPersAmp;
	}

	public void setInvokTimeSenLogNums(Double invokTimeSenLogNums) {
		InvokTimeSenLogNums = invokTimeSenLogNums;
	}

	public void setInvokTimeSenLogNumsAmp(Double invokTimeSenLogNumsAmp) {
		InvokTimeSenLogNumsAmp = invokTimeSenLogNumsAmp;
	}

	public void setInvokTimeSenLogNumsPers(Double invokTimeSenLogNumsPers) {
		InvokTimeSenLogNumsPers = invokTimeSenLogNumsPers;
	}

	public void setInvokTimeSenLogNumsPersAmp(Double invokTimeSenLogNumsPersAmp) {
		InvokTimeSenLogNumsPersAmp = invokTimeSenLogNumsPersAmp;
	}

	@Override
	public String toString() {
		return "XxaqMapCardPojo{" +
				"prvdId=" + prvdId +
				", prvdName='" + prvdName + '\'' +
				", cityName='" + cityName + '\'' +
				", rn=" + rn +
				", ErrRightNums=" + ErrRightNums +
				", ErrRightNumsAmplitude=" + ErrRightNumsAmplitude +
				", ErrRightNumsPers=" + ErrRightNumsPers +
				", ErrRightNumsPersAmplitude=" + ErrRightNumsPersAmplitude +
				", ErrorLogsNums=" + ErrorLogsNums +
				", ErrorLogsNumsAmplitude=" + ErrorLogsNumsAmplitude +
				", ErrorLogsNumsPers=" + ErrorLogsNumsPers +
				", ErrorLogsNumsPersAmplitude=" + ErrorLogsNumsPersAmplitude +
				", ExceSenLogsNums=" + ExceSenLogsNums +
				", ExceSenLogsNumsAmplitude=" + ExceSenLogsNumsAmplitude +
				", ExceSenLogsNumsPers=" + ExceSenLogsNumsPers +
				", ExceSenLogsNumsPersAmplitude=" + ExceSenLogsNumsPersAmplitude +
				", ErrCoffSenLogNums=" + ErrCoffSenLogNums +
				", ErrCoffSenLogNumsAmp=" + ErrCoffSenLogNumsAmp +
				", ErrCoffSenLogNumsPers=" + ErrCoffSenLogNumsPers +
				", ErrCoffSenLogNumsPersAmp=" + ErrCoffSenLogNumsPersAmp +
				", SenCoffExamLogNums=" + SenCoffExamLogNums +
				", SenCoffExamLogNumsAmp=" + SenCoffExamLogNumsAmp +
				", SenCoffExamLogNumsPers=" + SenCoffExamLogNumsPers +
				", SenCoffExamLogNumsPersAmp=" + SenCoffExamLogNumsPersAmp +
				", InvokTimeSenLogNums=" + InvokTimeSenLogNums +
				", InvokTimeSenLogNumsAmp=" + InvokTimeSenLogNumsAmp +
				", InvokTimeSenLogNumsPers=" + InvokTimeSenLogNumsPers +
				", InvokTimeSenLogNumsPersAmp=" + InvokTimeSenLogNumsPersAmp +
				'}';
	}
}
