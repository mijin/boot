package com.hpe.cmca.pojo;

/**
 * 省公司base类
 * 账号权限1301  金库管控1302  涉敏操作1303
 */
public abstract class XXAQAbsTabDo extends XXAQAbsDo {
    private String sysDomain;            //所属系统域
    private String sysId;            //系统编号
    private String sysNm;            //系统名称

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getSysNm() {
        return sysNm;
    }

    public void setSysNm(String sysNm) {
        this.sysNm = sysNm;
    }
}
