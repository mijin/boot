package com.hpe.cmca.pojo;

import java.util.Date;

/*
金库审批-审计清单 审计点9
7.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、敏感日志操作ID、操作类型名称、操作内容、敏感数据名称、敏感数据范围、敏感数据定位级别、所在地市或部门
9.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、敏感日志操作ID、操作类型名称、操作内容、操作时间、敏感数据名称、敏感数据范围、敏感数据定位级别、
金库申请/审批ID、申请人、审批人、申请授权类型、授权模式、授权结果、生效时间、失效时间、授权时长、所在地市或部门

  */
public class XXAQ130209Do extends XXAQAbsDetailsDo {
    private String audTrm;
    private String masterAccount;
    private String masterAccountType;
    private String subAccount;
    private String sysDomain;
    private String sysNm;
    private String senLogId;
    private String toDoType;
    private String toDoContent;
    private Date toDoDate;
    private String senNm;
    private String senRange;
    private String senlevel;
    private String applyId;
    private String applyNm;
    private String approvalNm;
    private String authorizationType;
    private String authorizationModel;
    private String authorizationResult;
    private Date useDate;
    private Date noUseDate;
    private String duration;
    private String deptNm;

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }

    public String getMasterAccount() {
        return masterAccount;
    }

    public void setMasterAccount(String masterAccount) {
        this.masterAccount = masterAccount;
    }

    public String getMasterAccountType() {
        return masterAccountType;
    }

    public void setMasterAccountType(String masterAccountType) {
        this.masterAccountType = masterAccountType;
    }

    public String getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }

    public String getSysNm() {
        return sysNm;
    }

    public void setSysNm(String sysNm) {
        this.sysNm = sysNm;
    }

    public String getSenLogId() {
        return senLogId;
    }

    public void setSenLogId(String senLogId) {
        this.senLogId = senLogId;
    }

    public String getToDoType() {
        return toDoType;
    }

    public void setToDoType(String toDoType) {
        this.toDoType = toDoType;
    }

    public String getToDoContent() {
        return toDoContent;
    }

    public void setToDoContent(String toDoContent) {
        this.toDoContent = toDoContent;
    }

    public Date getToDoDate() {
        return toDoDate;
    }

    public void setToDoDate(Date toDoDate) {
        this.toDoDate = toDoDate;
    }

    public String getSenNm() {
        return senNm;
    }

    public void setSenNm(String senNm) {
        this.senNm = senNm;
    }

    public String getSenRange() {
        return senRange;
    }

    public void setSenRange(String senRange) {
        this.senRange = senRange;
    }

    public String getSenlevel() {
        return senlevel;
    }

    public void setSenlevel(String senlevel) {
        this.senlevel = senlevel;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public String getApplyNm() {
        return applyNm;
    }

    public void setApplyNm(String applyNm) {
        this.applyNm = applyNm;
    }

    public String getApprovalNm() {
        return approvalNm;
    }

    public void setApprovalNm(String approvalNm) {
        this.approvalNm = approvalNm;
    }

    public String getAuthorizationType() {
        return authorizationType;
    }

    public void setAuthorizationType(String authorizationType) {
        this.authorizationType = authorizationType;
    }

    public String getAuthorizationModel() {
        return authorizationModel;
    }

    public void setAuthorizationModel(String authorizationModel) {
        this.authorizationModel = authorizationModel;
    }

    public String getAuthorizationResult() {
        return authorizationResult;
    }

    public void setAuthorizationResult(String authorizationResult) {
        this.authorizationResult = authorizationResult;
    }

    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }

    public Date getNoUseDate() {
        return noUseDate;
    }

    public void setNoUseDate(Date noUseDate) {
        this.noUseDate = noUseDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }
}
