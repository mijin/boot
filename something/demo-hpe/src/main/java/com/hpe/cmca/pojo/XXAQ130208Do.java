package com.hpe.cmca.pojo;

import java.util.Date;

/*
金库审批-审计清单 审计点8
7.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、敏感日志操作ID、操作类型名称、操作内容、敏感数据名称、敏感数据范围、敏感数据定位级别、所在地市或部门
8.数据月、所属系统域、系统名称、申请人、审批人、审批人主账号类型、审批时间、金库申请/审批ID、申请授权类型、授权模式、授权结果、所在地市或部门
9.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、敏感日志操作ID、操作类型名称、操作内容、操作时间、敏感数据名称、敏感数据范围、敏感数据定位级别、金库申请/审批ID、申请人、审批人、申请授权类型、授权模式、授权结果、生效时间、失效时间、授权时长、所在地市或部门
 */
public class XXAQ130208Do extends XXAQAbsDetailsDo {
    private String audTrm;
    private String sysDomain;
    private String sysNm;
    private String applyNm;
    private String approval;
    private String approvalMasterAccountType;
    private Date approvalDate;
    private String approvalId; //金库申请/审批ID
    private String applyType;//申请授权类型
    private String authorizationModel;
    private String authorizationResult;
    private String deptNm;

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }

    public String getSysNm() {
        return sysNm;
    }

    public void setSysNm(String sysNm) {
        this.sysNm = sysNm;
    }

    public String getApplyNm() {
        return applyNm;
    }

    public void setApplyNm(String applyNm) {
        this.applyNm = applyNm;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getApprovalMasterAccountType() {
        return approvalMasterAccountType;
    }

    public void setApprovalMasterAccountType(String approvalMasterAccountType) {
        this.approvalMasterAccountType = approvalMasterAccountType;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(String approvalId) {
        this.approvalId = approvalId;
    }

    public String getApplyType() {
        return applyType;
    }

    public void setApplyType(String applyType) {
        this.applyType = applyType;
    }

    public String getAuthorizationModel() {
        return authorizationModel;
    }

    public void setAuthorizationModel(String authorizationModel) {
        this.authorizationModel = authorizationModel;
    }

    public String getAuthorizationResult() {
        return authorizationResult;
    }

    public void setAuthorizationResult(String authorizationResult) {
        this.authorizationResult = authorizationResult;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }
}
