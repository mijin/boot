package com.hpe.cmca.interfaces;

import com.hpe.cmca.pojo.*;

import java.util.List;
import java.util.Map;

public interface NewXXAQMapper {
    /**
     * 地图
     *
     * @return
     */
    List<Map<String, Object>> map(XXAQParamData data);


    /**
     * 卡片
     *
     * @param data
     * @return
     */
    XXAQBottomDo bottom(XXAQParamData data);

    /**
     * 柱图，折线图，饼图
     *
     * @param data
     * @return
     */
    List<Map<String, Object>> concern(XXAQParamData data);


    /**
     * 清单
     *
     * @param data
     * @return
     */
    List<Map<String, Object>> detail(XXAQParamData data);

    /**
     * 省级别表格
     *
     * @param data
     * @return
     */
    List<Map<String, Object>> tab(XXAQParamData data);

    /**
     * 趋势图表格
     *
     * @param data
     * @return
     */
    List<Map<String, Object>> concernTab(XXAQParamData data);

    /**
     * 排名汇总
     * @param data
     * @return
     */
    List<Map<String, Object>> rank(XXAQParamData data);


    /**
     * 重点关注用户
     *
     * @param data
     * @return
     */
    List<Map<String, Object>> point(XXAQParamData data);



}
