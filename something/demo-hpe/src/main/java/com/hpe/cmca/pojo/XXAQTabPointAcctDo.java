package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/**
下钻
 */
public class XXAQTabPointAcctDo extends XXAQAbsPointDo {

    private Integer rn;
    private String audTrm;
    private String prvdName;
    private String mainAccount;// 主账号
    private String mainAccountType; //主账号类型
    private BigDecimal illAdminNum; //违规进行管理员操作次数
    private Integer isAdminTop50; //是否当月top50的账号
    private BigDecimal illSenNum; //违规次数
    private String isSenTop50; //是否当月top50的账号

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }

    public String getPrvdName() {
        return prvdName;
    }

    public void setPrvdName(String prvdName) {
        this.prvdName = prvdName;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getMainAccountType() {
        return mainAccountType;
    }

    public void setMainAccountType(String mainAccountType) {
        this.mainAccountType = mainAccountType;
    }

    public BigDecimal getIllAdminNum() {
        return illAdminNum;
    }

    public void setIllAdminNum(BigDecimal illAdminNum) {
        this.illAdminNum = illAdminNum;
    }

    public Integer getIsAdminTop50() {
        return isAdminTop50;
    }

    public void setIsAdminTop50(Integer isAdminTop50) {
        this.isAdminTop50 = isAdminTop50;
    }

    public BigDecimal getIllSenNum() {
        return illSenNum;
    }

    public void setIllSenNum(BigDecimal illSenNum) {
        this.illSenNum = illSenNum;
    }

    public String getIsSenTop50() {
        return isSenTop50;
    }

    public void setIsSenTop50(String isSenTop50) {
        this.isSenTop50 = isSenTop50;
    }
}
