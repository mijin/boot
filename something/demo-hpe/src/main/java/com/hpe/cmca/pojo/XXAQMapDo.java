package com.hpe.cmca.pojo;

import java.math.BigDecimal;

public class XXAQMapDo {

    private Integer rn; //排名
    private BigDecimal value; //值
    private BigDecimal val; //值

    private Integer prvdId;
    private String prvdNm;
    private String concern;            //关注点
    private String concernNm;            //关注点名称
    private String auditPoint;            //审计点
    private String auditPointNm;            //审计点名称
    private String audTrm;            //审计月

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public BigDecimal getValue() {
        return value;
    }

    //别名再也不要用关键字了，，
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getVal() {
        return val;
    }

    public void setVal(BigDecimal val) {
        this.val = val;
        this.value = val;
    }

    public Integer getPrvdId() {
        return prvdId;
    }

    public void setPrvdId(Integer prvdId) {
        this.prvdId = prvdId;
    }

    public String getPrvdNm() {
        return prvdNm;
    }

    public void setPrvdNm(String prvdNm) {
        this.prvdNm = prvdNm;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public String getConcernNm() {
        return concernNm;
    }

    public void setConcernNm(String concernNm) {
        this.concernNm = concernNm;
    }

    public String getAuditPoint() {
        return auditPoint;
    }

    public void setAuditPoint(String auditPoint) {
        this.auditPoint = auditPoint;
    }

    public String getAuditPointNm() {
        return auditPointNm;
    }

    public void setAuditPointNm(String auditPointNm) {
        this.auditPointNm = auditPointNm;
    }

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }
}
