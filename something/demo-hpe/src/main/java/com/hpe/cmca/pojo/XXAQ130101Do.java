package com.hpe.cmca.pojo;

/*
账号权限-审计清单
 1.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、   所在地市或部门、从账号类型
 2.数据月、主账号、主账号类型、从账号、所属系统域、系统名称、   涉敏权限内容、所在地市或部门
 */
public   class XXAQ130101Do extends XXAQAbsDetailsDo {
    private String audTrm;
    private String masterAccount;
    private String masterAccountType;
    private String subAccount;
    private String sysDomain;
    private String sysNm;


    //审计点1
    private String deptNm;
    private String subAccountType;



    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }

    public String getMasterAccount() {
        return masterAccount;
    }

    public void setMasterAccount(String masterAccount) {
        this.masterAccount = masterAccount;
    }

    public String getMasterAccountType() {
        return masterAccountType;
    }

    public void setMasterAccountType(String masterAccountType) {
        this.masterAccountType = masterAccountType;
    }

    public String getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }

    public String getSysNm() {
        return sysNm;
    }

    public void setSysNm(String sysNm) {
        this.sysNm = sysNm;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    public String getSubAccountType() {
        return subAccountType;
    }

    public void setSubAccountType(String subAccountType) {
        this.subAccountType = subAccountType;
    }

}
