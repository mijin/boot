package com.hpe.cmca.pojo;

import java.math.BigDecimal;

public   class XXAQBottomDo extends XXAQAbsBottomDo {
    private BigDecimal value;
    private BigDecimal val;
    private BigDecimal diff;

    public BigDecimal getVal() {
        return val;
    }

    public void setVal(BigDecimal val) {
        this.val = val;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getDiff() {
        return diff;
    }

    public void setDiff(BigDecimal diff) {
        this.diff = diff;
    }
}
