package com.hpe.cmca.util.compont;

import java.util.LinkedHashMap;

public class TLinkedHashMap<T> extends LinkedHashMap<String,T> {

    public TLinkedHashMap putObj(String key, T value) {
        super.put(key, value);
        return this;
    }

    public String getString(String key ) {
        return Convert.toStr(super.get(key));
    }

    public Integer getInt(String key ) {
        return Convert.toInt(super.get(key));
    }

}
