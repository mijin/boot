package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/*
金库-省公司
"排序规则：
先按照“涉敏操作未经金库审批的日志数量（条）”降序；
再按照“厂商人员作为金库授权人的日志数量（条）”降序；
再按照“涉敏操作超出授权时段的日志数量（条）”降序。"
 */
public class XXAQ1302TabDo extends XXAQAbsTabDo {
    private BigDecimal notApproved; //涉敏操作未经金库审批的日志数量
    private BigDecimal authorizePerson; //厂商人员作为金库授权人的日志数量
    private BigDecimal logCnt; //涉敏操作超出授权时段的日志数量

    public BigDecimal getNotApproved() {
        return notApproved;
    }

    public void setNotApproved(BigDecimal notApproved) {
        this.notApproved = notApproved;
    }

    public BigDecimal getAuthorizePerson() {
        return authorizePerson;
    }

    public void setAuthorizePerson(BigDecimal authorizePerson) {
        this.authorizePerson = authorizePerson;
    }

    public BigDecimal getLogCnt() {
        return logCnt;
    }

    public void setLogCnt(BigDecimal logCnt) {
        this.logCnt = logCnt;
    }
}
