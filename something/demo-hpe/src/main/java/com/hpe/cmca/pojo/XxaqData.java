/**
 * @description 宽带业务管理违规实体
 */
package com.hpe.cmca.pojo;

import java.io.Serializable;

public class XxaqData implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 9048107812014758769L;


	private String audTrm ;
	private int prvdId ;														//查询全国时，为省份ID/否则为地市ID
	private String focusCd ;
	private Integer rn ;//排名

	private String prvdName;												//查询全国时，为省份名称/否则为地市名称
	private Double errFirmidCnt ;	//违规授于涉敏操作权限的厂商数量（万）														//排名
	private Double errFirmlogCnt ; //厂商违规涉敏操作的敏感信息日志数量									//疑似虚假宽带用户数量
	private Double errGranklogCnt ;//越权进行涉敏操作的敏感信息日志数量						//疑似虚假宽带用户数量增幅
	private Double treaErrLogCnt ;//涉敏操作未经金库的敏感信息日志数量							//疑似虚假宽带用户占比
	private Double errTreafirmCnt ;//厂商作为金库授权人的审批日志数量
	private Double errTimelogCnt ;  //涉敏操作超出授权时段的敏感信息日志数量						//综资开通CRM未开通的用户数量

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getAudTrm() {
		return audTrm;
	}

	public int getPrvdId() {
		return prvdId;
	}

	public String getFocusCd() {
		return focusCd;
	}

	public Integer getRn() {
		return rn;
	}

	public String getPrvdName() {
		return prvdName;
	}

	public Double getErrFirmidCnt() {
		return errFirmidCnt;
	}


	public Double getErrFirmlogCnt() {
		return errFirmlogCnt;
	}

	public Double getErrGranklogCnt() {
		return errGranklogCnt;
	}

	public Double getTreaErrLogCnt() {
		return treaErrLogCnt;
	}

	public Double getErrTreafirmCnt() {
		return errTreafirmCnt;
	}

	public Double getErrTimelogCnt() {
		return errTimelogCnt;
	}

	public void setAudTrm(String audTrm) {
		this.audTrm = audTrm;
	}

	public void setPrvdId(int prvdId) {
		this.prvdId = prvdId;
	}

	public void setFocusCd(String focusCd) {
		this.focusCd = focusCd;
	}

	public void setRn(Integer rn) {
		this.rn = rn;
	}

	public void setPrvdName(String prvdName) {
		this.prvdName = prvdName;
	}

	public void setErrFirmidCnt(Double errFirmidCnt) {
		this.errFirmidCnt = errFirmidCnt;
	}

	public void setErrFirmlogCnt(Double errFirmlogCnt) {
		this.errFirmlogCnt = errFirmlogCnt;
	}

	public void setErrGranklogCnt(Double errGranklogCnt) {
		this.errGranklogCnt = errGranklogCnt;
	}

	public void setTreaErrLogCnt(Double treaErrLogCnt) {
		this.treaErrLogCnt = treaErrLogCnt;
	}

	public void setErrTreafirmCnt(Double errTreafirmCnt) {
		this.errTreafirmCnt = errTreafirmCnt;
	}

	public void setErrTimelogCnt(Double errTimelogCnt) {
		this.errTimelogCnt = errTimelogCnt;
	}

	@Override
	public String toString() {
		return "XxaqData{" +
				"audTrm='" + audTrm + '\'' +
				", prvdId=" + prvdId +
				", focusCd='" + focusCd + '\'' +
				", rn=" + rn +
				", prvdName='" + prvdName + '\'' +
				", errFirmidCnt=" + errFirmidCnt +
				", errFirmlogCnt=" + errFirmlogCnt +
				", errGranklogCnt=" + errGranklogCnt +
				", treaErrLogCnt=" + treaErrLogCnt +
				", errTreafirmCnt=" + errTreafirmCnt +
				", errTimelogCnt=" + errTimelogCnt +
				'}';
	}
}
