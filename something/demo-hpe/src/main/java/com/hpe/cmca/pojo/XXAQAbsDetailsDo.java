package com.hpe.cmca.pojo;

/**
 * 省公司清单base类
*  账号权限1301  金库管控1302  涉敏操作1303
 1	违规授予厂商人员管理员操作权限
 2	违规授予厂商人员涉敏操作权限
 3	违规删改系统日志
 4	厂商人员违规进行管理员操作
 5	厂商人员违规进行涉敏操作
 6	越权进行涉敏操作
 7	涉敏操作未经金库审批
 8	违规将厂商人员作为金库授权人
 9	涉敏操作超出授权时段
 */
public abstract class XXAQAbsDetailsDo  extends XXAQAbsDo {

}
