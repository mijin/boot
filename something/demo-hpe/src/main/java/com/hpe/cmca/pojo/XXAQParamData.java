package com.hpe.cmca.pojo;

import com.hpe.cmca.util.compont.TLinkedHashMap;

public class XXAQParamData {

    private final static TLinkedHashMap data = new TLinkedHashMap().putObj("1","业支和网络").putObj("2","业支域").putObj("3","网络域");


    private String prvdId ; // 省份标识 // 区域
    private String prvdId2 ; // 省份标识 // 区域
    private String audTrm ; // 审计月
    private String concern ; // 关注点
    private String parameterType;//传入类型，识别柱状图类型
    private int switchState;//开关状态
    private String focusCd ;
    private String flagId;//域
    private String priAccttype;//主账号类型
    private String subjectId;
    private String priAcct;
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public void setPriAcct(String priAcct) {
        this.priAcct = priAcct;
    }

    public String getPriAcct() {
        return priAcct;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public String getFlagId() {
        return flagId;
    }


    public void setFlagId(String flagId) {
        this.flagId = flagId;
        this.focusCd = data.get(flagId)+""; //标注数据
    }

    public void setFocusCd(String focusCd) {
        this.focusCd = focusCd;
    }

    public String getFocusCd() {
        return focusCd;
    }

    public void setSwitchState(int switchState) {
        this.switchState = switchState;
    }

    public int getSwitchState() {
        return switchState;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public String getConcern() {
        return concern;
    }

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }

    public String getPrvdId() {
        return prvdId;
    }

    public void setPrvdId(String prvdId) {
        this.prvdId = prvdId;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getParameterType() {
        return parameterType;
    }

    public String getPriAccttype() {
        return priAccttype;
    }

    public void setPriAccttype(String priAccttype) {
        this.priAccttype = priAccttype;
    }

    public String getPrvdId2() {
        return prvdId2;
    }

    public void setPrvdId2(String prvdId2) {
        this.prvdId2 = prvdId2;
    }

    @Override
    public String toString() {
        return "XXAQParamData{" +
                "prvdId='" + prvdId + '\'' +
                ", audTrm='" + audTrm + '\'' +
                ", concern='" + concern + '\'' +
                ", parameterType='" + parameterType + '\'' +
                ", switchState=" + switchState +
                ", focusCd='" + focusCd + '\'' +
                ", flagId='" + flagId + '\'' +
                ", priAccttype='" + priAccttype + '\'' +
                ", subjectId='" + subjectId + '\'' +
                ", priAcct='" + priAcct + '\'' +
                '}';
    }

    public String getContent() {
        return prvdId + audTrm + concern + parameterType + switchState +
                focusCd + flagId +  priAccttype + subjectId + priAcct+version+prvdId2 ;
    }
}
