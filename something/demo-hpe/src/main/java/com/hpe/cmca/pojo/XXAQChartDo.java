package com.hpe.cmca.pojo;

import java.math.BigDecimal;

public class XXAQChartDo {

    private Integer rn; //排名
    private BigDecimal num; //数值
    private BigDecimal pre; //占比

    private Integer prvdId;
    private String prvdNm;
    private String sysNm;
    private String sysDomain; //系统域
    private String concern;            //关注点
    private String concernNm;            //关注点名称
    private String auditPoint;            //审计点
    private String auditPointNm;            //审计点名称
    private String audTrm;            //审计月
    private Integer chartType;            //
    private String img;            //图片

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }

    public String getSysNm() {
        return sysNm;
    }

    public void setSysNm(String sysNm) {
        this.sysNm = sysNm;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getChartType() {
        return chartType;
    }

    public void setChartType(Integer chartType) {
        this.chartType = chartType;
    }

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getPre() {
        return pre;
    }

    public void setPre(BigDecimal pre) {
        this.pre = pre;
    }

    public Integer getPrvdId() {
        return prvdId;
    }

    public void setPrvdId(Integer prvdId) {
        this.prvdId = prvdId;
    }

    public String getPrvdNm() {
        return prvdNm;
    }

    public void setPrvdNm(String prvdNm) {
        this.prvdNm = prvdNm;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

    public String getConcernNm() {
        return concernNm;
    }

    public void setConcernNm(String concernNm) {
        this.concernNm = concernNm;
    }

    public String getAuditPoint() {
        return auditPoint;
    }

    public void setAuditPoint(String auditPoint) {
        this.auditPoint = auditPoint;
    }

    public String getAuditPointNm() {
        return auditPointNm;
    }

    public void setAuditPointNm(String auditPointNm) {
        this.auditPointNm = auditPointNm;
    }

    public String getAudTrm() {
        return audTrm;
    }

    public void setAudTrm(String audTrm) {
        this.audTrm = audTrm;
    }
}
