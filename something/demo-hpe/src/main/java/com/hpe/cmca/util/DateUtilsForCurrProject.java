package com.hpe.cmca.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Title 时间工具类
 * @author admin
 *
 */
public class DateUtilsForCurrProject {

	//查询半年前的年月
	@SuppressWarnings("static-access")
	public static String GetDateBeforeSixMonth(Date date){
		Calendar calendar = Calendar.getInstance(); //得到日历
 		calendar.setTime(date);//把当前时间赋给日历
 		calendar.add(calendar.MONTH, -6); 
 		date = calendar.getTime();
 		SimpleDateFormat sf = new SimpleDateFormat("yyyyMM") ;
 		String sixMonthDate  = sf.format(date) ;
 		return sixMonthDate ;
	}
	//年月格式转换
	public static String getYearAndMonth(Date date){
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMM") ;
		String newDate = sf.format(date) ;
		return newDate ;
	}
	//String转换Date
    public static Date stringToDate(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        return (sdf.parse(dateStr));
    }
	//String转换Date
	public static Date stringToDateChinese(String dateStr) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年M月");
		return (sdf.parse(dateStr));
	}
	//String转换Date（YYYY/MM）
	public static Date stringToDatIndex(String dateStr) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
		return (sdf.parse(dateStr));
	}
	//yyyyMM转换yyyy年MM月
    public static String yyyyMMToyyyyYearMMMonth(String dateStr) throws ParseException {
        Date date = stringToDate(dateStr) ;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月") ;
        String newDateStr = sdf.format(date) ;
        return newDateStr ;
    }

    /**
     * @param dateStr
     * @return
     */
    public static String formatChinese(String dateStr) {
        Date date = null;
        try {
            date = stringToDate(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月") ;
        String newDateStr = sdf.format(date) ;
        return newDateStr ;
    }
	//yyyy年M月yyyyMM转换
	public static String yyyyYearMMonthToyyyyMM(String dateStr) throws ParseException {
		Date date = stringToDatIndex(dateStr) ;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM") ;
		String newDateStr = sdf.format(date) ;
		return newDateStr ;
	}

    //当前字符串是否可以转换为Date
    public static boolean currStrIsNotDate(String date){
    	boolean b ;
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
    	try {
			sdf.parse(date) ;
			b = true ;
		} catch (ParseException e) {
			b= false ;
		}
    	return b ;
    }
    //获取开始年月到结束年月所有月份集合——格式:201510
    public static ArrayList<String> getAllYearAndMonth(String yearsStart , String yearsEnd) throws ParseException{
    	
    		//定义所有月份集合
    		ArrayList<String> allYearMonthList = new ArrayList<String>();  
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");//格式化为年月  
		  
		    Calendar min = Calendar.getInstance();  
		    Calendar max = Calendar.getInstance();  
		  
		    min.setTime(sdf.parse(yearsStart));  
		    min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);  
		  
		    max.setTime(sdf.parse(yearsEnd));  
		    max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);  
		  
		    Calendar curr = min;  
		    while (curr.before(max)) {  
		    	allYearMonthList.add(sdf.format(curr.getTime()));  
		    	curr.add(Calendar.MONTH, 1);  
		    }  
		    return allYearMonthList ;
    }

	//获取开始年月到结束年月所有月份集合并封装Map——格式:201510
	public static List<Map<String, Object>> getAllYearAndMontMap(String yearsStart , String yearsEnd) throws ParseException{
		//将数据进行封装
		List<Map<String, Object>> allYearMorthListMap = new ArrayList<Map<String, Object>>();
		//定义所有月份集合
		ArrayList<String> allYearMonthList = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");//格式化为年月

		Calendar min = Calendar.getInstance();
		Calendar max = Calendar.getInstance();

		min.setTime(sdf.parse(yearsStart));
		min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

		max.setTime(sdf.parse(yearsEnd));
		max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

		Calendar curr = min;
		while (curr.before(max)) {
			allYearMonthList.add(sdf.format(curr.getTime()));
			curr.add(Calendar.MONTH, 1);
		}
		if(allYearMonthList != null && allYearMonthList.size() !=0) {

			for (int i = 0; i < allYearMonthList.size(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("aud_trm", allYearMonthList.get(i));
				map.put("aud_trm_nm", DateUtilsForCurrProject.getAfterAnyMonthLastDay(allYearMonthList.get(i), 0));
				allYearMorthListMap.add(map) ;
			}
		}
		return allYearMorthListMap ;
	}

    //获取几个月后的年月（最后一天）
    public static String getAfterAnyMonthLastDay(String audTrm , int anyMonth) throws ParseException{

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月");
		Date date = sdf.parse(audTrm) ;
        
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, anyMonth + 1);
        calendar.set(Calendar.DATE, 0);
        
        return df.format(calendar.getTime());
    }
    //获取几个月后的年月YYYYMM
    public static String getAfterAnyMonth(String audTrm , int anyMonth) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Date date = null;
		try {
			date = sdf.parse(audTrm);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, anyMonth + 1);
        calendar.set(Calendar.DATE, 0);
        return sdf.format(calendar.getTime());
    }

	//获取几个月后的年月YYYYMM
	public static StringBuffer getAfterAnyMonthStrBuffer(String audTrm , int anyMonth) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Date date = null;
		try {
			date = sdf.parse(audTrm);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, anyMonth + 1);
		calendar.set(Calendar.DATE, 0);
		StringBuffer sbf = new StringBuffer(sdf.format(calendar.getTime()));
		return sbf ;
	}
    
    //获取几个月前的日期
    public static String getBeforeAnyMonth(Date  audTrm , int anyMath){
    	SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
        Calendar c = Calendar.getInstance();
        c.setTime(audTrm);
        c.add(Calendar.MONTH, -1*anyMath);
        Date m = c.getTime();
        String mon = format.format(m);
        return mon ; 
    }

	//将当前日期进行转换
	public static String getFormatDate(String dateTime){
		String newDateTime = null ;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = format.parse(dateTime);
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			newDateTime = sf.format(date) ;
		}catch (Exception e){
			e.printStackTrace();
			return newDateTime ;
		}
		return newDateTime ;
	}

    //获取当前日期
	public static String getCurrentDateTime(){
		Date date = new Date() ;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
		String dateTime = sf.format(date) ;
		return dateTime ;
	}

	//获取年月日
	public static String getCurrentDate(){
		Date date = new Date() ;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd") ;
		String dateTime = sf.format(date) ;
		return dateTime ;
	}

	//获取当前审计月份
	public static String getCurrentAudTrm(){
		Date date = new Date() ;
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMM") ;
		String dateTime = sf.format(date) ;
		return dateTime ;
	}

	public static Date strToDate(Object dateVal ) {
		return strToDate(dateVal,"yyyy-MM-dd");
	}
	public static Date strToDate(Object dateVal , String fromat) {
		if (dateVal == null || dateVal.toString().trim().length() == 0) {
			return null;
		}
		try {
			return new SimpleDateFormat(fromat).parse(dateVal.toString());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
   public static String dateToString(Date dateVal , String fromat) {
	if (dateVal == null || dateVal.toString().trim().length() == 0) {
		return null;
	}
	return new SimpleDateFormat(fromat).format(dateVal );
}
	public static String dateToString(Date dateVal  ) {
		return dateToString(dateVal,"yyyy-MM-dd HH:mm:ss");
	}

	//时间转换
	public static String gerFormatDate(String dateTime){
		String newDateTime = null ;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			Date newDate = sdf.parse(dateTime);
            SimpleDateFormat sdfStr = new SimpleDateFormat("yyyy-MM-dd");
			newDateTime = sdfStr.format(newDate) ;
		}catch (Exception e){
			e.printStackTrace();
			return null ;
		}
		return newDateTime ;
	}


	/**
	 * 根据String型时间，获取long型时间，单位毫秒
	 * @param inVal 时间字符串
	 * @return long型时间
	 */
	public static long fromDateStringToLong(String inVal) {
		Date date = null;
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			date = inputFormat.parse(inVal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date.getTime();
	}

}
