package com.hpe.cmca.service.impl;

import com.hpe.cmca.interfaces.NewXXAQMapper;
import com.hpe.cmca.pojo.*;
import com.hpe.cmca.service.IXXAQService;
import com.hpe.cmca.util.DateUtilsForCurrProject;
import com.hpe.cmca.util.compont.JFreeChartCompont;
import com.hpe.cmca.util.compont.TLinkedHashMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class XXAQServiceImpl implements IXXAQService {

    @Autowired
    private NewXXAQMapper xxaqMapper;


    private final static Map<String, ConcernAudit> audit = new HashMap<String, ConcernAudit>() {{
        put("1301", new XXAQServiceImpl.ConcernAudit(1, 2));
        put("1302", new XXAQServiceImpl.ConcernAudit(7, 9));
        put("1303", new XXAQServiceImpl.ConcernAudit(3, 6));
    }};

    /**
     * 地图
     *
     * @param data
     * @return
     */
    @Override
    public List<Map<String, Object>> map(XXAQParamData data) {
        List<Map<String, Object>> res = new LinkedList<>();
        List<Map<String, Object>> maps = xxaqMapper.map(data);
        for (Map<String, Object> map : maps) {
            Map<String, Object> tdata = new LinkedHashMap<>(3);
            tdata.put("prvdId", map.get("prvdId"));
            tdata.put("prvdName", map.get("prvdName"));
            tdata.put("value", new Object[]{map.get("rn"), map.get("val")});
            res.add(tdata);
        }
        return res;
    }

    /**
     * 卡片
     *
     * @param data
     * @return
     */
    @Override
    public Map<String, Object> bottom(XXAQParamData data) {
        Map<String, Object> map = new HashMap<>();
        for (int index = 1; index < 3; index++) {
            data.setParameterType(index + "");
            map.put(data.getConcern() + "0" + index, xxaqMapper.bottom(data));
        }
        return map;
    }

    /**
     * 柱图，折线图，饼图
     * 审计点类别（关注点）
     * 账号权限管理违规1301
     * 1
     * 违规授予厂商人员管理员操作权限
     * 2
     * 违规授予厂商人员涉敏操作权限
     * <p>
     * 涉敏操作管理违规1303
     * 3
     * 违规删改系统日志
     * 4
     * 厂商人员违规进行管理员操作
     * 5
     * 厂商人员违规进行涉敏操作
     * 6
     * 越权进行涉敏操作
     * <p>
     * 金库审批管理违规1302
     * 7
     * 涉敏操作未经金库审批
     * 8
     * 违规将厂商人员作为金库授权人
     * 9
     * 涉敏操作超出授权时段
     *
     * @param data
     * @return
     */

    @Override
    public Map<String, Object> concern(XXAQParamData data) {
        String focusCd = data.getFocusCd();
//        prvd的实现
        Map<String, Object> res = new LinkedHashMap<>();
//        res.put("prvdName", prvdInfoMapper.getPrvdName(Integer.valueOf(data.getPrvdId())));

        List<Map<String, Object>> firstData = new LinkedList<>();
        List<Map<String, Object>> secondData = new LinkedList<>();
        List<Map<String, Object>> thirdData = new LinkedList<>();
        List<Map<String, Object>> fourData = new LinkedList<>();
        List<Map<String, Object>> fifthData = new LinkedList<>();
        List<Map<String, Object>> sixthData = new LinkedList<>();
        List<Map<String, Object>> seventhData = new LinkedList<>();
        List<Map<String, Object>> eightData = new LinkedList<>();
        List<Map<String, Object>> tableData = new LinkedList<>();
        List<Map<String, Object>> cols = new LinkedList<Map<String, Object>>() {{
            add(new TLinkedHashMap().putObj("prop", "audit").putObj("lable", "关注点"));
            add(new TLinkedHashMap().putObj("prop", "linePic").putObj("lable", "违规趋势"));
        }};

//        优先申明好
        res.put("firstData", firstData);
        res.put("secondData", secondData);
        res.put("thirdData", thirdData);
        res.put("fourData", fourData);
        res.put("fifthData", fifthData);
        res.put("sixthData", sixthData);
        res.put("seventhData", seventhData);
        res.put("eightData", eightData);
        res.put("cols", cols);
        res.put("tableData", tableData);


//        以下处理为全国处理
        if (Objects.equals(data.getConcern(), "1301")) {
            if (Objects.equals(data.getPrvdId(), "10000")) {
                List<Map<String, Object>> concern = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern) {
                    firstData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num1")).putObj("percents", map.get("pre1")));
                    secondData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num2")).putObj("percents", map.get("pre2")));
                }
            } else {
//                num1 ：违规具备管理员操作权限的厂商人员账号数量   num2：违规具备敏感信息操作权限的厂商人员账号数量 此处需要两个域的数据
                data.setFocusCd("业支域");
                List<Map<String, Object>> concern1 = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern1) {
                    firstData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num1")));
                    thirdData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num2")));
                }

                data.setFocusCd("网络域");
                List<Map<String, Object>> concern2 = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern2) {
                    secondData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num1")));
                    fourData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num2")));
                }
            }
        }


        if (Objects.equals(data.getConcern(), "1303")) {
            if (Objects.equals(data.getPrvdId(), "10000")) {
                List<Map<String, Object>> concern = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern) {
                    firstData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num1")).putObj("percents", map.get("pre1")));
                    secondData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num2")).putObj("percents", map.get("pre2")));
                    thirdData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num3")).putObj("percents", map.get("pre3")));
                    fourData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num4")).putObj("percents", map.get("pre4")));
                }
            } else {
                data.setFocusCd("业支域");
                List<Map<String, Object>> concern1 = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern1) {
                    firstData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num1")));
                    thirdData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num2")));
                    fifthData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num3")));
                    seventhData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num4")));
                }

                data.setFocusCd("网络域");
                List<Map<String, Object>> concern2 = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern2) {
                    secondData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num1")));
                    fourData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num2")));
                    sixthData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num3")));
                    eightData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num4")));
                }
            }
        }


        if (Objects.equals(data.getConcern(), "1302")) {
            if (Objects.equals(data.getPrvdId(), "10000")) {
                List<Map<String, Object>> concern = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern) {
                    firstData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num1")).putObj("percents", map.get("pre1")));
                    secondData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num2")).putObj("percents", map.get("pre2")));
                    thirdData.add(new TLinkedHashMap().putObj("prvd", map.get("prvd")).putObj("num", map.get("num3")).putObj("percents", map.get("pre3")));
                }
            } else {
                data.setFocusCd("业支域");
                List<Map<String, Object>> concern1 = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern1) {
                    firstData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num1")));
                    thirdData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num2")));
                    fifthData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num3")));
                }

                data.setFocusCd("网络域");
                List<Map<String, Object>> concern2 = xxaqMapper.concern(data);
                for (Map<String, Object> map : concern2) {
                    secondData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num1")));
                    fourData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num2")));
                    sixthData.add(new TLinkedHashMap().putObj("area", map.get("area")).putObj("num", map.get("num3")));
                }
            }
        }


//        以下属于表格部分，尽量通用
        //        获取下半部分表格信息
        data.setFocusCd(focusCd);
        List<Map<String, Object>> concernTab = xxaqMapper.concernTab(data);
//表头
        for (Map<String, Object> map1 : concernTab) {
            String audTrm = map1.get("audTrm") + "";
            cols.add(new TLinkedHashMap().putObj("prop", audTrm).putObj("lable", DateUtilsForCurrProject.formatChinese(audTrm)));
        }
//表content
        XXAQServiceImpl.ConcernAudit concernAudit = audit.get(data.getConcern());
        for (int index = concernAudit.getMin(); index <= concernAudit.getMax(); index++) {
            Map<String, Object> ttt = new LinkedHashMap<>();
            for (Map<String, Object> col : cols) {
                String prop = col.get("prop") + "";
                for (Map<String, Object> xxaqChartDo : concernTab) {
                    if (prop.equals(xxaqChartDo.get("audTrm"))) {
                        ttt.put(prop, xxaqChartDo.get("num" + index));
                    } else if (prop.equals("audit")) {
                        ttt.put(prop, XXAQServiceImpl.ConcernAudit.getAudMsg(index));
                    }
                }
            }
            tableData.add(ttt);
        }

//     处理图片
        for (Map<String, Object> tableDatum : tableData) {
            List<JFreeChartCompont.LineData> list = new LinkedList<>();

//            将年份转换成序号
            Map<String, Integer> map = new LinkedHashMap<>();
            Integer index = 0;
            for (String key : tableDatum.keySet()) {
                map.put(key, index);
                index++;
            }


            for (Map.Entry<String, Object> entry : tableDatum.entrySet()) {
                if (StringUtils.isNumeric(entry.getKey())) {
                    list.add(new JFreeChartCompont.LineData(new BigDecimal(map.get(entry.getKey())), new BigDecimal(entry.getValue() == null ? "0" : entry.getValue().toString())));
                }
            }
            if (!list.isEmpty()) {
                JFreeChartCompont chart = new JFreeChartCompont(list, "/jfreechart/lineXY2.png", 300, 200);
                chart.creatChart();
                chart.toFile();
                String base64 = chart.getBase64();
                tableDatum.put("linePic", base64);
            }
        }

        return res;
    }


    /**
     * 表格，统计图
     *
     * @param data
     * @return
     */
    @Override
    public List<Map<String, Object>> tab(XXAQParamData data) {
        return xxaqMapper.tab(data);
    }

    @Override
    public List<Map<String, Object>> detail(XXAQParamData data) {
        return xxaqMapper.detail(data);
    }

    @Override
    public List<Map<String, Object>> rank(XXAQParamData data) {
        return xxaqMapper.rank(data);
    }

    /**
     * 重点关注用户
     *
     * @param data
     * @return
     */
    @Override
    public List<Map<String, Object>> point(XXAQParamData data) {
        return xxaqMapper.point(data);
    }

    public static class ConcernAudit {
        private Integer min;
        private Integer max;
        private final static Map<Integer, String> data = new HashMap<Integer, String>() {{
            put(1, "违规授予厂商人员管理员操作权限");
            put(2, "违规授予厂商人员涉敏操作权限");
            put(3, "违规删改系统日志");
            put(4, "厂商人员违规进行管理员操作");
            put(5, "厂商人员违规进行涉敏操作");
            put(6, "越权进行涉敏操作");
            put(7, "涉敏操作未经金库审批");
            put(8, "违规将厂商人员作为金库授权人");
            put(9, "涉敏操作超出授权时段");
        }};


        /**
         * 根据序号获取msg
         *
         * @param audit
         * @return
         */
        public static String getAudMsg(Integer audit) {
            return data.get(audit);
        }


        public ConcernAudit(Integer min, Integer max) {
            this.min = min;
            this.max = max;
        }

        public Integer getMin() {
            return min;
        }

        public Integer getMax() {
            return max;
        }
    }
}
