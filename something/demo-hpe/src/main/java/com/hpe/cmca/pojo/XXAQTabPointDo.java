package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/**
 * 厂商人员违规进行管理员操作/涉敏操作情况突出的账号情况
 * 主账号连续6个月违规情况
 */
public   class XXAQTabPointDo  extends XXAQAbsPointDo {
    private Integer rn;
    private String prvdName;
    private String mainAccount;// 主账号
    private String mainAccountType; //主账号类型
    private BigDecimal illAdminNum; //违规进行管理员操作的次数
    private BigDecimal onFactorScale; //在本公司违规进行管理员操作的次数占比
    private String illSenNum; //违规进行涉敏操作的次数
    private String onFacScale; //在本公司违规进行涉敏操作的次数占比

    public Integer getRn() {
        return rn;
    }

    public void setRn(Integer rn) {
        this.rn = rn;
    }

    public String getPrvdName() {
        return prvdName;
    }

    public void setPrvdName(String prvdName) {
        this.prvdName = prvdName;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getMainAccountType() {
        return mainAccountType;
    }

    public void setMainAccountType(String mainAccountType) {
        this.mainAccountType = mainAccountType;
    }

    public BigDecimal getIllAdminNum() {
        return illAdminNum;
    }

    public void setIllAdminNum(BigDecimal illAdminNum) {
        this.illAdminNum = illAdminNum;
    }

    public BigDecimal getOnFactorScale() {
        return onFactorScale;
    }

    public void setOnFactorScale(BigDecimal onFactorScale) {
        this.onFactorScale = onFactorScale;
    }

    public String getIllSenNum() {
        return illSenNum;
    }

    public void setIllSenNum(String illSenNum) {
        this.illSenNum = illSenNum;
    }

    public String getOnFacScale() {
        return onFacScale;
    }

    public void setOnFacScale(String onFacScale) {
        this.onFacScale = onFacScale;
    }
}
