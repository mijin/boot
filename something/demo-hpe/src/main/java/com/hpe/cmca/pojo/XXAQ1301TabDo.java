package com.hpe.cmca.pojo;

import java.math.BigDecimal;

/*
先按照“违规具备管理员操作权限的厂商人员账号数量（个）”降序；
再按照“违规具备涉敏操作权限的厂商人员账号数量（个）”降序。
 */
public class XXAQ1301TabDo extends XXAQAbsTabDo {
    private BigDecimal admin; //违规具备管理员操作权限的厂商人员账号数量
    private BigDecimal sensitization; //规具备涉敏操作权限的厂商人员账号数量

    public BigDecimal getAdmin() {
        return admin;
    }

    public void setAdmin(BigDecimal admin) {
        this.admin = admin;
    }

    public BigDecimal getSensitization() {
        return sensitization;
    }

    public void setSensitization(BigDecimal sensitization) {
        this.sensitization = sensitization;
    }
}
