----账号权限历史数据+V1.5数据
--- 账号权限管理违规排名汇总
CREATE MULTISET TABLE hpbus.sum_give_sys_acc_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  have_sys_access_cnt INTEGER TITLE '具备管理员操作权限的账号数量（个）',
	  inf_have_sys_access_cnt INTEGER TITLE '违规具备管理员操作权限的厂商人员账号数量（个）',
	  sys_access_per DECIMAL(18,4) TITLE '违规具备管理员操作权限的厂商人员账号数量占比',
	  have_sens_opr_cnt INTEGER TITLE '具备敏感信息操作权限的账号数量（个）',
	  inf_have_sens_opr_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员账号数量（个）',
	  sens_opr_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员账号数量占比',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_give_sys_acc_1300 '账号权限管理违规排名汇总';
---历史数据
CREATE MULTISET TABLE hpbus.sum_xxaq_qxgk_1301 ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      err_system_cnt INTEGER TITLE '涉及客户敏感信息系统数量',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      err_granklog_cnt INTEGER TITLE '未经授权操作客户敏感信息日志数量',
      err_granklog_per DECIMAL(18,4) TITLE '未经授权操作客户敏感信息日志数量占比',
      err_sm_priacct_cnt INTEGER TITLE '非涉敏人员违规操作客户敏感信息涉及主账号数量',
      err_firmlog_cnt INTEGER TITLE '厂商人员违规操作客户敏感信息日志数量',
      err_firmlog_per DECIMAL(18,4) TITLE '厂商人员违规操作客户敏感信息日志数量占比',
      err_cs_priacct_cnt INTEGER TITLE '厂商人员违规操作客户敏感信息涉及主账号数量',
      err_staffid_cnt INTEGER TITLE '具备敏感信息操作权限的工号数量',
      err_firmid_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员工号数量',
      err_firmid_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员工号数量占比',
      dimension_flag CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '维度标识')
PRIMARY INDEX ( firm_prvd_id )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);


REPLACE VIEW HPBUS.sum_give_sys_acc_1301_web_vt AS 
LOCKING HPBUS.sum_give_sys_acc_1300 ACCESS 
select coalesce(t1.aud_trm,t2.aud_trm) as aud_trm,coalesce(t1.firm_prvd_id,t2.firm_prvd_id) as firm_prvd_id,coalesce(t1.firm_prvd_nm,t2.firm_prvd_nm) as firm_prvd_nm,t1.rn,t1.have_sys_access_cnt,t1.inf_have_sys_access_cnt,t1.sys_access_per,
t1.have_sens_opr_cnt,t1.inf_have_sens_opr_cnt,t1.sens_opr_per,t1.system_range_domain,t2.rank_prvd,t2.err_system_cnt,t2.err_custlog_cnt,t2.err_granklog_cnt,t2.err_granklog_per,t2.err_sm_priacct_cnt,t2.err_firmlog_cnt,t2.err_firmlog_per,t2.err_cs_priacct_cnt,t2.err_staffid_cnt,t2.err_firmid_cnt,t2.err_firmid_per,t2.dimension_flag from
(select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),
  1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,
  firm_prvd_nm,have_sys_access_cnt,inf_have_sys_access_cnt,sys_access_per,
  have_sens_opr_cnt,inf_have_sens_opr_cnt,sens_opr_per,system_range_domain 
from HPBUS.sum_give_sys_acc_1300)t1
full join
(select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),
  1) as format 'yyyymm') as char(6)) as aud_trm,rank_prvd , cast(firm_prvd_id as char(5)) as firm_prvd_id,
  firm_prvd_nm,err_system_cnt,err_custlog_cnt,err_granklog_cnt,err_granklog_per,err_sm_priacct_cnt,err_firmlog_cnt,err_firmlog_per,err_cs_priacct_cnt,err_staffid_cnt,err_firmid_cnt,err_firmid_per,
case dimension_flag 
 when '1' then '业支和网络' 
 when '2' then '业支域' 
 when '3' then '网络域' 
end as dimension_flag 
from hpbus.sum_xxaq_qxgk_1301)t2
on t1.aud_trm=t2.aud_trm
and t1.firm_prvd_id=t2.firm_prvd_id
and t1.system_range_domain=t2.dimension_flag;


---涉敏操作历史数据+V1.5数据
--- 涉敏操作管理违规排名汇总
CREATE MULTISET TABLE hpbus.sum_inf_sens_opr_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  sens_sys_opr_log_cnt INTEGER TITLE '涉敏系统的操作日志总数量（条）',  
      sens_opr_log_cnt INTEGER TITLE '其中：涉敏操作日志数量（条）',      err_custlog_cnt    e
      not_sens_opr_log_cnt INTEGER TITLE '其中：不涉敏操作日志数量（条）',   
      del_sys_log_acct_cnt INTEGER TITLE '违规删改系统日志涉及的账号数量（个）',  
      del_sys_log_cnt INTEGER TITLE '违规删改系统日志次数（次）',  
      sys_opr_acct_cnt INTEGER TITLE '违规进行管理员操作涉及的账号总数量（个）',   
      sys_opr_cnt INTEGER TITLE '违规进行管理员操作的总次数（次）',   
      csry_sys_opr_acct_cnt INTEGER TITLE '厂商人员违规进行管理员操作涉及的账号数量（个）',
      csry_sys_opr_log_cnt INTEGER TITLE '厂商人员违规进行管理员操作的日志数量（条）',
      csry_sys_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行管理员操作的日志数量占比',
      csry_zhqh_sys_acc_zhsl INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限涉及的账号数量（个）',
      csry_zhqh_sys_acc_cnt INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限的次数（次）',
      csry_sens_opr_acct_cnt INTEGER TITLE '厂商人员违规进行涉敏操作涉及的账号数量（个）',
      csry_sens_opr_log_cnt INTEGER TITLE '厂商人员违规进行涉敏操作的日志数量（条）',   
      csry_sens_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行涉敏操作的日志数量占比', 
      person_sens_opr_acct_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作涉及的账号数量（个）',    
      person_sens_opr_log_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量（条）',
      person_sens_opr_log_per DECIMAL(18,4) TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量占比',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_inf_sens_opr_1300 '涉敏操作管理违规排名汇总';


REPLACE VIEW HPBUS.sum_inf_sens_opr_1303_web_vt AS 
LOCKING HPBUS.sum_inf_sens_opr_1300 ACCESS 
select coalesce(t1.aud_trm,t2.aud_trm) as aud_trm,coalesce(t1.firm_prvd_id,t2.firm_prvd_id) as firm_prvd_id,coalesce(t1.firm_prvd_nm,t2.firm_prvd_nm) as firm_prvd_nm,t1.rn,t1.sens_sys_opr_log_cnt,t1.sens_opr_log_cnt,t1.not_sens_opr_log_cnt,t1.del_sys_log_acct_cnt,t1.del_sys_log_cnt,t1.sys_opr_acct_cnt,t1.sys_opr_cnt,t1.csry_sys_opr_acct_cnt,t1.csry_sys_opr_log_cnt,t1.csry_sys_opr_log_per,t1.csry_zhqh_sys_acc_zhsl,t1.csry_zhqh_sys_acc_cnt,t1.csry_sens_opr_acct_cnt,t1.csry_sens_opr_log_cnt,t1.csry_sens_opr_log_per,t1.person_sens_opr_acct_cnt,t1.person_sens_opr_log_cnt,t1.person_sens_opr_log_per,t1.system_range_domain,t2.rank_prvd,t2.err_system_cnt,t2.err_custlog_cnt,t2.err_granklog_cnt,t2.err_granklog_per,t2.err_sm_priacct_cnt,t2.err_firmlog_cnt,t2.err_firmlog_per,t2.err_cs_priacct_cnt,t2.err_staffid_cnt,t2.err_firmid_cnt,t2.err_firmid_per,t2.dimension_flag from
(select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),
  1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,
  firm_prvd_nm,sens_sys_opr_log_cnt,sens_opr_log_cnt,not_sens_opr_log_cnt,
  del_sys_log_acct_cnt,del_sys_log_cnt,sys_opr_acct_cnt,sys_opr_cnt,
  csry_sys_opr_acct_cnt,csry_sys_opr_log_cnt,csry_sys_opr_log_per,
  csry_zhqh_sys_acc_zhsl,csry_zhqh_sys_acc_cnt,csry_sens_opr_acct_cnt,
  csry_sens_opr_log_cnt,csry_sens_opr_log_per,person_sens_opr_acct_cnt,
  person_sens_opr_log_cnt,person_sens_opr_log_per,system_range_domain 
from HPBUS.sum_inf_sens_opr_1300)t1
full join
(select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),
  1) as format 'yyyymm') as char(6)) as aud_trm,rank_prvd , cast(firm_prvd_id as char(5)) as firm_prvd_id,
  firm_prvd_nm,err_system_cnt,err_custlog_cnt,err_granklog_cnt,err_granklog_per,err_sm_priacct_cnt,err_firmlog_cnt,err_firmlog_per,err_cs_priacct_cnt,err_staffid_cnt,err_firmid_cnt,err_firmid_per,
case dimension_flag 
 when '1' then '业支和网络' 
 when '2' then '业支域' 
 when '3' then '网络域' 
end as dimension_flag 
from hpbus.sum_xxaq_qxgk_1301)t2
on t1.aud_trm=t2.aud_trm
and t1.firm_prvd_id=t2.firm_prvd_id
and t1.system_range_domain=t2.dimension_flag;



---金库管控

CREATE MULTISET TABLE hpbus.sum_xxaq_jkgk_1302 ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      err_system_cnt INTEGER TITLE '涉及客户敏感信息系统数量',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      trea_exam_log_cnt INTEGER TITLE '金库审批日志数量',
      trea_err_log_cnt INTEGER TITLE '未经金库审批日志数量',
      trea_err_log_per DECIMAL(18,4) TITLE '未经金库审批日志数量占比',
      err_trea_cnt INTEGER TITLE '金库授权时长超规定的日志数量',
      err_trea_per DECIMAL(18,4) TITLE '金库授权时长超规定的日志数量占比',
      err_timelog_cnt INTEGER TITLE '其中:操作时长不在授权时长范围内的日志数量',
      err_timeoutlog_cnt INTEGER TITLE '其中:授权时长超过规定时长的日志数量(3h)',
      err_treaexam_cnt INTEGER TITLE '金库模式审批人设置不当的日志数量',
      err_treaexam_per DECIMAL(18,4) TITLE '金库模式审批人设置不当的日志数量占比',
      err_treadept_cnt INTEGER TITLE '其中:金库审批人与申请人不属同一部门的日志数量',
      err_treafirm_cnt INTEGER TITLE '其中:金库审批人为厂商人员的日志数量',
	  err_treafirm_acct_cnt INTEGER TITLE '其中:金库审批人为厂商人员涉及的账号数量（个）',
      dimension_flag CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '维度标识')
PRIMARY INDEX ( firm_prvd_id )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);

Replace view hpbus.sum_xxaq_jkgk_1302_vt as  locking hpbus.sum_xxaq_jkgk_1302 access 
 select  rank_prvd,cast(cast(ADD_MONTHS(CAST (Aud_trm||'01' AS DATE format 'YYYYMMDD'),1) as format 'YYYYMM') as char(6)) as  Aud_trm
,firm_prvd_id,firm_prvd_nm,err_system_cnt,err_custlog_cnt,trea_exam_log_cnt,trea_err_log_cnt,trea_err_log_per,err_trea_cnt,err_trea_per,err_timelog_cnt,err_timeoutlog_cnt,err_treaexam_cnt,err_treaexam_per,err_treadept_cnt,err_treafirm_cnt,err_treafirm_acct_cnt,dimension_flag from hpbus.sum_xxaq_jkgk_1302;

------------------系统汇总---------------------------------------
--- 账号权限管理违规地市&系统排名汇总
CREATE MULTISET TABLE hpbus.sum_give_sys_acc_prvd_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  organ_path VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '地市或省公司部门',
	  system_range_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
	  have_sys_access_cnt INTEGER TITLE '具备管理员操作权限的账号数量（个）',
	  inf_have_sys_access_cnt INTEGER TITLE '违规具备管理员操作权限的厂商人员账号数量（个）',
	  sys_access_per DECIMAL(18,4) TITLE '违规具备管理员操作权限的厂商人员账号数量占比',
	  have_sens_opr_cnt INTEGER TITLE '具备敏感信息操作权限的账号数量（个）',
	  inf_have_sens_opr_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员账号数量（个）',
	  sens_opr_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员账号数量占比',
	  sum_range VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '汇总维度'
      )
PRIMARY INDEX (firm_prvd_id);


CREATE MULTISET TABLE hpbus.sum_xxaq_qxgk_1301_sys ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      system_cd VARCHAR(20) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称编码',
      system_nm VARCHAR(120) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
      system_range_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      err_granklog_cnt INTEGER TITLE '未经授权操作客户敏感信息日志数量',
      err_granklog_per DECIMAL(18,4) TITLE '未经授权操作客户敏感信息日志数量占比',
      err_firmlog_cnt INTEGER TITLE '厂商人员违规操作客户敏感信息日志数量',
      err_firmlog_per DECIMAL(18,4) TITLE '厂商人员违规操作客户敏感信息日志数量占比',
      err_staffid_cnt INTEGER TITLE '具备敏感信息操作权限的工号数量',
      err_firmid_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员工号数量',
      err_firmid_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员工号数量占比')
PRIMARY INDEX ( firm_prvd_id ,system_cd )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);


---权限管控web页面系统汇总维度
REPLACE VIEW HPBUS.sum_give_sys_acc_1301_web_sys_vt AS 
LOCKING HPBUS.sum_give_sys_acc_prvd_1300 ACCESS 
LOCKING HPBUS.sum_xxaq_qxgk_1301_sys ACCESS 
select coalesce(t1.aud_trm,t2.aud_trm) as aud_trm,coalesce(t1.firm_prvd_id,t2.firm_prvd_id) as firm_prvd_id,coalesce(t1.firm_prvd_nm,t2.firm_prvd_nm) as firm_prvd_nm,t1.rn,t1.system_range_nm,t1.system_range_domain,t1.have_sys_access_cnt,t1.inf_have_sys_access_cnt,t1.sys_access_per,t1.have_sens_opr_cnt,t1.inf_have_sens_opr_cnt,t1.sens_opr_per,t2.rank_prvd,t2.system_cd,t2.system_range_domain_t2,t2.err_custlog_cnt,t2.err_granklog_cnt,t2.err_granklog_per,t2.err_firmlog_cnt,t2.err_firmlog_per,t2.err_staffid_cnt,t2.err_firmid_cnt,t2.err_firmid_per from 
(select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),
  1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,system_range_nm,system_range_domain,have_sys_access_cnt,inf_have_sys_access_cnt,sys_access_per,have_sens_opr_cnt,inf_have_sens_opr_cnt,sens_opr_per from HPBUS.sum_give_sys_acc_prvd_1300 where sum_range ='系统汇总'
)t1
full join
(select rank_prvd,cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,cast(firm_prvd_id as char(5)) as firm_prvd_id,firm_prvd_nm,system_cd,system_nm,system_range_nm as system_range_domain_t2,err_custlog_cnt,err_granklog_cnt,err_granklog_per,err_firmlog_cnt,err_firmlog_per,err_staffid_cnt,err_firmid_cnt,err_firmid_per from HPBUS.sum_xxaq_qxgk_1301_sys 
)t2
on t1.aud_trm=t2.aud_trm
and t1.firm_prvd_id=t2.firm_prvd_id
and t1.system_range_domain=t2.system_range_domain_t2
and t1.system_range_nm=t2.system_nm;

--- 涉敏操作管理违规地市&系统排名汇总
CREATE MULTISET TABLE hpbus.sum_inf_sens_opr_prvd_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  organ_path VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '地市或省公司部门',
	  system_range_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
	  sens_sys_opr_log_cnt INTEGER TITLE '涉敏系统的操作日志总数量（条）',
      sens_opr_log_cnt INTEGER TITLE '其中：涉敏操作日志数量（条）',
      not_sens_opr_log_cnt INTEGER TITLE '其中：不涉敏操作日志数量（条）',
      del_sys_log_acct_cnt INTEGER TITLE '违规删改系统日志涉及的账号数量（个）',
      del_sys_log_cnt INTEGER TITLE '违规删改系统日志次数（次）',
      sys_opr_acct_cnt INTEGER TITLE '违规进行管理员操作涉及的账号总数量（个）',
      sys_opr_cnt INTEGER TITLE '违规进行管理员操作的总次数（次）',
      csry_sys_opr_acct_cnt INTEGER TITLE '厂商人员违规进行管理员操作涉及的账号数量（个）',
      csry_sys_opr_log_cnt INTEGER TITLE '厂商人员违规进行管理员操作的日志数量（条）',
      csry_sys_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行管理员操作的日志数量占比',
      csry_zhqh_sys_acc_zhsl INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限涉及的账号数量（个）',
      csry_zhqh_sys_acc_cnt INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限的次数（次）',
      csry_sens_opr_acct_cnt INTEGER TITLE '厂商人员违规进行涉敏操作涉及的账号数量（个）',
      csry_sens_opr_log_cnt INTEGER TITLE '厂商人员违规进行涉敏操作的日志数量（条）',
      csry_sens_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行涉敏操作的日志数量占比',
      person_sens_opr_acct_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作涉及的账号数量（个）',
      person_sens_opr_log_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量（条）',
      person_sens_opr_log_per DECIMAL(18,4) TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量占比',
	  sum_range VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '汇总维度'
      )
PRIMARY INDEX (firm_prvd_id);


CREATE MULTISET TABLE hpbus.sum_xxaq_qxgk_1301_sys ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      system_cd VARCHAR(20) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称编码',
      system_nm VARCHAR(120) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
      system_range_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      err_granklog_cnt INTEGER TITLE '未经授权操作客户敏感信息日志数量',
      err_granklog_per DECIMAL(18,4) TITLE '未经授权操作客户敏感信息日志数量占比',
      err_firmlog_cnt INTEGER TITLE '厂商人员违规操作客户敏感信息日志数量',
      err_firmlog_per DECIMAL(18,4) TITLE '厂商人员违规操作客户敏感信息日志数量占比',
      err_staffid_cnt INTEGER TITLE '具备敏感信息操作权限的工号数量',
      err_firmid_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员工号数量',
      err_firmid_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员工号数量占比')
PRIMARY INDEX ( firm_prvd_id ,system_cd )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);


---涉敏操作web页面系统汇总维度
REPLACE VIEW HPBUS.sum_inf_sens_opr_1303_web_sys_vt AS 
LOCKING HPBUS.sum_inf_sens_opr_prvd_1300 ACCESS 
LOCKING HPBUS.sum_xxaq_qxgk_1301_sys ACCESS 
select coalesce(t1.aud_trm,t2.aud_trm) as aud_trm,coalesce(t1.firm_prvd_id,t2.firm_prvd_id) as firm_prvd_id,coalesce(t1.firm_prvd_nm,t2.firm_prvd_nm) as firm_prvd_nm,t1.rn,t1.system_range_nm,t1.system_range_domain,t1.sens_sys_opr_log_cnt,t1.sens_opr_log_cnt,t1.not_sens_opr_log_cnt,t1.del_sys_log_acct_cnt,t1.del_sys_log_cnt,t1.sys_opr_acct_cnt,t1.sys_opr_cnt,t1.csry_sys_opr_acct_cnt,t1.csry_sys_opr_log_cnt,t1.csry_sys_opr_log_per,t1.csry_zhqh_sys_acc_zhsl,t1.csry_zhqh_sys_acc_cnt,t1.csry_sens_opr_acct_cnt,t1.csry_sens_opr_log_cnt,t1.csry_sens_opr_log_per,t1.person_sens_opr_acct_cnt,t1.person_sens_opr_log_cnt,t1.person_sens_opr_log_per,t2.rank_prvd,t2.system_cd,t2.system_range_domain_t2,t2.err_custlog_cnt,t2.err_granklog_cnt,t2.err_granklog_per,t2.err_firmlog_cnt,t2.err_firmlog_per,t2.err_staffid_cnt,t2.err_firmid_cnt,t2.err_firmid_per from 
(select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),
  1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,system_range_nm,system_range_domain,
sens_sys_opr_log_cnt,sens_opr_log_cnt,not_sens_opr_log_cnt,del_sys_log_acct_cnt,del_sys_log_cnt,sys_opr_acct_cnt,sys_opr_cnt,csry_sys_opr_acct_cnt,csry_sys_opr_log_cnt,csry_sys_opr_log_per,csry_zhqh_sys_acc_zhsl,csry_zhqh_sys_acc_cnt,csry_sens_opr_acct_cnt,csry_sens_opr_log_cnt,csry_sens_opr_log_per,person_sens_opr_acct_cnt,person_sens_opr_log_cnt,person_sens_opr_log_per from HPBUS.sum_inf_sens_opr_prvd_1300 where sum_range ='系统汇总'
)t1
full join
(select rank_prvd,cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,cast(firm_prvd_id as char(5)) as firm_prvd_id,firm_prvd_nm,system_cd,system_nm,system_range_nm as system_range_domain_t2,err_custlog_cnt,err_granklog_cnt,err_granklog_per,err_firmlog_cnt,err_firmlog_per,err_staffid_cnt,err_firmid_cnt,err_firmid_per from HPBUS.sum_xxaq_qxgk_1301_sys 
)t2
on t1.aud_trm=t2.aud_trm
and t1.firm_prvd_id=t2.firm_prvd_id
and t1.system_range_domain=t2.system_range_domain_t2
and t1.system_range_nm=t2.system_nm;


CREATE MULTISET TABLE hpbus.sum_xxaq_jkgk_1302_sys ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      system_cd VARCHAR(20) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称编码',
      system_nm VARCHAR(120) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
      system_range_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      trea_exam_log_cnt INTEGER TITLE '金库审批日志数量',
      trea_err_log_cnt INTEGER TITLE '未经金库审批日志数量',
      trea_err_log_per DECIMAL(18,4) TITLE '未经金库审批日志数量占比',
      err_trea_cnt INTEGER TITLE '金库授权时长超规定的日志数量',
      err_trea_per DECIMAL(18,4) TITLE '金库授权时长超规定的日志数量占比',
      err_timelog_cnt INTEGER TITLE '其中:操作时长不在授权时长范围内的日志数量',
      err_timeoutlog_cnt INTEGER TITLE '其中:授权时长超过规定时长的日志数量(3h)',
      err_treaexam_cnt INTEGER TITLE '金库模式审批人设置不当的日志数量',
      err_treaexam_per DECIMAL(18,4) TITLE '金库模式审批人设置不当的日志数量占比',
      err_treadept_cnt INTEGER TITLE '其中:金库审批人与申请人不属同一部门的日志数量',
      err_treafirm_cnt INTEGER TITLE '其中:金库审批人为厂商人员的日志数量')
PRIMARY INDEX ( firm_prvd_id ,system_cd )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);

CREATE MULTISET TABLE hpbus.det_xxaq_jkgk_1302 ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      opr_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感日志操作ID',
      firm_prvd_id SMALLINT TITLE '省份编码',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      pri_acct VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号',
      pri_acct_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型编码',
      pri_acct_type_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型名称',
      bra_accnt VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '从账号',
      system_cd CHAR(3) CHARACTER SET LATIN CASESPECIFIC TITLE '系统编号',
      system_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
      system_range_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      cty_dept_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '所在地市或部门',
      drive_type_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '操作类型编号',
      drive_type_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '操作类型名称',
      drive_content VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '操作内容',
      drive_dt CHAR(14) CHARACTER SET LATIN CASESPECIFIC TITLE '操作时间',
      sens_data_nm VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据名称',
      sens_data_range_cd VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围编码',
      sens_data_range VARCHAR(400) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围',
      sens_data_type_cd VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别编码',
      sens_data_type_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别',
      sens_location_level VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据定位级别',
      sens_location_level_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据定位级别名称',
      trea_exam_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '金库申请/审批ID',
      trea_app_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '申请人',
      trea_app_dept VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '申请人所在部门',
      exam_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人',
      exam_acct_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人主账号类型编码',
      exam_acct_type_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人主账号类型名称',
      exam_id_dept VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人所在部门',
      app_grant_type_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '申请授权类型编码',
      app_grant_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '申请授权类型',
      grant_id CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '授权模式编码',
      grant_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '授权模式',
      exam_dt VARCHAR(14) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '审批时间',
      grant_effect_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '授权结果编码',
      grant_effect VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '授权结果',
      trea_eff_dt CHAR(14) CHARACTER SET LATIN CASESPECIFIC TITLE '生效时间',
      trea_end_dt CHAR(14) CHARACTER SET LATIN CASESPECIFIC TITLE '失效时间',
      grant_duration DECIMAL(18,2) TITLE '授权时长',
      grant_info VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '授权信息',
      err_exam_sens CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否未经金库审批违规',
      err_grant_time CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否操作时长不在授权时长范围内违规',
      err_grant_duration CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否授权时长违规(超3h)',
      err_exam_apply CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否审批人与申请人同部门违规',
      err_exam_firm CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否审批人为厂商违规')
PRIMARY INDEX ( opr_id ,firm_prvd_id ,pri_acct ,drive_content ,
drive_dt ,trea_exam_id )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE MULTISET TABLE hpbus.det_xxaq_qxgk_1301 ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      opr_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感日志操作ID',
      firm_prvd_id SMALLINT TITLE '省份编码',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      pri_acct VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号',
      pri_acct_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型编码',
      pri_acct_type_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型名称',
      bra_acct VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '从账号',
      system_cd CHAR(3) CHARACTER SET LATIN CASESPECIFIC TITLE '系统编号',
      system_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
      system_range_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      cty_dept_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '所在地市或部门',
      drive_type_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '操作类型编号',
      drive_type_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '操作类型名称',
      drive_content VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '操作内容',
      sens_data_nm VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据名称',
      sens_data_range_cd VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围编码',
      sens_data_range VARCHAR(400) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围',
      sens_data_type_cd VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别编码',
      sens_data_type_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别',
      sens_location_level VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据定位级别',
      sens_location_level_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据定位级别名称',
      sens_access_str VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '涉敏权限内容',
      opr_access VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '操作权限编码',
      opr_access_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '操作权限',
      err_drive_sens_data CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否未经授权操作敏感信息违规',
      err_firm_grant_data CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否厂商操作敏感信息违规',
      err_drive_firm_data CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否厂商具有敏感信息操作权限违规')
PRIMARY INDEX ( aud_trm ,opr_id ,firm_prvd_id ,pri_acct ,drive_content )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);


-----------------------地市汇总----------------------------------------
CREATE MULTISET TABLE hpbus.sum_xxaq_qxgk_1301_cty ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      cty_dept_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '地市或省公司部门',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      err_granklog_cnt INTEGER TITLE '未经授权操作客户敏感信息日志数量',
      err_granklog_per DECIMAL(18,4) TITLE '未经授权操作客户敏感信息日志数量占比',
      err_sm_priacct_cnt INTEGER TITLE '非涉敏人员违规操作客户敏感信息涉及主账号数量',
      err_firmlog_cnt INTEGER TITLE '厂商人员违规操作客户敏感信息日志数量',
      err_firmlog_per DECIMAL(18,4) TITLE '厂商人员违规操作客户敏感信息日志数量占比',
      err_cs_priacct_cnt INTEGER TITLE '厂商人员违规操作客户敏感信息涉及主账号数量',
      err_staffid_cnt INTEGER TITLE '具备敏感信息操作权限的工号数量',
      err_firmid_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员工号数量',
      err_firmid_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员工号数量占比')
PRIMARY INDEX ( firm_prvd_id ,cty_dept_nm )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);


CREATE MULTISET TABLE hpbus.sum_xxaq_jkgk_1302_cty ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      rank_prvd INTEGER TITLE '排名',
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      firm_prvd_id SMALLINT TITLE '省份标识',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      cty_dept_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '地市或省公司部门',
      err_custlog_cnt INTEGER TITLE '客户敏感信息操作日志数量',
      trea_exam_log_cnt INTEGER TITLE '金库审批日志数量',
      trea_err_log_cnt INTEGER TITLE '未经金库审批日志数量',
      trea_err_log_per DECIMAL(18,4) TITLE '未经金库审批日志数量占比',
      err_trea_cnt INTEGER TITLE '金库授权时长超规定的日志数量',
      err_trea_per DECIMAL(18,4) TITLE '金库授权时长超规定的日志数量占比',
      err_timelog_cnt INTEGER TITLE '其中:操作时长不在授权时长范围内的日志数量',
      err_timeoutlog_cnt INTEGER TITLE '其中:授权时长超过规定时长的日志数量(3h)',
      err_treaexam_cnt INTEGER TITLE '金库模式审批人设置不当的日志数量',
      err_treaexam_per DECIMAL(18,4) TITLE '金库模式审批人设置不当的日志数量占比',
      err_treadept_cnt INTEGER TITLE '其中:金库审批人与申请人不属同一部门的日志数量',
      err_treafirm_cnt INTEGER TITLE '其中:金库审批人为厂商人员的日志数量')
PRIMARY INDEX ( firm_prvd_id ,cty_dept_nm )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);

