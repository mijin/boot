---	账号权限管理违规审计清单
CREATE MULTISET TABLE hpbus.det_give_sys_acc_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  pri_acct VARCHAR(180) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号',
      pri_acct_type VARCHAR(20) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型',
	  bra_acct VARCHAR(700) CHARACTER SET LATIN CASESPECIFIC TITLE '从账号',
      bra_acct_type VARCHAR(20) CHARACTER SET LATIN CASESPECIFIC TITLE '从账号类型',
	  system_range_cd VARCHAR(3) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统类型编号',
	  system_range_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统类型名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
	  belong_dept VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '所在地市或部门',
	  sens_access_str VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '涉敏权限内容',
	  opr_access VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '操作权限',
	  if_give_sens_access VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否违规授予厂商人员涉敏操作权限',
	  if_give_sys_access VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否违规授予厂商人员管理员操作权限',
	  infrac_type VARCHAR(60) CHARACTER SET LATIN CASESPECIFIC TITLE '违规类型'
      )
PRIMARY INDEX (pri_acct, bra_acct);

comment on table hpbus.det_give_sys_acc_1300 '账号权限管理违规审计清单';

--视图
REPLACE VIEW HPBUS.det_give_sys_acc_1300_vt AS LOCKING HPBUS.det_give_sys_acc_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,firm_prvd_id,firm_prvd_nm,pri_acct,pri_acct_type,bra_acct,bra_acct_type,system_range_cd,system_range_nm,system_range_domain,belong_dept,sens_access_str,opr_access,if_give_sens_access,if_give_sys_access,infrac_type from HPBUS.det_give_sys_acc_1300;



---	涉敏操作管理违规审计清单
CREATE MULTISET TABLE hpbus.det_sens_opr_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  opr_id VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '操作日志ID',
	  pri_acct VARCHAR(180) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号',
      pri_acct_type VARCHAR(20) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型',
	  bra_acct VARCHAR(700) CHARACTER SET LATIN CASESPECIFIC TITLE '从账号',
	  system_range_cd VARCHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '系统编号',
	  system_range_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
	  belong_dept VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '所在地市或部门',
	  opr_type_nm VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作类型名称',
	  opr_descri VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '操作内容',
	  sens_nm VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据名称',
	  sens_range VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围',
      sens_sort VARCHAR(120) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别',
      sens_location_level VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '敏感数据级别',
	  sens_access_str VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '涉敏权限内容',
	  opr_access VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '操作权限',
	  if_infrac_del_log VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否违规删改系统日志',
	  if_infrac_sys_opr VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否厂商人员违规进行管理员操作',
	  if_switch_sys_access VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否厂商人员通过账号切换的方式使用管理员权限',
	  if_infrac_sens_access VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否厂商人员违规进行涉敏操作',
	  if_overstep_sens_access VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '是否越权进行涉敏操作'
      )
PRIMARY INDEX (pri_acct, bra_acct);

comment on table hpbus.det_sens_opr_1300 '涉敏操作管理违规审计清单';

--视图
REPLACE VIEW HPBUS.det_sens_opr_1300_vt AS LOCKING HPBUS.det_sens_opr_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,firm_prvd_id,firm_prvd_nm,opr_id,pri_acct,pri_acct_type,bra_acct,system_range_cd,system_range_nm,system_range_domain,belong_dept,opr_type_nm,opr_descri,sens_nm,sens_range,sens_sort,sens_location_level,sens_access_str,opr_access,if_infrac_del_log,if_infrac_sys_opr,if_switch_sys_access,if_infrac_sens_access,if_overstep_sens_access from HPBUS.det_sens_opr_1300;


--- 账号权限管理违规排名汇总
CREATE MULTISET TABLE hpbus.sum_give_sys_acc_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  have_sys_access_cnt INTEGER TITLE '具备管理员操作权限的账号数量（个）',
	  inf_have_sys_access_cnt INTEGER TITLE '违规具备管理员操作权限的厂商人员账号数量（个）',
	  sys_access_per DECIMAL(18,4) TITLE '违规具备管理员操作权限的厂商人员账号数量占比',
	  have_sens_opr_cnt INTEGER TITLE '具备敏感信息操作权限的账号数量（个）',
	  inf_have_sens_opr_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员账号数量（个）',
	  sens_opr_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员账号数量占比',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_give_sys_acc_1300 '账号权限管理违规排名汇总';

--视图
REPLACE VIEW HPBUS.sum_give_sys_acc_1300_vt AS LOCKING HPBUS.sum_give_sys_acc_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,have_sys_access_cnt,inf_have_sys_access_cnt,sys_access_per,have_sens_opr_cnt,inf_have_sens_opr_cnt,sens_opr_per,system_range_domain from HPBUS.sum_give_sys_acc_1300;



--- 账号权限管理违规地市&系统排名汇总
CREATE MULTISET TABLE hpbus.sum_give_sys_acc_prvd_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  organ_path VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '地市或省公司部门',
	  system_range_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
	  have_sys_access_cnt INTEGER TITLE '具备管理员操作权限的账号数量（个）',
	  inf_have_sys_access_cnt INTEGER TITLE '违规具备管理员操作权限的厂商人员账号数量（个）',
	  sys_access_per DECIMAL(18,4) TITLE '违规具备管理员操作权限的厂商人员账号数量占比',
	  have_sens_opr_cnt INTEGER TITLE '具备敏感信息操作权限的账号数量（个）',
	  inf_have_sens_opr_cnt INTEGER TITLE '违规具备敏感信息操作权限的厂商人员账号数量（个）',
	  sens_opr_per DECIMAL(18,4) TITLE '违规具备敏感信息操作权限的厂商人员账号数量占比',
	  sum_range VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '汇总维度'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_give_sys_acc_prvd_1300 '账号权限管理违规地市&系统排名汇总';

--视图
REPLACE VIEW HPBUS.sum_give_sys_acc_prvd_1300_vt AS LOCKING HPBUS.sum_give_sys_acc_prvd_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,organ_path,system_range_nm,system_range_domain,have_sys_access_cnt,inf_have_sys_access_cnt,sys_access_per,have_sens_opr_cnt,inf_have_sens_opr_cnt,sens_opr_per,sum_range from HPBUS.sum_give_sys_acc_prvd_1300;



--- 涉敏操作管理违规排名汇总
CREATE MULTISET TABLE hpbus.sum_inf_sens_opr_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  sens_sys_opr_log_cnt INTEGER TITLE '涉敏系统的操作日志总数量（条）',
      sens_opr_log_cnt INTEGER TITLE '其中：涉敏操作日志数量（条）',
      not_sens_opr_log_cnt INTEGER TITLE '其中：不涉敏操作日志数量（条）',
      del_sys_log_acct_cnt INTEGER TITLE '违规删改系统日志涉及的账号数量（个）',
      del_sys_log_cnt INTEGER TITLE '违规删改系统日志次数（次）',
      sys_opr_acct_cnt INTEGER TITLE '违规进行管理员操作涉及的账号总数量（个）',
      sys_opr_cnt INTEGER TITLE '违规进行管理员操作的总次数（次）',
      csry_sys_opr_acct_cnt INTEGER TITLE '厂商人员违规进行管理员操作涉及的账号数量（个）',
      csry_sys_opr_log_cnt INTEGER TITLE '厂商人员违规进行管理员操作的日志数量（条）',
      csry_sys_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行管理员操作的日志数量占比',
      csry_zhqh_sys_acc_zhsl INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限涉及的账号数量（个）',
      csry_zhqh_sys_acc_cnt INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限的次数（次）',
      csry_sens_opr_acct_cnt INTEGER TITLE '厂商人员违规进行涉敏操作涉及的账号数量（个）',
      csry_sens_opr_log_cnt INTEGER TITLE '厂商人员违规进行涉敏操作的日志数量（条）',
      csry_sens_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行涉敏操作的日志数量占比',
      person_sens_opr_acct_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作涉及的账号数量（个）',
      person_sens_opr_log_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量（条）',
      person_sens_opr_log_per DECIMAL(18,4) TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量占比',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_inf_sens_opr_1300 '涉敏操作管理违规排名汇总';

--视图
REPLACE VIEW HPBUS.sum_inf_sens_opr_1300_vt AS LOCKING HPBUS.sum_inf_sens_opr_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,sens_sys_opr_log_cnt,sens_opr_log_cnt,not_sens_opr_log_cnt,del_sys_log_acct_cnt,del_sys_log_cnt,sys_opr_acct_cnt,sys_opr_cnt,csry_sys_opr_acct_cnt,csry_sys_opr_log_cnt,csry_sys_opr_log_per,csry_zhqh_sys_acc_zhsl,csry_zhqh_sys_acc_cnt,csry_sens_opr_acct_cnt,csry_sens_opr_log_cnt,csry_sens_opr_log_per,person_sens_opr_acct_cnt,person_sens_opr_log_cnt,person_sens_opr_log_per,system_range_domain from HPBUS.sum_inf_sens_opr_1300;


--- 涉敏操作管理违规地市&系统排名汇总
CREATE MULTISET TABLE hpbus.sum_inf_sens_opr_prvd_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  organ_path VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '地市或省公司部门',
	  system_range_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
	  sens_sys_opr_log_cnt INTEGER TITLE '涉敏系统的操作日志总数量（条）',
      sens_opr_log_cnt INTEGER TITLE '其中：涉敏操作日志数量（条）',
      not_sens_opr_log_cnt INTEGER TITLE '其中：不涉敏操作日志数量（条）',
      del_sys_log_acct_cnt INTEGER TITLE '违规删改系统日志涉及的账号数量（个）',
      del_sys_log_cnt INTEGER TITLE '违规删改系统日志次数（次）',
      sys_opr_acct_cnt INTEGER TITLE '违规进行管理员操作涉及的账号总数量（个）',
      sys_opr_cnt INTEGER TITLE '违规进行管理员操作的总次数（次）',
      csry_sys_opr_acct_cnt INTEGER TITLE '厂商人员违规进行管理员操作涉及的账号数量（个）',
      csry_sys_opr_log_cnt INTEGER TITLE '厂商人员违规进行管理员操作的日志数量（条）',
      csry_sys_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行管理员操作的日志数量占比',
      csry_zhqh_sys_acc_zhsl INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限涉及的账号数量（个）',
      csry_zhqh_sys_acc_cnt INTEGER TITLE '厂商人员通过账号切换的方式使用管理员权限的次数（次）',
      csry_sens_opr_acct_cnt INTEGER TITLE '厂商人员违规进行涉敏操作涉及的账号数量（个）',
      csry_sens_opr_log_cnt INTEGER TITLE '厂商人员违规进行涉敏操作的日志数量（条）',
      csry_sens_opr_log_per DECIMAL(18,4) TITLE '厂商人员违规进行涉敏操作的日志数量占比',
      person_sens_opr_acct_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作涉及的账号数量（个）',
      person_sens_opr_log_cnt INTEGER TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量（条）',
      person_sens_opr_log_per DECIMAL(18,4) TITLE '不在涉敏人员库中的人员违规进行涉敏操作的日志数量占比',
	  sum_range VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '汇总维度'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_inf_sens_opr_prvd_1300 '涉敏操作管理违规地市&系统排名汇总';

--视图
REPLACE VIEW HPBUS.sum_inf_sens_opr_prvd_1300_vt AS LOCKING HPBUS.sum_inf_sens_opr_prvd_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,organ_path,system_range_nm,system_range_domain,sens_sys_opr_log_cnt,sens_opr_log_cnt,not_sens_opr_log_cnt,del_sys_log_acct_cnt,del_sys_log_cnt,sys_opr_acct_cnt,sys_opr_cnt,csry_sys_opr_acct_cnt,csry_sys_opr_log_cnt,csry_sys_opr_log_per,csry_zhqh_sys_acc_zhsl,csry_zhqh_sys_acc_cnt,csry_sens_opr_acct_cnt,csry_sens_opr_log_cnt,csry_sens_opr_log_per,person_sens_opr_acct_cnt,person_sens_opr_log_cnt,person_sens_opr_log_per,sum_range from HPBUS.sum_inf_sens_opr_prvd_1300;



--- 日志内容不规范排名汇总
CREATE MULTISET TABLE hpbus.sum_log_cont_nonorms_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id VARCHAR(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  opr_log_cnt INTEGER TITLE '操作日志数量（条）',
      cont_nonomrs_log_cnt INTEGER TITLE '内容不规范的日志总数量（条）',
      cont_nonomrs_log_per DECIMAL(18,4) TITLE '内容不规范的日志数量占比',
      log_serveip_out_cnt INTEGER TITLE '服务端IP地址格式违规的日志数量（条）',
      log_clientip_out_cnt INTEGER TITLE '客户端IP地址格式违规的日志数量（条）',
      log_pri_acct_null_cnt INTEGER TITLE '主账号为空的日志数量（条）',
      log_bra_acct_null_cnt INTEGER TITLE '从账号为空的日志数量（条）',
      log_serveip_null_cnt INTEGER TITLE '服务端IP地址为空的日志数量（条）',
      log_clientip_null_cnt INTEGER TITLE '客户端IP地址为空的日志数量（条）',
      log_start_null_cnt INTEGER TITLE '操作开始时间为空的日志数量（条）',
      log_descri_null_cnt INTEGER TITLE '操作描述为空的日志数量（条）',
      system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域'
      )
PRIMARY INDEX (firm_prvd_id);

comment on table hpbus.sum_log_cont_nonorms_1300 '日志内容不规范排名汇总';

--视图
REPLACE VIEW HPBUS.sum_log_cont_nonorms_1300_vt AS LOCKING HPBUS.sum_log_cont_nonorms_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,opr_log_cnt,cont_nonomrs_log_cnt,cont_nonomrs_log_per,log_serveip_out_cnt,log_clientip_out_cnt,log_pri_acct_null_cnt,log_bra_acct_null_cnt,log_serveip_null_cnt, log_clientip_null_cnt,log_start_null_cnt,log_descri_null_cnt,system_range_domain from sum_log_cont_nonorms_1300;

--- 各单位上报日志情况敏感级别排名汇总
CREATE MULTISET TABLE hpbus.sum_unit_sens_rank_statis_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id varchar(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm varchar(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      oper_system_type varchar(3) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作系统类型',
	  oper_system_nm varchar(50) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作系统名称',
      sens_location_level varchar(20) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '敏感数据定位级别',
      sum_log_cnt INTEGER TITLE '日志总数量',
      front_log_cnt INTEGER TITLE '前台访问日志数量',
      backsg_log_cnt INTEGER  TITLE '后台访问日志数量',
      CMCC_prov_prvd_id SMALLINT TITLE 'CMCC省公司标识' NOT NULL)
PRIMARY INDEX (firm_prvd_id,oper_system_type,sens_location_level);

comment on table hpbus.sum_unit_sens_rank_statis_1300 '各单位上报日志的敏感级别排名汇总';

REPLACE VIEW HPBUS.sum_unit_sens_rank_statis_1300_vt AS LOCKING HPBUS.sum_unit_sens_rank_statis_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,system_range_domain,oper_system_type,oper_system_nm,sens_location_level,sum_log_cnt,front_log_cnt,backsg_log_cnt,CMCC_prov_prvd_id from HPBUS.sum_unit_sens_rank_statis_1300;

--- 各单位上报日志情况操作类型排名汇总
CREATE MULTISET TABLE hpbus.sum_unit_oper_type_statis_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id varchar(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm varchar(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      oper_system_type varchar(3) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作系统类型',
	  oper_system_nm varchar(50) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作系统名称',
      opr_type_cd varchar(2) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作类型编号',
	  opr_type_nm varchar(20) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作类型名称',
      sum_log_cnt INTEGER TITLE '日志总数量',
      front_log_cnt INTEGER TITLE '前台访问日志数量',
      backsg_log_cnt INTEGER  TITLE '后台访问日志数量',
      CMCC_prov_prvd_id SMALLINT TITLE 'CMCC省公司标识' NOT NULL)
PRIMARY INDEX (firm_prvd_id,oper_system_type,opr_type_cd);

comment on table hpbus.sum_unit_oper_type_statis_1300 '各单位上报日志的操作类型排名汇总';

REPLACE VIEW HPBUS.sum_unit_oper_type_statis_1300_vt AS LOCKING HPBUS.sum_unit_oper_type_statis_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,system_range_domain,oper_system_type,oper_system_nm,opr_type_cd,opr_type_nm,sum_log_cnt,front_log_cnt,backsg_log_cnt,CMCC_prov_prvd_id from HPBUS.sum_unit_oper_type_statis_1300;

--- 各单位上报日志情况操作凭证排名汇总
 CREATE MULTISET TABLE hpbus.sum_unit_oper_voucher_statis_1300, NO FALLBACK, 
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT 
     (
	  aud_trm VARCHAR(6) CHARACTER SET LATIN CASESPECIFIC TITLE '数据月',
	  rn SMALLINT TITLE '排名',
	  firm_prvd_id varchar(5) CHARACTER SET LATIN CASESPECIFIC TITLE '公司',
	  firm_prvd_nm varchar(50) CHARACTER SET LATIN CASESPECIFIC TITLE '公司名称',
	  system_range_domain VARCHAR(10) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      oper_system_type varchar(3) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作系统类型',
	  oper_system_nm varchar(50) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作系统名称',
      oper_voucher_cd varchar(2) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作凭证状态编码',
	  oper_voucher_nm varchar(20) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '操作凭证状态名称',
      sum_log_cnt INTEGER TITLE '日志总数量',
      front_log_cnt INTEGER TITLE '前台访问日志数量',
      backsg_log_cnt INTEGER  TITLE '后台访问日志数量',
      CMCC_prov_prvd_id SMALLINT TITLE 'CMCC省公司标识' NOT NULL)
PRIMARY INDEX (firm_prvd_id,oper_system_type,oper_voucher_cd);

comment on table hpbus.sum_unit_oper_voucher_statis_1300 '各单位上报日志的操作凭证排名汇总';

REPLACE VIEW HPBUS.sum_unit_oper_voucher_statis_1300_vt AS LOCKING HPBUS.sum_unit_oper_voucher_statis_1300 ACCESS 
select cast(cast(ADD_MONTHS(cast(aud_trm||'01' as date format 'yyyymmdd'),1) as format 'yyyymm') as char(6)) as aud_trm,rn,firm_prvd_id,firm_prvd_nm,system_range_domain,oper_system_type,oper_system_nm,oper_voucher_cd,oper_voucher_nm,sum_log_cnt,front_log_cnt,backsg_log_cnt,CMCC_prov_prvd_id from HPBUS.sum_unit_oper_voucher_statis_1300;

---金库管理审计清单
CREATE MULTISET TABLE hpbus.det_xxaq_jkgk_1302 ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      aud_trm VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC TITLE '审计月',
      opr_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感日志操作ID',
      firm_prvd_id SMALLINT TITLE '省份编码',
      firm_prvd_nm VARCHAR(120) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '公司',
      pri_acct VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号',
      pri_acct_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型编码',
      pri_acct_type_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '主账号类型名称',
      bra_accnt VARCHAR(300) CHARACTER SET LATIN CASESPECIFIC TITLE '从账号',
      system_cd CHAR(3) CHARACTER SET LATIN CASESPECIFIC TITLE '系统编号',
      system_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '系统名称',
      system_range_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '所属系统域',
      cty_dept_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '所在地市或部门',
      drive_type_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '操作类型编号',
      drive_type_nm VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '操作类型名称',
      drive_content VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '操作内容',
      drive_dt CHAR(14) CHARACTER SET LATIN CASESPECIFIC TITLE '操作时间',
      sens_data_nm VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据名称',
      sens_data_range_cd VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围编码',
      sens_data_range VARCHAR(400) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据范围',
      sens_data_type_cd VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别编码',
      sens_data_type_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据类别',
      sens_location_level VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据定位级别',
      sens_location_level_nm VARCHAR(200) CHARACTER SET LATIN CASESPECIFIC TITLE '敏感数据定位级别名称',
      trea_exam_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '金库申请/审批ID',
      trea_app_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '申请人',
      trea_app_dept VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '申请人所在部门',
      exam_id VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人',
      exam_acct_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人主账号类型编码',
      exam_acct_type_nm VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人主账号类型名称',
      exam_id_dept VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '审批人所在部门',
      app_grant_type_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '申请授权类型编码',
      app_grant_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '申请授权类型',
      grant_id CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '授权模式编码',
      grant_type VARCHAR(100) CHARACTER SET LATIN CASESPECIFIC TITLE '授权模式',
      exam_dt VARCHAR(14) CHARACTER SET LATIN NOT CASESPECIFIC TITLE '审批时间',
      grant_effect_cd CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '授权结果编码',
      grant_effect VARCHAR(50) CHARACTER SET LATIN CASESPECIFIC TITLE '授权结果',
      trea_eff_dt CHAR(14) CHARACTER SET LATIN CASESPECIFIC TITLE '生效时间',
      trea_end_dt CHAR(14) CHARACTER SET LATIN CASESPECIFIC TITLE '失效时间',
      grant_duration DECIMAL(18,2) TITLE '授权时长',
      grant_info VARCHAR(500) CHARACTER SET LATIN CASESPECIFIC TITLE '授权信息',
      err_exam_sens CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否未经金库审批违规',
      err_grant_time CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否操作时长不在授权时长范围内违规',
      err_grant_duration CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否授权时长违规(超3h)',
      err_exam_apply CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否审批人与申请人同部门违规',
      err_exam_firm CHAR(2) CHARACTER SET LATIN CASESPECIFIC TITLE '是否审批人为厂商违规')
PRIMARY INDEX ( opr_id ,firm_prvd_id ,pri_acct ,drive_content ,
drive_dt ,trea_exam_id )
PARTITION BY CAST((((((firm_prvd_id  MOD  10000 )* 10 )+ ((((aud_trm / 10000 ) MOD  2 )* 31 )* 12 ))+ (((aud_trm / 100 ) MOD  100 )* 31 ))+ (aud_trm  MOD  100 )) AS INTEGER);
