package com.hpe.cmca.service;

import com.hpe.cmca.DemoMybitsApplicationTests;
import com.hpe.cmca.pojo.XXAQAbsDetailsDo;
import com.hpe.cmca.pojo.XXAQAbsTabDo;
import com.hpe.cmca.pojo.XXAQParamData;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
public class IXXAQServiceTest extends DemoMybitsApplicationTests {


    @Autowired
    private IXXAQService xxaqService;

    @Test
    public void map() throws Exception {
    }

    @Test
    public void bottom() throws Exception {
    }

    @Test
    public void concern() throws Exception {
        XXAQParamData data = new XXAQParamData();
        data.setPrvdId("13100");
        data.setFlagId("2");
        data.setConcern("1302");
        data.setAudTrm("201909");
        Map<String, Object> res = xxaqService.concern(data);
        System.err.println(JSONObject.fromObject(res));
    }

    @Test
    public void tab() throws Exception {
        XXAQParamData data = new XXAQParamData();
        data.setPrvdId("13100");
        data.setConcern("1303");
        data.setAudTrm("202002");
        List<Map<String, Object>> res = xxaqService.tab(data);
        System.err.println(JSONObject.fromObject(res));
    }

    @Test
    public void detail() throws Exception {
        XXAQParamData data = new XXAQParamData();
        data.setPrvdId("13100");
        data.setAudTrm("202002");
        data.setParameterType("1");
        List<Map<String, Object>> res1 = xxaqService.detail(data);
        System.err.println(JSONArray.fromObject(res1));
        data.setAudTrm("201910");
        List<Map<String, Object>> res2 = xxaqService.detail(data);
        System.err.println(JSONArray.fromObject(res2));
        data.setAudTrm("201909");
        List<Map<String, Object>> res3 = xxaqService.detail(data);
        System.err.println(JSONArray.fromObject(res3));


    }

    @Test
    public void rank() throws Exception {
    }

    @Test
    public void point() throws Exception {
        XXAQParamData data = new XXAQParamData();
        data.setPrvdId("13100");
        data.setAudTrm("202002");
        data.setParameterType("1");
        data.setPriAcct("11");
        log.info("{}",xxaqService.point(data));
        data.setAudTrm("201912");
        log.info("{}",xxaqService.point(data));
    }

}