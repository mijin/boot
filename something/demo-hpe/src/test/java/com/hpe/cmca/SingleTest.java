package com.hpe.cmca;

import com.hpe.cmca.util.compont.TLinkedList;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

@Slf4j
public class SingleTest {
    public static class SQlMsg{
        private List<String> tables;
        private List<String> columns;
        private List<String> wheres;
        private List<String> orderBys;
    }


    @Test
    public void test1() {
        TLinkedList tLinkedList = new TLinkedList<String>().addObj("id").addObj("name").addObj("remark").addObj("pwd").addObj("amt").addObj("num");

        String tableNm = "hpmgtr";
        log.info("{}",selectPersonLike(null,"1","2",tLinkedList,tableNm));
    }


    public String selectPersonLike(final String id, final String firstName, final String lastName, final LinkedList<String> tLinkedList, String tableNm) {
        return new SQL() {{
            for (String column : tLinkedList) {
                SELECT(column);
            }
            FROM(tableNm);
            if (id != null) {
                WHERE("P.ID like ${id}");
            }
            if (firstName != null) {
                WHERE("P.FIRST_NAME like ${firstName}");
            }
            if (lastName != null) {
                WHERE("P.LAST_NAME like ${lastName}");
            }
            ORDER_BY("P.LAST_NAME");
        }}.toString();
    }
}
