package com.hpe.cmca;

 import com.hpe.cmca.config.datasource.DynamicDataSourceContextHolder;
 import com.hpe.cmca.enums.DataSourceType;
 import com.hpe.cmca.mapper.HpeMapper;
 import com.hpe.cmca.service.IXXAQService;
 import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
 import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
 import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoMybitsApplicationTests {
    @Autowired
    HpeMapper hpeMapper;




    @Test
    public void test1(){
        log.info("{}" );
    }

    @Test
    public void test2(){
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.teradata.name());
        log.info("{}",hpeMapper.td(null));
    }
    @Test
    public void test3(){
        DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.MASTER.name());
        log.info("{}",hpeMapper.mysql(null));
    }
}
