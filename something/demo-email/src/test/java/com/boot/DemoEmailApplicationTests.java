package com.boot;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.internet.MimeMessage;
import java.io.File;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoEmailApplicationTests {

    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void sendSimpleMail() throws Exception {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("123@qq.com");
        message.setTo("345@qq.com");
        message.setSubject("主题：百度");
        message.setText("helloEmail");

        mailSender.send(message);
    }

    @Test
    public void sendInlineMail() throws Exception {

        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("123@qq.com");
        helper.setTo("345@qq.com");
        helper.setSubject("主题：html");
        helper.setText("<h3><a href='http://www.baidu.com'>百度一下，你就知道</a></h3>", true);


        mailSender.send(mimeMessage);

    }
}
