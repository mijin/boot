package com.boot.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RabbitListener(queues = "first")//监听 ‘first’ 列队
public class Receiver {

    @RabbitHandler//对消息的处理方法
    public void process(String hello) {
        log.info("Receiver:{}" , hello);
    }
}
