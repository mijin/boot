package com.boot.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class Sender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * 生产者
     * 产生一个字符，发送到  ‘first’ 列队里面
     */
    public void send() {
        String context = "date:" + new Date();
        log.info("Sender:{}" ,context);
        this.rabbitTemplate.convertAndSend("first", context);
    }
}
