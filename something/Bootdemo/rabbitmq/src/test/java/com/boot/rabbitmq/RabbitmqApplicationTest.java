package com.boot.rabbitmq;

import com.boot.rabbitmq.model.Model;
import com.boot.rabbitmq.mqservice.Send;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitmqApplicationTest {

    @Autowired
    Send send;

    @Test
    public void test1(){
        long l = System.currentTimeMillis();
        for (int i = 0; i <1 ; i++) {
            send.sendModel(new Model(i,"张三","maybe"));
        }
        log.info("耗时:{}", System.currentTimeMillis() - l);
    }


    @Test
    public void test2(){

        List<Model> list = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            list.add(new Model(i,"张三","maybe"));
        }


        List<Model> collect = list.stream().filter(e -> e.getId() == 0).collect(Collectors.toList());

        log.info("collect:{}", collect  );
    }
}