package com.boot.rabbitmq.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Model implements Serializable {
    private int id;
    private String name;
    private String remark;
    private Date date;

    public Model(int id, String name, String remark, Date date) {
        this.id = id;
        this.name = name;
        this.remark = remark;
        this.date = date;
    }

    public Model(int id, String name, String remark) {
        this(id,name,remark,new Date());
    }


}
