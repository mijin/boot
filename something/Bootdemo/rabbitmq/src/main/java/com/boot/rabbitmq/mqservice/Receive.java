package com.boot.rabbitmq.mqservice;

import com.boot.rabbitmq.model.Model;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

/**
 * 消费者
 */
public interface Receive {

    void  reveive(Model model,  Channel channel,
                  Message message);
}
