package com.boot.rabbitmq.mqservice;

import com.boot.rabbitmq.model.Model;

/**
 * 生产者
 */
public interface Send {
    void sendModel(Model model);
}
