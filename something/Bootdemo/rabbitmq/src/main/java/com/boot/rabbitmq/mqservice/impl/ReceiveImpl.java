package com.boot.rabbitmq.mqservice.impl;

import com.boot.rabbitmq.config.RabbitmqConfig;
import com.boot.rabbitmq.model.Model;
import com.boot.rabbitmq.mqservice.Receive;
import com.boot.rabbitmq.util.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class ReceiveImpl implements Receive {


    @Override
    @RabbitListener(queues = RabbitmqConfig.token )
    public void reveive(@Payload Model model,Channel channel,
                        Message message) {
        RabbitMQUtils.askMessage(channel, message);
        log.info("收到mq消息：{}",model);
    }
}
