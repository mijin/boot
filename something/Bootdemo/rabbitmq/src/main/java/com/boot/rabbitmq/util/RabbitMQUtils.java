package com.boot.rabbitmq.util;

 import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
 import org.springframework.amqp.core.Message;

 import java.io.IOException;

public final class RabbitMQUtils {

    /**
     * 常用确认消息机制
     * @param channel
     * @param message
     */
    public static void askMessage(Channel channel, Message message) {
        askMessage(channel,message,false);
    }


    public static void askMessage(Channel channel, Message message,boolean flag) {
        try {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),flag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }






    public static void askMessage(Channel channel, long tag, final Logger logger) {
        askMessage(channel, tag, logger, false);
    }

    public static void askMessage(Channel channel, long tag, final Logger logger, boolean multiple) {
        try {
            channel.basicAck(tag, multiple);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void rejectMessage(Channel channel, long tag, final Logger logger) {
        rejectMessage(channel, tag, logger, false, false);
    }

    public static void rejectAndBackMQ(Channel channel, long tag, final Logger logger) {
        rejectMessage(channel, tag, logger, false, true);
    }

    public static void rejectMessage(Channel channel, long tag, final Logger logger, boolean multiple, boolean request) {
        try {
            channel.basicNack(tag, multiple, request);
        } catch (IOException e) {
            logger.error("RabbitMQ，IO异常，异常原因为：{}", e.getMessage());
        }
    }

}
