package com.boot.rabbitmq.mqservice.impl;

import com.boot.rabbitmq.config.RabbitmqConfig;
import com.boot.rabbitmq.model.Model;
import com.boot.rabbitmq.mqservice.Send;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SendImpl implements Send  {

    @Autowired
    private RabbitTemplate  rabbitTemplate;

    @Override
    public void sendModel(Model model) {
         rabbitTemplate.convertAndSend(RabbitmqConfig.token, model);
    }

}
