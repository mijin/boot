package com.boot.controller;

import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BootController {

    @RequestMapping("/test")
    public Object test(){
        return SpringVersion.class.getPackage();
    }

}
