package com.boot.nativesql.utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBHelper {
	public final static String className = "com.mysql.cj.jdbc.Driver";//驱动
	public final static String url = "jdbc:mysql://123.207.22.86:39000/car?characterEncoding=UTF-8&useSSL=false";
	public final static String user = "root";
	public final static String pw = "root";

	// 获取连接对象方法
	public final static Connection getConnection() {

		// 定义数据库连接对象
		Connection conn = null;
		// 加载连接SQLSERVER数据库的驱动
		try {
			Class.forName(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		// 通过驱动管理对象获得数据库连接对象
		try {
			conn = DriverManager.getConnection(url, user, pw);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public final static void closeConnection(Connection conn, Statement st, ResultSet rs) {
		// 关闭ResultSet对象
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		// 关闭Satement对象
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		// 关闭 Connection对象
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public final static void closeConnection(Connection conn) {
				closeConnection(conn,null,null);
	}

}


