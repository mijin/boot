package com.boot.nativesql;

import com.boot.nativesql.utils.DBHelper;
import lombok.extern.java.Log;
import org.junit.Test;

import java.sql.*;
import java.util.*;

@Log
public class NativeSqlTest {

    @Test
    public void count() {
        Connection connection = DBHelper.getConnection();
        PreparedStatement ps = null;
        String sql ="SELECT COUNT(*)  tcount FROM blog";

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery();
           int count = 1;
           while (resultSet.next()){
               ResultSetMetaData rsmd = resultSet.getMetaData();
               int columnCount = rsmd.getColumnCount();
               Map<String, Object> map = new LinkedHashMap<>(columnCount);
               String key =   rsmd.getColumnName(count);
               Object val = resultSet.getObject(count);
               map.put(key,val);
               log.info(map.toString());
               count++;
           }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBHelper.closeConnection(connection);
        }
    }



    @Test
    public void install() {
        Connection connection = DBHelper.getConnection();
        PreparedStatement ps = null;
        String sql ="INSERT INTO `car`.`blog`(`content`, `html_content`, `summary`, `title`) VALUES (?, ?, ?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1,"文本内容");
            statement.setString(2,"html文本内容");
            statement.setString(3,"summarysss");
            statement.setString(4,"标题");
             boolean execute = statement.execute();
            log.info("execute:"+execute);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBHelper.closeConnection(connection);
        }
    }


    @Test
    public void select() {
         Connection connection = DBHelper.getConnection();
         String sql ="SELECT  *  FROM blog LIMIT 10";
        try {
            List<Map<String,Object>> list = new ArrayList<>();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                ResultSetMetaData rsmd = resultSet.getMetaData();
                int columnCount = rsmd.getColumnCount();

                Map<String, Object> map = new LinkedHashMap<>(columnCount);
                for (int i = 1; i < columnCount; i++) {
                    String key =   rsmd.getColumnName(i);
                    Object val = resultSet.getObject(i);
                    map.put(key,val);
                }
                list.add(map);
             }
            log.info(list.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBHelper.closeConnection(connection);
        }
    }


    @Test
    public void test1() {
        boolean equals = Objects.equals("1", null);
    }



}
