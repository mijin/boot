package com.boot.template.controller;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootVersion;
 import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
public class InfoController {

    //模板引擎对象
    @Autowired
    private TemplateEngine templateEngine;
    @Autowired
    private Configuration configuration;
    @GetMapping("/info")
    public Package info() {
        return SpringBootVersion.class.getPackage();
    }

    @GetMapping("/getTemplate")
    public   Object getTemplate() {
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("title","用户名激活");
        Context context = new Context();
        context.setVariables(dataMap);
        String emailText = templateEngine.process("emailTemplates",context);
        dataMap.put("template",emailText);
        return dataMap;
    }

    @GetMapping("/getFreemark")
    public   Object getFreemark() throws IOException, TemplateException {
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("title", LocalDateTime.now());
        Template template = configuration.getTemplate("emailTemplates.ftl");
        String templateValue = FreeMarkerTemplateUtils.processTemplateIntoString(template, dataMap);
        dataMap.put("template",templateValue);
        return dataMap;
    }


}
