package com.boot.excel.model;

import com.boot.excel.annotation.Excel;

public class Model {
    @Excel(name = "序号")
    private Integer id ;
    @Excel(name = "名称")
    private String name;
    @Excel(name = "备注")
    private String remark;

    public Model(Integer id, String name, String remark) {
        this.id = id;
        this.name = name;
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRemark() {
        return remark;
    }
}
