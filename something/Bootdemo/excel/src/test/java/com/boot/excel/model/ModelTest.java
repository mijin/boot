package com.boot.excel.model;

import com.boot.excel.util.ExcelUtil;
import lombok.extern.java.Log;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@Log
public class ModelTest {
    @Test
    public void out() {
        List<Model> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(new Model(i,System.currentTimeMillis()+"","excel"+i));
        }
        ExcelUtil<Model>  excelUtil = new ExcelUtil(Model.class);
        String excel = excelUtil.exportExcel(list, "字典类型.xlsx");
        log.info(excel);
    }

}
