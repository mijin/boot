package com.boot.hello.model;

public   class SModulesBean {
    /**
     * id : 7ccdc9ec-4ac5-11e9-a023-00e04c561608
     * code : UserApp
     * name : �û��ƶ�ƽ̨
     */

    private String id;
    private String code;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
