package com.boot.hello.model;

import java.util.List;

public class TestModel {
    /**
     * name : 111
     * password : 111
     * sModules : [{"id":"7ccdc9ec-4ac5-11e9-a023-00e04c561608","code":"UserApp","name":"�û��ƶ�ƽ̨"},{"id":"d0dd822d-4ac4-11e9-a023-00e04c561608","code":"Firevisual","name":"�������ӻ�ϵͳ"}]
     * isEnable : true
     */

    private String name;
    private String password;
    private boolean isEnable;
    private List< SModulesBean> sModules;
    private List< String> list;

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsEnable() {
        return isEnable;
    }

    public void setIsEnable(boolean isEnable) {
        this.isEnable = isEnable;
    }

    public List< SModulesBean> getSModules() {
        return sModules;
    }

    public void setSModules(List< SModulesBean> sModules) {
        this.sModules = sModules;
    }




}
