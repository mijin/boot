package com.boot.hello.controller;

import com.boot.hello.model.SModulesBean;
import org.springframework.boot.SpringBootVersion;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
public class HelloController
{

    @RequestMapping("/")
    public Object hello() {
        return SpringBootVersion.class.getPackage();
    }


    @PostMapping("/pp")
    public Object post(@RequestBody List<SModulesBean> sModules) {
        sModules.get(0);
        return SpringVersion.class.getPackage();
    }


    @PostMapping("/pp2")
    public Object pp2(@RequestBody Map<String,Object> sModules) {
        String name = sModules.size()+"";
        return sModules;
    }


}
