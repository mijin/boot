package com.boot.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
public class ScheduledTasks {


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 5000)//每五秒运行一次
    public void reportCurrentTime() {
        log.info("时间： {}", dateFormat.format(new Date()));
    }


    /**
     * cron表达式，cron表达式
     * http://cron.qqe2.com/
     */
    @Scheduled(cron="*/10 * * * * *")//没10秒运行一次
    public void corn() {
        log.info("corn： {}", dateFormat.format(new Date()));
    }
}
