package com.tjh.starter.autoconf;

import com.tjh.starter.model.HelloService;
import com.tjh.starter.properties.HelloServiceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration //这是一个配置类
@EnableConfigurationProperties(HelloServiceProperties.class) //使用java类作为配置文件
@ConditionalOnClass(HelloService.class)//需要被配置的类
@ConditionalOnProperty(prefix = "tjh",value = "enable",matchIfMissing = true)
public class HelloServiceAutoconfiguration {
    @Autowired
    private HelloServiceProperties helloServiceProperties;

    @Bean
    @ConditionalOnMissingBean(HelloService.class)
    public HelloService helloService(){
        HelloService bean = new HelloService();
        bean.setMsg(helloServiceProperties.getMsg());
        return bean;
    }


}
