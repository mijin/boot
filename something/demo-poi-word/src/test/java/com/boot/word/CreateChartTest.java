package com.boot.word;

import com.boot.util.JFreeChartUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.Units;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
@Slf4j
public class CreateChartTest {



    @Test
    public void test1() {
        int i = Units.toEMU(14.60);
        log.info(i+"");
    }
    @Test
    public void test3() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//添加数据
        dataset.addValue(440, "数据", "类型1");
        dataset.addValue(360, "数据", "类型2");
        dataset.addValue(510, "数据", "类型3");
        dataset.addValue(390, "数据", "类型4");
        JFreeChart chart = JFreeChartUtil.zhutu("XXX统计图", "类型", "数据额", dataset);
        //
        saveAsFile(chart, "D:\\jfreechart\\ccc.png", 600, 400);

    }

    @Test
    public void test2() {

        //步骤1：创建XYDataset对象（准备数据）
        XYDataset dataset = createXYDataset();
        //步骤2：根据Dataset 生成JFreeChart对象，以及做相应的设置
        JFreeChart freeChart = createChart(dataset);
        //步骤3：将JFreeChart对象输出到文件，Servlet输出流等
        saveAsFile(freeChart, "D:\\jfreechart\\lineXY.png", 600, 400);
    }

    // 保存为文件
    public static void saveAsFile(JFreeChart chart, String outputPath,
                                  int weight, int height) {
        FileOutputStream out = null;
        try {
            File outFile = new File(outputPath);
            if (!outFile.getParentFile().exists()) {
                outFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(outputPath);
            // 保存为PNG
            ChartUtils.writeChartAsPNG(out, chart, weight, height);
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // do nothing
                }
            }
        }
    }

    // 根据XYDataset创建JFreeChart对象
    public static JFreeChart createChart(XYDataset dataset) {
        //        主题样式
        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
        //设置标题字体
        standardChartTheme.setExtraLargeFont(new Font("宋书",Font.BOLD,20));
        //设置图例的字体
        standardChartTheme.setRegularFont(new Font("宋书",Font.PLAIN,15));
        //设置轴向的字体
        standardChartTheme.setLargeFont(new Font("宋书",Font.PLAIN,15));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);
        // 创建JFreeChart对象：ChartFactory.createXYLineChart
        JFreeChart jfreechart = ChartFactory.createXYLineChart("流量欠费", // 标题
                "收入", // categoryAxisLabel （category轴，横轴，X轴标签）
                "年份", // valueAxisLabel（value轴，纵轴，Y轴的标签）
                dataset, // dataset
                PlotOrientation.VERTICAL,
                true, // legend
                false, // tooltips
                false); // URLs
        // 使用CategoryPlot设置各种参数。以下设置可以省略。
        XYPlot plot = (XYPlot) jfreechart.getPlot();

        return jfreechart;
    }

    /**
     * 创建XYDataset对象
     */
    private static XYDataset createXYDataset() {
//        XYSeries xyseries1 = new XYSeries("张三");
//         xyseries1.add(1987, 50);
//        xyseries1.add(1997, 20);
//        xyseries1.add(2007, 30);
//        xyseries1.add(2010, 30);
//        xyseries1.add(2017, 30);
//        XYSeries xyseries2 = new XYSeries("李四");
//        xyseries2.add(1987, 20);
//        xyseries2.add(1997, 10D);
//        xyseries2.add(2007, 40D);
//        xyseries2.add(2010, 40);
//        xyseries2.add(2017, 40);
        XYSeries xyseries3 = new XYSeries("王五");
        xyseries3.add(1987, 40);
        xyseries3.add(1997, 30.0008);
        xyseries3.add(2007, 38.24);
        xyseries3.add(2010, 50);
        xyseries3.add(2017, 50);
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
//        xySeriesCollection.addSeries(xyseries1);
//        xySeriesCollection.addSeries(xyseries2);
        xySeriesCollection.addSeries(xyseries3);
        return xySeriesCollection;
    }
}
