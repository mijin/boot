package com.boot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoPoiWordApplication.class)
public class DemoPoiWordApplicationTests {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected JdbcTemplate jdbcTemplate;



	@Test
	public void contextLoads() {
		log.info(jdbcTemplate.queryForObject("select '1'",String.class));
	}

}
