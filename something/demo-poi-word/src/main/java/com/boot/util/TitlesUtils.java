package com.boot.util;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TitlesUtils {

    private   ThreadLocal<TitlesUtils> local = new ThreadLocal<>();
    private static final List<String> list = Arrays.asList("一", "二", "三", "四", "五", "六", "七", "八", "九", "十");
    Map<String, Boolean> map1 = new LinkedHashMap<>(); //标题1
    Map<String, Boolean> map2 = new LinkedHashMap<>(); //标题2
    Map<String, Boolean> map3 = new LinkedHashMap<>(); //标题3

    public TitlesUtils() {
        for (String title : list) {
            map1.put(title, true);
            map2.put(title, true);
            map3.put(title, true);
        }
        local.set(this);
    }


    public String one() {
        String val = "";
        TitlesUtils titles = local.get();
        for (Map.Entry<String, Boolean> entry : titles.map1.entrySet()) {
            if (entry.getValue()) {
                val = entry.getKey();
                break;
            }
        }
        titles.map1.put(val,false);
        for (String item : list) { //clear
            titles.map2.put(item,true);
            titles.map3.put(item,true);
        }
        return val;
    }

    public String two() {
        String val = "";
        TitlesUtils titles = local.get();
        for (Map.Entry<String, Boolean> entry : titles.map2.entrySet()) {
            if (entry.getValue()) {
                val = entry.getKey();
                break;
            }
        }
        titles.map2.put(val,false);
        for (String item : list) { //clear
            titles.map3.put(item,true);
        }
        return val;
    }
    public String three() {
        String val = "";
        TitlesUtils titles = local.get();
        for (Map.Entry<String, Boolean> entry : titles.map2.entrySet()) {
            if (entry.getValue()) {
                val = entry.getKey();
                break;
            }
        }
        titles.map3.put(val,false);//标记已使用
        return val;
    }
}
