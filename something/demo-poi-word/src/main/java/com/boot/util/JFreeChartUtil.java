package com.boot.util;

 import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
 import org.jfree.chart.axis.NumberAxis;
 import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
 import org.jfree.chart.renderer.category.BarRenderer;
 import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.xy.XYDataset;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

public class JFreeChartUtil {
    /**
     * 折线
     *
     * @param dataset
     * @param asX
     * @param asY
     * @param chartTiltle
     * @return
     */
    public static JFreeChart createChart(XYDataset dataset, String asX, String asY, String chartTiltle) {
        //        主题样式
        StandardChartTheme standardChartTheme = new StandardChartTheme("CN");
        //设置标题字体
        standardChartTheme.setExtraLargeFont(new Font("宋书", Font.BOLD, 20));
        //设置图例的字体
        standardChartTheme.setRegularFont(new Font("宋书", Font.PLAIN, 15));
        //设置轴向的字体
        standardChartTheme.setLargeFont(new Font("宋书", Font.PLAIN, 15));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);
        // 创建JFreeChart对象：ChartFactory.createXYLineChart
        JFreeChart jfreechart = ChartFactory.createXYLineChart(chartTiltle, // 标题
                asX, // categoryAxisLabel （category轴，横轴，X轴标签）
                asY, // valueAxisLabel（value轴，纵轴，Y轴的标签）
                dataset, // dataset
                PlotOrientation.VERTICAL,
                true, // legend
                false, // tooltips
                false); // URLs
        // 使用CategoryPlot设置各种参数。以下设置可以省略。
        XYPlot plot = (XYPlot) jfreechart.getPlot();

        return jfreechart;
    }

    /**
     * @param title
     * @param asX
     * @param asY
     * @param dataset
     * @return
     */
    public static JFreeChart zhutu(String title, String asX, String asY, CategoryDataset dataset) {
        JFreeChart chart = ChartFactory.createBarChart(title, asX, asX, dataset, PlotOrientation.VERTICAL, true, false, false);
        CategoryPlot plot = chart.getCategoryPlot();//设置图的高级属性
        //对X轴做操作
        CategoryAxis domainAxis = plot.getDomainAxis();
//对Y轴做操作
        ValueAxis rAxis = plot.getRangeAxis();

/*----------设置消除字体的锯齿渲染（解决中文问题）--------------*/
        chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);

//设置标题字体
        TextTitle textTitle = chart.getTitle();
        textTitle.setFont(new Font("黑体", Font.PLAIN, 20));
//设置X轴坐标上的文字
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));
//设置X轴的标题文字
        domainAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));
//设置Y轴坐标上的文字
        rAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 12));
//设置Y轴的标题文字
        rAxis.setLabelFont(new Font("黑体", Font.PLAIN, 12));
//底部汉字乱码的问题
        chart.getLegend().setItemFont(new Font("宋体", Font.PLAIN, 12));
        //图片背景色
        plot.setBackgroundPaint(Color.WHITE);
        //设置网格竖线颜色
        plot.setDomainGridlinePaint(Color.WHITE);
        //设置网格横线颜色
        plot.setRangeGridlinePaint(Color.BLACK);
        //设置数据区的边界线条颜色
        plot.setOutlinePaint(Color.WHITE);

        //设置距离图片右端距离
        NumberAxis na = (NumberAxis) plot.getRangeAxis();
        na.setAutoRangeIncludesZero(true);
        DecimalFormat df = new DecimalFormat("#0.000");
        //数据轴数据标签的显示格式
        na.setNumberFormatOverride(df);
        BarRenderer renderer  = new BarRenderer();
        plot.setRenderer(renderer);

        renderer.setMaximumBarWidth(0.08); //设置柱子宽度
        renderer.setMinimumBarLength(0.1); //设置柱子高度
        renderer.setSeriesPaint(0,new Color(79,129,189)); //设置柱的颜色




        return chart;
    }


    public static String saveAsFile(JFreeChart chart, String outputPath,
                                    int weight, int height) {
        FileOutputStream out = null;
        try {
            File outFile = new File(outputPath);
            if (!outFile.getParentFile().exists()) {
                outFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(outputPath);
            // 保存为PNG
            ChartUtils.writeChartAsPNG(out, chart, weight, height);
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // do nothing
                }
            }
        }
        return outputPath;
    }


}
