package com.boot.util;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class JdbcTemplatePlus extends JdbcTemplate {
    private final static Map<String, BeanPropertyRowMapper> data = new HashMap<>();

    public JdbcTemplatePlus(DataSource dataSource) {
        super(dataSource);
    }

     public <T> List<T> getList(String sql,Class<T> clz) throws DataAccessException {
        if (data.containsKey(clz.getName())) {
            return super.query(sql, data.get(clz.getName()));

        }
        BeanPropertyRowMapper mapper = new BeanPropertyRowMapper(clz);
        data.put(clz.getName(), mapper);
        return super.query(sql, mapper);
    }
}
