package com.boot.model;


import lombok.Data;

import java.util.Date;

@Data
public class WordGenConfig {
    private Integer id;
    private String version;
    private Integer subjectId;
    private String audTrm;
    private Integer prvdId;
    private String remark;
    private String localPath;
    private String ftpPath;
    private Date genDate;
    private Integer autowird;
    private Integer success;
    private String wordNameTemplate;
    private String querySql;
}
