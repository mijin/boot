package com.boot.model;


import lombok.Data;

import java.io.File;

@Data
public class WordConfig {
    private Integer id;
    private String text;
    private String textTemplate; //模板内容
    private Integer fontSize;
    private String querySql; //sql
    private String countSql; //sql   0不显示 1显示
    private Integer titleLevel;   //标题级别
    private Integer isTable;   //是不是表格
    private Integer indentation; //首行缩进
    private String paragraphAlignment;
    private String fontFamily;
    private Integer blockCode;
    private Integer bold;
    private Integer splicing;
    private Integer subjectId;
    private String version;
    private String wordName;
    private String remark;
    private String imgFile;
}
