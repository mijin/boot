package com.boot.controller;

import com.boot.model.WordGenConfig;
import com.boot.service.IWordService;
import com.boot.util.JdbcTemplatePlus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/word")
public class WordController {

    @Autowired
    private JdbcTemplatePlus jdbcTemplate;

    @Autowired
    private IWordService wordService;

    //    生成ｗｏｒｄ
    @RequestMapping("/gen")
    public Object gen( ) {
        long start = System.currentTimeMillis();
        List<WordGenConfig> list = jdbcTemplate.getList("SELECT * FROM `word_gen_config` WHERE  success != 1",  WordGenConfig.class);
        for (WordGenConfig wordGenConfig : list) {
            wordService.genWord(wordGenConfig);
        }
        return String.format("time:%s ms",System.currentTimeMillis()-start );
    }

}
