package com.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPoiWordApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPoiWordApplication.class, args);
	}

}
