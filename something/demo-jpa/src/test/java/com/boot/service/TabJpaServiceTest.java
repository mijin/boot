package com.boot.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.boot.repository.TabJpaRpository;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.boot.DemoJpaApplicationTest;
import com.boot.dataObject.TabJpa;

@Component
public class TabJpaServiceTest extends DemoJpaApplicationTest {
    @Autowired
    private TabJpaService tabJpaService;
    @Autowired
    private TabJpaRpository tabJpaRpository;

    Logger log = LoggerFactory.getLogger(getClass());
    @Test
    public void save() {

        for (int index = 0; index < 20 ; index++) {
            TabJpa model = new TabJpa();
            model.setMsg("firstJpa");
            model.setRemark(LocalDateTime.now().toString());
            TabJpa save = tabJpaService.save(model);
            log.info("save:{}",save);
        }

        /*List<String> list = Arrays.asList("a", "s", "d", "f", "g", "h");
        list.stream().forEach(msg->{
            TabJpa model = new TabJpa();
            model.setMsg(msg);
            model.setRemark(new Date().toString());
            TabJpa save = tabJpaService.save(model);
            log.info("save:{}",save);
        });*/
    }

    @Test
    public void deleteById() {
        tabJpaService.deleteById(1);
    }

    @Test
    public void findById() {
        TabJpa jpa = tabJpaService.findById(1);
        log.info("jpa:{}",jpa);
    }

    @Test
    public void findAll() {
        List<TabJpa> all = tabJpaService.findAll();
        log.info("all:{}",all);
    }
    @Test
    public void findTabJpa() {
        TabJpa jpa = tabJpaService.findTabJpa("a");
        log.info("jpa:{}",jpa);
    }

    @Test
   public void page(){
        PageRequest pageable = PageRequest.of(1, 10);
        Page<TabJpa> all = tabJpaRpository.findAll(pageable);
        for (TabJpa tabJpa : all) {
            log.info("{}",tabJpa.toString());
        }

    }
}