package com.boot.service;

import com.boot.dataObject.TabJpa;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TabJpaService {
    /**
     * 新增or更新
     */
    TabJpa save(TabJpa model);

    /**
     * 删除
     */
    void deleteById(Integer id);


    /**
     * Load查询
     */
    TabJpa findById(Integer id);



    /**
     *  查询
     */
    List<TabJpa> findAll( );

    /**
     *  hql查询
     */
    TabJpa findTabJpa(@Param("msg") String msg);

}
