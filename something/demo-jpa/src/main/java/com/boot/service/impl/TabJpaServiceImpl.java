package com.boot.service.impl;

import com.boot.dataObject.TabJpa;
import com.boot.repository.TabJpaRpository;
import com.boot.service.TabJpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TabJpaServiceImpl implements TabJpaService {
    @Autowired
    private TabJpaRpository tabJpaRpository;

    @Override
    public TabJpa save(TabJpa model) {
        return tabJpaRpository.save(model);
    }

    @Override
    public void deleteById(Integer id) {
        TabJpa tabJpa = this.findById(id);
        tabJpaRpository.delete(tabJpa);
    }

    @Override
    public TabJpa findById(Integer id) {
        return tabJpaRpository.findById(id).get();
    }

    @Override
    public List<TabJpa> findAll() {
        return tabJpaRpository.findAll();
    }

    @Override
    public TabJpa findTabJpa(String msg) {
        return tabJpaRpository.findTabJpa(msg);
    }
}
