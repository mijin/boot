package com.boot.repository;

import com.boot.dataObject.TabJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TabJpaRpository extends JpaRepository<TabJpa,Integer> {
    @Query("from TabJpa t where t.msg=:msg")
    TabJpa findTabJpa(@Param("msg") String msg);
}
