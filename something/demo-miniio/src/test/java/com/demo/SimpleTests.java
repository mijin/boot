package com.demo;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@Slf4j
public class SimpleTests {
    @Test
    public void test01() throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        MinioClient minioClient = MinioClient.builder().endpoint("http://192.168.1.105:9000/").credentials("admin", "admin123").build();

        String bucketName = "tjh";
        // 创建InputStream上传
        File file = new File("C:\\Users\\佳佳\\Pictures\\微信图片_20221214155755.jpg");
        InputStream inputStream = new FileInputStream(file);

        long start = System.currentTimeMillis();
        String name = file.getName();

        // 上传流
        minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket(bucketName)
                        .object("2021/11/28/" + UUID.randomUUID() + "." + name.substring(name.lastIndexOf(".")) ).stream(inputStream, inputStream.available(), -1)
                        .build());
        inputStream.close();
        log.info("uploaded successfully 耗时：" + (System.currentTimeMillis() - start));
    }

}
