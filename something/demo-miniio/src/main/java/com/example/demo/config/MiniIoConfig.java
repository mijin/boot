package com.example.demo.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;

@Configurable
public class MiniIoConfig {

    @Bean
    MiniIoProp prop() {
        return new MiniIoProp();
    }

    @Bean
    MinioClient minioClient(MiniIoProp prop) {
        return MinioClient.builder().endpoint(prop.getEndpoint()).credentials(prop.getAccessKey(), prop.getSecretKey()).build();
    }


    @Data
    static class MiniIoProp {
        private String endpoint;
        private String accessKey;
        private String secretKey;
    }
}
