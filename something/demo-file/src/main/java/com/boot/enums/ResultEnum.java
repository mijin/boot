package com.boot.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {
    FILEISNULL(1,"文件为null")
    ,UPLOADSUCCESS(2,"上传成功");

    private Integer code;
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
