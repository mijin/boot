package com.boot.utils;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;

public class Accest extends Assert {
    /**
     * 传进来的参数不为空抛出异常
     *
     * @param object
     * @param message
     */
    public static void isNull(Object object, String message) {
        if (object != null) {
            Accest.castExcetion(message);
        }
    }

    /**
     * 传进来的参数为null抛出异常
     *
     * @param object
     * @param message
     */
    public static void notNull(Object object, String message) {
        if (object == null) {
            Accest.castExcetion(message);
        }
    }

    /**
     * 金额为0抛出异常
     *
     * @param decimal
     * @param message
     */
    public static void notZero(BigDecimal decimal, String message) {
        if (BigDecimal.ZERO.compareTo(decimal) == 0) {
            Accest.castExcetion(message);
        }
    }

    /**
     * a < b 抛出异常
     *
     * @param a
     * @param message
     */
    public static void compareTo(BigDecimal a, BigDecimal b, String message) {
        if (a.compareTo(b) < 0) {
            Accest.castExcetion(message);
        }
    }

    /**
     * 传进来的参数为空抛出异常
     *
     * @param str
     * @param message
     */
    public static void isEmpty(String str, String message) {
        if (StringUtils.isEmpty(str.trim())) {
            Accest.castExcetion(message);
        }
    }

    /**
     * 传进来的参数为空抛出异常
     *
     * @param str
     * @param message
     */
    public static void notNull(String str, String message) {
        if (StringUtils.isEmpty(str.trim())) {
            Accest.castExcetion(message);
        }
    }
    /**
     * 传进来的参数为true抛出异常
     *
     * @param message
     */
    public static void isTrue(boolean flag, String message) {
        if (flag) {
            Accest.castExcetion(message);
        }
    }

    /**
     *  @功能: 传进来的参数为true抛出异常
     *  @作者:jh @代号:金华
     *  @时间:2019年1月3日
     *  @param flag
     */
    public static void isTrue(boolean flag) {
        if (flag) {
            Accest.castExcetion(null);
        }
    }

    /**
     * @功能:抛出异常
     * @作者:jh @代号:金华
     * @时间:2019年1月3日
     * @param message
     */
    private static void castExcetion(String message) {
        if(StringUtils.isEmpty(message)) {
            throw new RuntimeException();
        }else {
            throw new RuntimeException(message);
        }
    }
}
