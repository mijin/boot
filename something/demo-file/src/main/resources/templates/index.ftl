<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="uploadForm">
    <input id="file" type="file" name="file"/>
    <button id="upload" type="button" onclick="fileUpload()">upload</button>
</div>

<h1>图片回显</h1>
<hr/>
<img src=""/>
<script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
<script  >
    function fileUpload() {
        var formData = new FormData();
        formData.append('file', $('#file')[0].files[0]);
        $.ajax({
            url: '/fileUpload',
            type: 'POST',
            cache: false,
            data: formData,
            processData: false,
            contentType: false
        }).done(function (res) {
          $('img').attr('src',res);
        }).fail(function (res) {

        });
    }
</script>
</body>
</html>