package com.boot.service;

import com.boot.dataObject.TabMybits;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TabMybitsService {
    /**
     * 新增or更新
     */
    TabMybits save(TabMybits model);


    /**
     * Load查询
     */
    TabMybits findById(Integer id);


    /**
     * 查询
     */
    List<TabMybits> findAll();

    /**
     * sql注解查询
     */
    TabMybits findTabMybits(String msg);


    void delete(Integer id);
}
