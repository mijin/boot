package com.boot.service.impl;

import com.boot.dataObject.TabMybits;
import com.boot.repository.TabMybitsRpository;
import com.boot.service.TabMybitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TabMybitsServiceImpl implements TabMybitsService {
    @Autowired
    private TabMybitsRpository tabMybitsRpository;

    @Override
    public TabMybits save(TabMybits model) {
        return tabMybitsRpository.save(model);
    }

    @Override
    public TabMybits findById(Integer id) {
        return tabMybitsRpository.findById(id);
    }

    @Override
    public List<TabMybits> findAll() {
        return tabMybitsRpository.findAll();
    }

    @Override
    public TabMybits findTabMybits(String msg) {
        return tabMybitsRpository.findTabMybits(msg);
    }

    @Override
    public void delete(Integer id) {
        tabMybitsRpository.delete(id);
    }
}
