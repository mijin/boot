package com.boot.framework.handler;

public class CountHandler implements CreateSqlByMethod {
    @Override
    public String methodName() {
        return "count";
    }

    @Override
    public String sql(String tableName, Object[] args) {
        return String.format("select count(*) from  %s", tableName);
    }
}
