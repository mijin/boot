package com.boot.framework;

import java.util.List;
import java.util.Map;

public interface MapperRepository {

    /**
     * 查询所有
     *
     * @return
     */
    List<Map<String, Object>> selectAll();

    Integer count();

}
