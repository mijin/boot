package com.boot.framework.handler;

public interface CreateSqlByMethod {
    /**方法名称
     * @return
     */
    String methodName();

    /**生成sql
     *
     * @param tableName
     * @param args
     * @return
     */
    String sql(String tableName,Object[] args);
}
