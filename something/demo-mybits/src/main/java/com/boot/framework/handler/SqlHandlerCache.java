package com.boot.framework.handler;

import java.util.HashMap;
import java.util.Map;

public abstract class SqlHandlerCache {

    private static Map<String, CreateSqlByMethod> map = new HashMap<>();

    public static CreateSqlByMethod get(String methodName){
        if (map.isEmpty()) {
            init();
        }
        return map.get(methodName);
    }


    private static void init(){
        CountHandler countHandler = new CountHandler();
        map.put(countHandler.methodName(), countHandler);
    }
}
