package com.boot.dataObject;


import lombok.Data;

import java.util.List;

@Data
public class WordConfig {
    private Integer id;
    private String text;
    private String audTrm;
    private String prvdId;
    private String textTemplate; //模板内容
    private Integer fontSize;
    private Integer parentId;//父id
    private String titleContent; //标题
    private String querySql; //sql
    private String countSql; //sql   0不显示 1显示
    private Integer titleLevel;   //标题级别
    private Integer isTable;   //是不是表格
    private Integer indentation; //首行缩进
    private String paragraphAlignment;
    private String fontFamily;
    private Integer blockCode;
    private Integer bold;
    private Integer splicing;
    private Integer subjectId;
    private String version;
    private String wordName;
    private List<WordConfig> subClass;//子


}
