package com.boot.dataObject;


import com.boot.dataObject.abs.TabAbs;
import lombok.Data;

@Data
public class TabMybits  extends TabAbs {
    private Integer id;
    private String msg;
    private String remark;
}
