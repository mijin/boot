package com.boot.repository;

import com.boot.dataObject.TabMybits;
import com.boot.dataObject.abs.TabAbs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TabMybitsRpository {
    TabMybits save(TabMybits model);

    TabMybits delete(Integer id);

    TabMybits findById(Integer id);

    List<TabMybits> findAll();



    List<? extends TabAbs> findlist();

    @Select("select * from tab_mybits t where  t.msg = #{msg}")
    TabMybits findTabMybits(@Param("msg") String msg);
}
