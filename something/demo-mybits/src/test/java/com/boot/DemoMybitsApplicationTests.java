package com.boot;

import com.boot.dataObject.TabMybits;
import com.boot.framework.MapperRepository;
import com.boot.service.TabMybitsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
//@MapperScan(basePackages = {"com.boot.repository"})
public class DemoMybitsApplicationTests {
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Test
    public void contextLoads() {
        String tabName = " sum_khqf_4004_cty_vt";
        String cols = "gen_date,\n" +
                "\t aud_trm,\n" +
                "\tprvd_id,\n" +
                "\tprvd_nm,\n" +
                "\tcity_id,\n" +
                "\tcity_nm,\n" +
                "\tsum_infrac_cust_cnt,\n" +
                "\tsum_infrac_acct_cnt,\n" +
                "\tsum_infrac_amt,\n" +
                "\trank_cty";

        cols = cols.replace("\n", "");
        StringBuilder sql = new StringBuilder("\nCREATE TABLE `").append(tabName.trim().toLowerCase()).append("` ( \n");
        String[] split = cols.split(",");
        int index = 0;
        for (String clo : split) {
            index++;
            if (index < split.length) {
                sql.append("  ").append(clo.trim().toLowerCase()).append("  varchar(225),\n");
            } else {
                sql.append("  ").append(clo.trim().toLowerCase()).append("  varchar(225) \n )");
            }
        }

        IntStream intStream = Stream.of(cols.split(",")).mapToInt(e -> 1);
        int sum = intStream.sum();


        log.info(sql.toString());
        int update = 0;
        try {
            update = jdbcTemplate.update(sql.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jdbcTemplate.update("INSERT INTO `creat_table_info` (`sql_content`, `table_name`, `create_date`,`remark`) VALUES (?, ?, ?, ?)", sql, tabName, new Date(), "khqf");
        }
    }

    @Autowired
    TabMybitsService tabMybitsService;
    @Autowired
    @Qualifier("tab_mybits")
    MapperRepository mapperRepository;

    @Test
    public void test() {
        for (int index = 0; index < 10; index++) {
            TabMybits modeo = new TabMybits();
            modeo.setId(index);

            modeo.setMsg(index + "==");
            modeo.setRemark(index + "---");
            tabMybitsService.save(modeo);

            log.info("mapperRepository.count()={}",mapperRepository.count());
        }
    }

}
