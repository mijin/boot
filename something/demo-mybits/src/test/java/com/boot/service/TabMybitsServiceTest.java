package com.boot.service;

import com.boot.DemoMybitsApplicationTests;
import com.boot.dataObject.TabMybits;
import com.boot.repository.TabMybitsRpository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
@Component
public class TabMybitsServiceTest  extends DemoMybitsApplicationTests {
    String colName = "pers";
    String tabName = " sum_dzq_prvdcty_det_vt";
    String cols = "cmccerr_d_num                 \n" +
            "cmccerr_c_amt_per             \n" +
            "cmccerr_c_num                 \n" +
            "cmccerr_c_amts                \n" +
            "cmccerr_b_num                 \n" +
            "cmccerr_b_amt                 \n" +
            "cmccerr_a_amt_pers            \n" +
            "cmccerr_a_amts                \n" +
            "cmccerr_a_amt                 \n" +
            "cmcc_err_num_per              \n" +
            "cmcc_err_subs                 \n" +
            "cmcc_err_amts                 \n" +
            "cmcc_sum_subs                 \n" +
            "cmcc_sum_num                  \n" +
            "cmcc_sum_amt                  \n" +
            "cmcc_prov_prvd_nm             \n" +
            "aud_trm                       \n" +
            "rank_amt                      \n" +
            "rank_old                      \n" +
            "rank_new                      \n" +
            "cmcc_prov_prvd_id             \n" +
            "cmcc_amt                      \n" +
            "cmccerr_c_amt                 \n" +
            "cmcc_err_amt_per              \n" +
            "cmccerr_a_num                 \n" +
            "cmccerr_b_amt_pers            \n" +
            "cmccerr_d_amt                 \n" +
            "cmccerr_d_amt_per             \n" +
            "cmcc_err_amt                  \n" +
            "cmccerr_b_amts                \n" +
            "cmccerr_a_amt_per             \n" +
            "cmcc_err_num                  \n" +
            "cmccerr_b_amt_per             \n" +
            "cmccerr_d_amts                \n" +
            "cmccerr_d_amt_pers            \n" +
            "cmccerr_c_amt_pers            ";
    @Test
     public void contextLoads1() {
        if (!cols.contains("\n,")) {
            cols = cols.replace("\n","\n,");
        }
     }


}