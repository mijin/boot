package com.boot.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import com.boot.DemoMybitsApplicationTests;
import com.boot.dataObject.WordConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.junit.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;
import org.slf4j.Logger;
import org.springframework.expression.*;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

@Slf4j
public class WordTest extends DemoMybitsApplicationTests {

    /**
     * 创建文件夹
     */
    @Test
    public void test1() {
        File file = new File("D:\\offer\\create_table.docx");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdir();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void write2Docx() throws Exception {
        XWPFDocument document = new XWPFDocument();

        //Write the Document in file system
        FileOutputStream out = new FileOutputStream(new File("D:\\offer\\create_table.docx"));

        //添加标题
        XWPFParagraph titleParagraph = document.createParagraph();
        //设置段落居中
        titleParagraph.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun titleParagraphRun = titleParagraph.createRun();

        titleParagraphRun.setText("Java PoI");
        titleParagraphRun.setColor("000000");
        titleParagraphRun.setFontSize(20);

        //段落
        XWPFParagraph firstParagraph = document.createParagraph();
        XWPFRun run = firstParagraph.createRun();
        run.setText("Java POI 生成word文件。");
        run.setColor("696969");
        run.setFontSize(16);

        //设置段落背景颜色
        CTShd cTShd = run.getCTR().addNewRPr().addNewShd();
        cTShd.setVal(STShd.CLEAR);
        cTShd.setFill("97FFFF");

        //换行
        XWPFParagraph paragraph1 = document.createParagraph();
        XWPFRun paragraphRun1 = paragraph1.createRun();
        paragraphRun1.setText("\r");

        //基本信息表格
        XWPFTable infoTable = document.createTable();
        //去表格边框
        infoTable.getCTTbl().getTblPr().unsetTblBorders();

        //列宽自动分割
        CTTblWidth infoTableWidth = infoTable.getCTTbl().addNewTblPr().addNewTblW();
        infoTableWidth.setType(STTblWidth.DXA);
        infoTableWidth.setW(BigInteger.valueOf(9072));

        //表格第一行
        XWPFTableRow infoTableRowOne = infoTable.getRow(0);
        infoTableRowOne.getCell(0).setText("职位");
        infoTableRowOne.addNewTableCell().setText(": Java 开发工程师");

        //表格第二行
        XWPFTableRow infoTableRowTwo = infoTable.createRow();
        infoTableRowTwo.getCell(0).setText("姓名");
        infoTableRowTwo.getCell(1).setText(": seawater");

        //表格第三行
        XWPFTableRow infoTableRowThree = infoTable.createRow();
        infoTableRowThree.getCell(0).setText("生日");
        infoTableRowThree.getCell(1).setText(": xxx-xx-xx");

        //表格第四行
        XWPFTableRow infoTableRowFour = infoTable.createRow();
        infoTableRowFour.getCell(0).setText("性别");
        infoTableRowFour.getCell(1).setText(": 男");

        //表格第五行
        XWPFTableRow infoTableRowFive = infoTable.createRow();
        infoTableRowFive.getCell(0).setText("现居地");
        infoTableRowFive.getCell(1).setText(": xx");
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(document, sectPr);

        //添加页眉
        CTP ctpHeader = CTP.Factory.newInstance();
        CTR ctrHeader = ctpHeader.addNewR();
        CTText ctHeader = ctrHeader.addNewT();
        String headerText = "ctpHeader";
        ctHeader.setStringValue(headerText);
        XWPFParagraph headerParagraph = new XWPFParagraph(ctpHeader, document);
        //设置为右对齐
        headerParagraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFParagraph[] parsHeader = new XWPFParagraph[1];
        parsHeader[0] = headerParagraph;
        policy.createHeader(XWPFHeaderFooterPolicy.DEFAULT, parsHeader);

        //添加页脚
        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();
        String footerText = "ctpFooter";
        ctFooter.setStringValue(footerText);
        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        headerParagraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        policy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

        document.write(out);
        out.close();
    }

    @Test
    public void test2() throws Exception {
        String fileName = "D:\\offer\\客户信息安全201902.docx";
        File file = new File(fileName);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdir();
        }
        XWPFDocument document = new XWPFDocument();
        FileOutputStream out = new FileOutputStream(file);
        //添加标题
        XWPFParagraph titleParagraph = document.createParagraph();
        //设置段落居中
        ParagraphAlignment center = ParagraphAlignment.valueOf("CENTER");
        titleParagraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun titleParagraphRun = titleParagraph.createRun();
        titleParagraphRun.setText("客户信息安全201902");
        titleParagraphRun.setFontSize(20);

        //段落
        List<WordConfig> list = new ArrayList<>();
        for (int index = 1; index < 10; index++) {

        }


        for (WordConfig config : list) {
            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();
            if (config.getText().contains("1")) {
                paragraph.setIndentationFirstLine(600);
            }
            run.setText(config.getText());
            run.setFontSize(config.getFontSize());
        }


        document.write(out);
        out.close();
    }


    @Test
    public void test3() throws Exception {
        String audTrm = "201901";
        String prvdId = "10010";
        String subjectId = "14";

        String fileName = spel("D:\\offer\\客户信息{#prvdId}{#audTrm}.docx", new HashMap<String, Object>() {{
            put("audTrm",audTrm);
            put("prvdId",prvdId);
        }});

        File file = new File(fileName);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdir();
        }

        String sql = "SELECT * from word_config WHERE aud_trm = ? AND prvd_id = ? AND subject_id = ? AND parent_code = 0";
        List<WordConfig> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(WordConfig.class),audTrm,prvdId,subjectId);
        Titles thisTitle = new Titles();
        List<WordConfig> rlist = new LinkedList<>();
        for (WordConfig config : list) {
            if (jdbcTemplate.queryForObject(config.getCountSql(), Integer.class).compareTo(0) != 0) { //确定要显示
                String text = spel(config.getTextTemplate(), jdbcTemplate.queryForList(config.getQuerySql()));
//                如果是标题是
                if (config.getTitleLevel().compareTo(1) == 0) {
                    config.setText(String.format("%s、%s",thisTitle.one(),text ));
                    rlist.add(config);
                } else if (config.getTitleLevel().compareTo(2) == 0) {
                    config.setText(String.format("(%s) %s", thisTitle.two(),text));
                    rlist.add(config);
                } else {
                    if (config.getSplicing().compareTo(1)==0) {
                        WordConfig wordConfig = rlist.get(rlist.size()-1);
                        wordConfig.setText(wordConfig.getText()+text);
                    }else {
                        config.setText(text);
                        rlist.add(config);
                    }
                }
            }
        }

        XWPFDocument document = new XWPFDocument();
        for (WordConfig config : rlist) {
            XWPFParagraph paragraph = document.createParagraph();
            paragraph.setAlignment(ParagraphAlignment.valueOf(config.getParagraphAlignment().toUpperCase()));
            paragraph.setIndentationFirstLine(config.getIndentation()); //缩进
            XWPFRun run = paragraph.createRun();
            run.setBold(config.getBold().equals(1));
            run.setText(config.getText());
            run.setFontSize(config.getFontSize());
            run.setFontFamily(config.getFontFamily());
        }

        FileOutputStream out = new FileOutputStream(file);
        document.write(out);
        out.close();
    }


    @Test
    public void test4() throws Exception {
        String v = spel("asds{#a}",  new HashMap<String, Object>() {{
            put("v", "1");
            put("a", "1");
        }});
        log.info(v);
    }

    public   String spel(String template,  Map<String, Object>   data) {
        return this.spel(template,Arrays.asList(data));
    }
    //    splange
    public   String spel(String template, List<Map<String, Object> > list) {
        StringBuilder sb = new StringBuilder();
        ExpressionParser parser = new SpelExpressionParser();
        ParserContext parserContext = new ParserContext() {
            @Override
            public boolean isTemplate() {
                return true;
            }
            @Override
            public String getExpressionPrefix() {
                return "{";
            }
            @Override
            public String getExpressionSuffix() {
                return "}";
            }
        };
        Expression expression = parser.parseExpression(template, parserContext);
        for (Map<String, Object> map : list) {
            StandardEvaluationContext ctx = new StandardEvaluationContext();
            ctx.setVariables(map);
            sb.append(expression.getValue(ctx, String.class));
        }
        return sb.toString();
    }

    public static class Titles {
        private   ThreadLocal<Titles> local = new ThreadLocal<>();
        private static final   List<String> list = Arrays.asList("一", "二", "三", "四", "五", "六", "七", "八", "九", "十");
        Map<String, Boolean> map1 = new LinkedHashMap<>(); //标题1
        Map<String, Boolean> map2 = new LinkedHashMap<>(); //标题2
        Map<String, Boolean> map3 = new LinkedHashMap<>(); //标题3

        public Titles() {
            for (String title : list) {
                map1.put(title, true);
                map2.put(title, true);
                map3.put(title, true);
            }
            local.set(this);
        }


        public String one() {
            String val = "";
            Titles titles = local.get();
            for (Map.Entry<String, Boolean> entry : titles.map1.entrySet()) {
                if (entry.getValue()) {
                  val = entry.getKey();
                  break;
                }
            }
            titles.map1.put(val,false);
            for (String item : list) { //clear
                titles.map2.put(item,true);
                titles.map3.put(item,true);
            }
            return val;
        }

        public String two() {
            String val = "";
            Titles titles = local.get();
            for (Map.Entry<String, Boolean> entry : titles.map2.entrySet()) {
                if (entry.getValue()) {
                    val = entry.getKey();
                    break;
                }
            }
            titles.map2.put(val,false);
            for (String item : list) { //clear
                titles.map3.put(item,true);
            }
            return val;
        }
    }
}
