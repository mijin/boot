package com.boot.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;

@Slf4j
@Component
public class Task {

    public static Random random = new Random();

    @Async("selfpool")
    public void doTaskOne() throws Exception {
        log.info("开始做任务一");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("完成任务一，耗时：{}", (end - start));
    }

    @Async("selfpool")
    public void doTaskTwo() throws Exception {
        log.info("开始做任务二");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("完成任务二，耗时：{}", (end - start));
    }

    @Async("selfpool")
    public void doTaskThree() throws Exception {
        log.info("开始做任务三");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        log.info("完成任务三，耗时：{}", (end - start));
    }

    @Async("selfpool")
    public void nextdate() throws Exception {
        log.info("开始获取时间");
        long start = System.currentTimeMillis();
        Date nextDate = Task.getNextDate();
        long end = System.currentTimeMillis();
        log.info("完成任务四，获取到时间：{}",nextDate);
        log.info("完成任务四，耗时：{}", (end - start));
    }

    public static Date getNextDate() {
        try {
            Thread.sleep(24 * 60 * 60 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Date();
    }


}
