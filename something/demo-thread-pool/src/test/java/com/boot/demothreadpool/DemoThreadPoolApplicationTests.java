package com.boot.demothreadpool;

import com.boot.task.Task;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringRunner;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoThreadPoolApplicationTests {

    @Autowired
    private Task task;

    @Test
    public void test() throws Exception {

        task.doTaskOne();
        task.doTaskTwo();
        task.doTaskThree();

        Thread.currentThread().join();
        //在多线程编程中,Thread.CurrentThread 表示获取当前正在运行的线程，join方法是阻塞当前调用线程，直到某线程完全执行才调用线程才继续执行，如果获取的当前线程是主线程，调用Join方法，会是怎样的呢
        //阻塞住，因为线程无法结束
    }
    @Test
    public void nextDate() throws Exception {

        log.info("cc:{}","sss");
       /* task.nextdate();

        Thread.currentThread().join();*/
    }

}
