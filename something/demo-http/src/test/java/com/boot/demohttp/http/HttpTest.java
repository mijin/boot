package com.boot.demohttp.http;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

@Slf4j
public class HttpTest {
    RestTemplate restTemplate = new RestTemplate();

    @Test
    public void http1(){
        String url = "http://localhost:8081/springmvc/car?count=";

        for (int index = 0; index < 1000; index++) {
            ResponseEntity<String> forEntity = restTemplate.getForEntity(url+index, String.class);
            log.info("index:{},code:{}",  index, forEntity.getStatusCode());
        }
    }

    @Test
    public void testpool(){
        String url = "http://localhost:8081/springmvc/car?count=";

        // 创建一个线程池对象，控制要创建几个线程对象。
        ExecutorService pool = Executors.newFixedThreadPool(20);

        // 可以执行Runnable对象或者Callable对象代表的线程

        for (int index = 0; index < 100; index++) {
            Future<?> submit = pool.submit(new MyRunnable(url+index));
            try {
                Object o = submit.get();
                log.info(o+"");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
        //结束线程池
        pool.shutdown();
    }

    @Test
    public void test()  {
        String url = "http://localhost:8081/springmvc/car?count=";
        String s = doGet(url);
        log.info(s);
    }

    public   String doGet(String url)  {
        String result = "";
        URL realUrl = null;
        try {
            realUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // 打开和URL之间的连接
        URLConnection connection = null;
        try {
            connection = realUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            connection.connect();
            Map<String, List<String>> headerFields = connection.getHeaderFields();
            for (Map.Entry<String, List<String>> entry : headerFields.entrySet()) {
                log.info("k  :  {},v  :  {}",entry.getKey(),entry.getValue());
                if (entry.getKey() == null) {
                    return entry.getValue().get(0);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        try {
//            in = new BufferedReader(new InputStreamReader(
//                    connection.getInputStream()
//            ));
//            String rline = null;
//            while ((rline=in.readLine())!=null){
//                result+=rline;
//            }
//            return result;
//        }catch (Exception e){
//
//        }finally {
//            try {
//                in.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        return result;
    }



        class MyRunnable implements Runnable {
        private String url;

            public MyRunnable(String url) {
                this.url = url;
            }

            @Override
        public void run() {
                String s = doGet(url);
                log.info(url);
            }
    }
}
