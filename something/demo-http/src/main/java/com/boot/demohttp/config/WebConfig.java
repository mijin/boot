package com.boot.demohttp.config;

import com.boot.demohttp.compont.ForWardCompont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    ForWardCompont forWardCompont;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(forWardCompont).addPathPatterns("/**");
    }
}
