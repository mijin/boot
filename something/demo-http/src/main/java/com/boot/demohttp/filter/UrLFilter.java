package com.boot.demohttp.filter;


import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@Slf4j
@WebFilter(filterName = "a", urlPatterns = "/**")
public class UrLFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info(request.getLocalAddr());
        filterChain.doFilter(request, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
