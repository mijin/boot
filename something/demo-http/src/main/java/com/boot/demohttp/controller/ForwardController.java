package com.boot.demohttp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

@org.springframework.web.bind.annotation.RestController
public class ForwardController {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/aa")
    public Object forward(){
        return "11";
    }
}
