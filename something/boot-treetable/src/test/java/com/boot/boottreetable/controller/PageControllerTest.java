package com.boot.boottreetable.controller;

import org.junit.Test;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.sun.deploy.cache.Cache.exists;


public class PageControllerTest {

    @Test
    public void  test(){
        SimpleDateFormat format = new SimpleDateFormat("//yyyy//MM//dd//");
        String format1 = format.format(new Date());
        downloadPicture("https://apic.douyucdn.cn/upload/avanew/face/201802/07/16/6e8e3a5d6a55c2109ae03a6ddf7fd729_big.jpg","F:\\"+format1,"shenghuo.jpg");
    }

    //链接url下载图片
    private   void downloadPicture(String u,String path,String name) {
        URL url = null;
        int imageNumber = 0;

        try {
            url = new URL(u);
            DataInputStream dataInputStream = new DataInputStream(url.openStream());
            File file = new File(path);
            if(!file.exists()){
                file.mkdirs();
            }

            String imageName =  path+name;

            FileOutputStream fileOutputStream = new FileOutputStream(new File(imageName));
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int length;

            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            byte[] context=output.toByteArray();
            fileOutputStream.write(output.toByteArray());
            dataInputStream.close();
            fileOutputStream.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}