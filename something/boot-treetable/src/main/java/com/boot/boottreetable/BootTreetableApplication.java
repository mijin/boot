package com.boot.boottreetable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootTreetableApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootTreetableApplication.class, args);
	}
}
