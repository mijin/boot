package com.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SecurityController {
    @RequestMapping("/login")
    public String userLogin() {
        return "login";
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/login-error")
    public String loginError() {
        return "loginError";
    }


    @RequestMapping("/hello")
    @ResponseBody
    public String hello() {
        return "spring security hello world";
    }
}
