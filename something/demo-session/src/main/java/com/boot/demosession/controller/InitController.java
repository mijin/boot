package com.boot.demosession.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class InitController {
    /**
     * 登录接口
     * @param request
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public Object login (HttpServletRequest request){
        request.getSession().setAttribute("sessionId", request.getRequestURL());
        return request.getSession().getId();
    }

    /**
     * 获取session消息
     * @param request
     * @return
     */
    @RequestMapping(value = "/session", method = RequestMethod.GET)
    public Object session (HttpServletRequest request){
         return request.getSession().getId();
    }
}
