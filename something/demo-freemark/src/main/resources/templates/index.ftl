<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="/static/simple.css">
</head>
<body>
<span class="simple">${msg?if_exists }</span>

</body>
<script src="/static/simple.js"></script>
</html>