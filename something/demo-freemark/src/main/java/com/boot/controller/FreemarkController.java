package com.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FreemarkController {

    @RequestMapping("/hello")
    public String hello(Model model, HttpServletRequest request){
        model.addAttribute("msg","Freemark");
        return "index";
    }

}
