package com.code.fileserver.controller;

import org.springframework.boot.SpringBootVersion;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VsersionController {
    @RequestMapping("/v")
    public Object v(){
        return SpringBootVersion.class.getPackage();
    }
}
