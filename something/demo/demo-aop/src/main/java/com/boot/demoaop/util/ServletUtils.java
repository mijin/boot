package com.boot.demoaop.util;


import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 客户端工具类
 *
 * @author ruoyi
 */
public class ServletUtils
{

    /**
     * @return
     * 页码
     */
    public static Integer getPageNum()
    {
        Integer pageNum = getParameterToInt("pageNum");
        return  pageNum.equals(0)?1:pageNum;
    }

    /**
     * @return
     * 条数
     */
    public static Integer getPageSize()
    {
        Integer pageSize = getParameterToInt("pageSize");
        return pageSize.equals(0)?10000: pageSize;
    }


    /**
     * @return
     * 获取oderby字段
     */
    public static String getOrderBy()
    {
        String orderBy = getParameter("orderBy");
        if (StringUtils.isEmpty(orderBy)) {
            return  orderBy;
        }
        return "1";
    }
    /**
     * 获取String参数
     */
    public static String getParameter(String name)
    {
        return  getRequest().getParameter(name);
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name)
    {
        String parameter = getRequest().getParameter(name);
        if (StringUtils.isEmpty(parameter)) {
            return   Integer.valueOf(getRequest().getParameter(name));
        }
        return 0;
    }


    /**
     * 获取request
     */
    public static HttpServletRequest getRequest()
    {
        return getRequestAttributes().getRequest();
    }


    /**
     * 获取session
     */
    public static HttpSession getSession()
    {
        return getRequest().getSession();
    }


    /**
     * 获取到session所有的值
     * @return
     */
    public  static Map<String,Object> getSessionMap(){
        Map<String,Object> map = new HashMap<>();
        Enumeration<String> attributeNames = getSession().getAttributeNames();
        while (attributeNames.hasMoreElements()){
            String key = attributeNames.nextElement();
            map.put(key,getSession().getAttribute(key));
        }
        return map;
    }


    /**
     * session 是否有这个key,val不为null
     * @param key
     * @return
     */
    public static boolean sessionHasKey(String key){
        return getSession().getAttribute(key)==null?false:true;
    }

    /**
     * 从session里面获取值
     * @param key
     * @param <T>
     * @return
     */
    public static <T>  T sessionAttr(String key){
        if (sessionHasKey(key)) {
            return (T) getSession().getAttribute(key);
        }
        return null;
    }

    public static ServletRequestAttributes getRequestAttributes()
    {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    /**
     * 将字符串渲染到客户端
     *
     * @param response 渲染对象
     * @param string 待渲染的字符串
     * @return null
     */
    public static String renderString(HttpServletResponse response, String string)
    {
        try
        {
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(string);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }


}
