package com.boot.demoaop.controller;

import com.boot.demoaop.util.ServletUtils;
import lombok.extern.java.Log;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Objects;

@Log
@RestController
public class HelloController {

    private String str;

    public void setStr(String str) {
        this.str = str;
    }

    @RequestMapping("/index")
    public Object index(){
        log.info("index开始执行");
        HttpSession session = ServletUtils.getSession();
        session.setAttribute("date",new Date());
        return SpringVersion.class.getPackage();
    }

    @RequestMapping("/session")
    public Object session(){
        log.info("session开始执行");
        return ServletUtils.getSessionMap();
    }

    @RequestMapping("/exption")
    public Object exption(){
        log.info("exption开始执行");
        if (true) {
            Objects.requireNonNull(null);
        }
        return ServletUtils.getSessionMap();
    }

    @RequestMapping("/date")
    public Object date(){
        Date date = ServletUtils.sessionAttr("date");
        return date;
    }

    @RequestMapping("/str")
    public Object str(){
        return str;
    }



}
