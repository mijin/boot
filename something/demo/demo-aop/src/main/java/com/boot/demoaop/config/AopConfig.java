package com.boot.demoaop.config;

import com.boot.demoaop.util.ServletUtils;
import lombok.extern.java.Log;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;


/**
 * around>before>method>around>after>afterReturning
 */
@Log
@Aspect
@Component
public class AopConfig {

    /**
     * 定义好切点
     */
    @Pointcut("execution( * com.boot.demoaop.controller.*.*(..))")
    public void pointCutAt() {
    }

    /**
     * 环绕切，最强
     * @param pjp
     * @return
     */
    @Around("pointCutAt()")
    public Object around(ProceedingJoinPoint pjp){
        Object proceed = null;
        log.info("环绕aop----------start---------"+ServletUtils.getRequest().getRequestURI());
        try {
              proceed = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        log.info("环绕aop----------end---------"+ServletUtils.getRequest().getRequestURI());
        return proceed;
    }


    /**
     * 在环绕切点执行后，method.invod 执行前
     * @param joinPoint
     */
    @Before("pointCutAt()")
    public void before(JoinPoint joinPoint){
       log.info("before======================");
    }

    /**
     * method-》around=》after
     */
    @After("pointCutAt()")
    public void after( ){
        log.info("方法之后执行...after.");
    }

    /**
     * around>before>method>around>after>afterReturning
     */
    @AfterReturning("pointCutAt()")
    public void afterReturning(){
        log.info("afterReturning--------------------------");
    }

    /**
     * 处理异常
     * 不顶用的切法
     */
    @AfterThrowing(value = "pointCutAt()",throwing = "ex")
    public void afterThrow(Throwable ex){
        log.info("afterThrow--------------------------");
    }

}
