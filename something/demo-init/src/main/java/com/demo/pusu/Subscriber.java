package com.demo.pusu;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.JedisPubSub;

@Slf4j
public class Subscriber extends JedisPubSub {
    @Override
    public void onMessage(String channel, String message) {
        log.info("监听到管道：{}--的信息：{}",channel,message);
        super.onMessage(channel, message);
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println(String.format("onSubscribe redis channel success, channel %s, subscribedChannels %d",
                channel, subscribedChannels));
        super.onSubscribe(channel, subscribedChannels);
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        System.out.println(String.format("onUnsubscribe redis channel, channel %s, subscribedChannels %d",
                channel, subscribedChannels));
        super.onUnsubscribe(channel, subscribedChannels);
    }
}
