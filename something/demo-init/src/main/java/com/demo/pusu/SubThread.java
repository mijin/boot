package com.demo.pusu;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class SubThread extends Thread {
    private Jedis jedis;
    private JedisPubSub jedisPubSub;
    private String channel;


    public SubThread(Jedis jedis, JedisPubSub jedisPubSub, String channel) {
        this.jedis = jedis;
        this.channel = channel;
        this.jedisPubSub = jedisPubSub;
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
         try {
            jedis.subscribe(jedisPubSub, channel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
