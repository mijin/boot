package com.demo.pusu.impl;

import com.demo.pusu.PubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class PubServiceImpl implements PubService {
    @Autowired
    private Jedis jedis;

    @Override
    public Long pub(String channel, String message) {
        return jedis.publish(channel, message);
    }
}
