package com.demo.controller;

//import com.tjh.starter.model.HelloService;

import com.demo.model.SModulesBean;
import com.demo.model.TestModel;
import com.demo.pusu.PubService;
import com.demo.service.BookingCreatedEvent;
import com.demo.util.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class HelloController {

    private final static Map<Date, Object> map = new HashMap<>();

    @Autowired
    PubService pubService;

    @PostMapping("/pp")
    public Object post(@RequestBody List<SModulesBean> sModules) {
        sModules.get(0);
        return SpringVersion.class.getPackage();
    }

    @GetMapping("/event")
    public Object event() {
        BookingCreatedEvent event = new BookingCreatedEvent(this, new SModulesBean("id", System.currentTimeMillis() + "", "name"));
        SpringUtils.publishEvent(event);
        return SpringVersion.class.getPackage();
    }

    @GetMapping("/redisChannel")
    public Object redisChannel(HttpSession session) {
       map.put(new Date(),session.getId());
        return pubService.pub("GenFile", map.toString());
    }


    @PostMapping("/pp2")
    public Object pp2(@RequestBody Map<String, Object> sModules) {
        String name = sModules.size() + "";
        return sModules;
    }

    @GetMapping("/thread")
    public Object thread() {
        Map<String, Object> data = new HashMap<>();
        map.put(new Date(), data);
        data.put(Thread.currentThread().getName(), Thread.currentThread().getId());
        return map;
    }

    @GetMapping("/getThreadGroup")
    public Object getThreadGroup() {
        ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();

        //此线程组中活动线程的估计数
        int noThreads = threadGroup.activeCount();

        Thread[] lstThreads = new Thread[noThreads];
        //把对此线程组中的所有活动子组的引用复制到指定数组中。
        threadGroup.enumerate(lstThreads);

        List<Map<String, Object>> list = new ArrayList<>();
        for (Thread thread : lstThreads) {
            Map<String, Object> data = new HashMap<>();
            data.put("线程数量", lstThreads.length);
            data.put("线程id", thread.getId());
            data.put("线程名称", thread.getName());
            data.put("线程状态", thread.getState().toString());
            list.add(data);
        }

        list = list.stream().filter(e -> e.get("线程状态").equals("RUNNABLE")).collect(Collectors.toList());

        return list;
    }
}


