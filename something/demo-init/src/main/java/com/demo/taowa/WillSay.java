package com.demo.taowa;


import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

public interface WillSay {

     Logger log = LoggerFactory.getLogger(WillSay.class.getName());


    void say();

}
