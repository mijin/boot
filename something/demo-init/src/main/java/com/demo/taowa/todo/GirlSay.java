package com.demo.taowa.todo;

import com.demo.taowa.WillSay;
import com.demo.taowa.WillSayDec;

public class GirlSay extends WillSayDec {
    public GirlSay(WillSay willSay) {
        super(willSay);
    }


    @Override
    public void say() {
        super.willSay.say();
        log.info( "妹子:的呼唤");
        call();
    }


     public void call() {
        log.info( "妹子:的回调");
    }
}
