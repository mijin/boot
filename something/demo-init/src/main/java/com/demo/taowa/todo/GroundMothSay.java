package com.demo.taowa.todo;

import com.demo.taowa.WillSay;
import com.demo.taowa.WillSayDec;

public class GroundMothSay extends WillSayDec {
    public GroundMothSay(WillSay willSay) {
        super(willSay);
    }


    @Override
    public void say() {
        super.willSay.say();
        log.info( "奶奶:的呼唤");
        call();
    }


     public void call() {
        log.info( "奶奶:的回调");
    }
}
