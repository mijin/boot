package com.demo.taowa;

/**
 * 动作抽象
 */
public abstract class WillSayDec implements WillSay {
    protected WillSay willSay;

    public WillSayDec(WillSay willSay) {
        this.willSay = willSay;
    }

    @Override
    public void say() {
        willSay.say();
    }
}
