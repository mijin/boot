package com.demo.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class IUserDo {
    private int id;
    private String name;
    private String password;
    private String remark;
    private int otherName;
    private String date;

}
