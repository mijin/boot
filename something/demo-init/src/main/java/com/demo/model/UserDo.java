package com.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@AllArgsConstructor
@Data
public class UserDo {
    private int id;
    private String name;
    private String password;
    private String remark;
    private Date date;

}
