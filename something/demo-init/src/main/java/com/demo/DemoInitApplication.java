package com.demo;

import com.demo.controller.HelloController;
import com.demo.pusu.SubThread;
import com.demo.pusu.Subscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.util.concurrent.Executor;

@Slf4j
@SpringBootApplication
public class DemoInitApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoInitApplication.class, args);
    }

    @Autowired
    @Qualifier("taskExecutor")
    Executor executor;


    @Bean
    public Jedis jedis() {
        String redisIp = "101.201.235.27";
        int reidsPort = 39001;
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), redisIp, reidsPort, 3000, "root", 0);

//        配置redis监听
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Jedis jedis = jedisPool.getResource();
                jedis.subscribe(new JedisPubSub(){
                    @Override
                    public void onMessage(String channel, String message) {
                        log.info("监听到管道：{}--的信息：{}",channel,message);
                        super.onMessage(channel, message);
                    }
                }, "GenFile");
            }
        });


        return jedisPool.getResource();
    }


}
