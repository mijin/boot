package com.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class BookingCreatedEventLister implements ApplicationListener<BookingCreatedEvent> {
    final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onApplicationEvent(BookingCreatedEvent bookingCreatedEvent) {
        log.info("data {}:", bookingCreatedEvent.getsModulesBean() );
    }
}
