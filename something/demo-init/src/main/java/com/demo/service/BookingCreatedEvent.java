package com.demo.service;

import com.demo.model.SModulesBean;
import org.springframework.context.ApplicationEvent;

public class BookingCreatedEvent extends ApplicationEvent {
   private SModulesBean sModulesBean;
     public BookingCreatedEvent(Object source) {
        super(source);
     }
    public BookingCreatedEvent(Object source,SModulesBean sModulesBean) {
        this(source);
        this.sModulesBean = sModulesBean;
    }

    public SModulesBean getsModulesBean() {
        return sModulesBean;
    }




}
