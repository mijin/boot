package com.demo.concover;

import com.demo.model.IUserDo;
import com.demo.model.UserDo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.Map;


@Mapper
public interface UserConcoverIUser {
    UserConcoverIUser INSTANCE = Mappers.getMapper(UserConcoverIUser.class);

    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "password",target = "password"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "password",target = "otherName"  ),
            @Mapping(source = "date",target = "date" ,dateFormat = "yyyy-MM-DD")
    })
    IUserDo concover(UserDo userDo);




}
