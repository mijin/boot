package com.demo;

import com.demo.concover.UserConcoverIUser;
import com.demo.model.IUserDo;
import com.demo.model.UserDo;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Mapstruct extends Call {

    @Test
    @Override
    public void test1() {

        UserDo userDo = new UserDo(1, "name", "123456", "mark",new Date());
        IUserDo concover = UserConcoverIUser.INSTANCE.concover(userDo);
        log.info(concover.toString());
    }
    @Test
    @Override
    public void test2() {
        Map<String,Object> source = new HashMap<String,Object>(){{
            put("nows", LocalDateTime.now());
        }};

//        Map<String, Object> andPut = UserConcoverIUser.INSTANCE.getAndPut(source);
//        log.info(andPut.toString());
    }
}
