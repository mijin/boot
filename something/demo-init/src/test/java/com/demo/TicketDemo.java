package com.demo;

import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class TicketDemo {

    protected final static org.slf4j.Logger log = LoggerFactory.getLogger(TicketDemo.class.getName());


    @Test
    public void tTets() throws InterruptedException {
        long start = System.currentTimeMillis();

        CompletableFuture<Integer> completableFuture  = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                log.info("开始执行....");
                timceSleep();
                return 100;
            }
        });
        completableFuture.whenComplete(new BiConsumer<Integer, Throwable>() {
            @Override
            public void accept(Integer integer, Throwable throwable) {
                log.info("执行结果..."+integer);
            }
        });


//       主线程耗时约....
        log.info("main耗时"  +  (System.currentTimeMillis()-start));
        new CountDownLatch(1).await();

    }

    static void timceSleep(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
