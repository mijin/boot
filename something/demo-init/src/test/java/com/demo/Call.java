package com.demo;

import com.demo.anno.Api;
import com.demo.anno.Apis;
import com.demo.pusu.Publisher;
import com.demo.pusu.SubThread;
import com.demo.taowa.impl.EatSay;
import com.demo.taowa.todo.GirlSay;
import com.demo.taowa.todo.GroundMothSay;
import com.demo.taowa.todo.WomanSay;
import org.junit.Test;
import org.springframework.core.annotation.AnnotationUtils;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class Call extends TicketDemo {

    @Apis({@Api("treadata"), @Api("mysql"), @Api("oracle"),@Api})
    public void api() {
        log.info(LocalDateTime.now().toString());
    }

    @Test
    public void test2() throws NoSuchMethodException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, InstantiationException {

        Class<?> clz = Class.forName("com.demo.Call");
        Object instance = clz.newInstance();
        Method method = instance.getClass().getMethod("api");
        Object invoke = method.invoke(instance);
        Apis annotation = AnnotationUtils.getAnnotation(method, Apis.class);
        for (Api api : annotation.value()) {
            log.info(api.value());
        }

    }

    @Test
    public void test1() {
        EatSay eatSay = new EatSay();
        GroundMothSay groundMothSay = new GroundMothSay(eatSay);
        WomanSay womanSay = new WomanSay(groundMothSay);
        GirlSay girlSay = new GirlSay(womanSay);

        girlSay.say();
    }
//    @Test
    public void test3() {
        String redisIp = "101.201.235.27";
        int reidsPort = 39001;
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), redisIp, reidsPort,3000,"root",0);
        System.out.println(String.format("redis pool is starting, redis ip %s, redis port %d", redisIp, reidsPort));

//        SubThread subThread = new SubThread(jedisPool);
//        subThread.start();

        Publisher publisher = new Publisher(jedisPool);
        publisher.start();
    }

    public static void main(String[] args) {
        Call c = new Call();
        c.test3();
    }
}
