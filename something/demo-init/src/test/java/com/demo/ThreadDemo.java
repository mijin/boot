package com.demo;

import org.junit.Test;

import java.util.concurrent.*;
import java.util.logging.Logger;


public class ThreadDemo {
    protected final static Logger log = Logger.getLogger(ThreadDemo.class.getName());

    @Test
    public void demo1() throws ExecutionException, InterruptedException {
//    创建线程池对象
        ExecutorService threadPool = Executors.newFixedThreadPool(4);
//       创建一个实现Callable接口的对象

        Callable<Integer> c1 = new MyCallable<Integer>(10, 20);
        Callable<Integer> c2 = new MyCallable<Integer>(100, 200);

        //获取线程池中的线程，调用call完成操作
        Future<Integer> submit = threadPool.submit(c1);
        Integer res1 = submit.get();

        log.info(res1.toString());

        //获取线程池中的线程，调用call完成操作
        Future<Integer> submit2 = threadPool.submit(c2);
        Integer res2 = submit2.get();
        log.info(res2.toString());

        threadPool.shutdown();

    }


    static class MyCallable<T> implements Callable<Integer>{

        private Integer x;
        private Integer y;
        public MyCallable() {
        }

        public MyCallable(Integer x, Integer y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public Integer call() throws Exception {
            return x+y;
        }
    }
}
