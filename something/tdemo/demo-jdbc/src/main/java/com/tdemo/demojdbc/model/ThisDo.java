package com.tdemo.demojdbc.model;

import lombok.Data;

import java.util.Date;

@Data
public class ThisDo {
    private Integer id;
    private Date date;
}
