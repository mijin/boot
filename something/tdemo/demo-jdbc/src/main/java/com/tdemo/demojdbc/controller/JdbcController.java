package com.tdemo.demojdbc.controller;

import com.tdemo.demojdbc.model.ThisDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JdbcController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/date")
    public Object date(){

        return jdbcTemplate.queryForObject("SELECT 1 id, now() date",new BeanPropertyRowMapper<>(ThisDo.class));
    }

}
