package com.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SocketController {
    @RequestMapping("/web")
    public String hello( ){
        return "web";
    }
}
