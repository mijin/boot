package com.boot.demo.domain.orderingandbnsubscribing.request;
//    产品下附加包及包元素（资费，服务）
public class PackageElement {
    //        包编号
    private String packageId;
    //        元素编号
    private String elementId;
    //元素类型 D资费,S服务,A活动，X S服务
    private String elementType;
}