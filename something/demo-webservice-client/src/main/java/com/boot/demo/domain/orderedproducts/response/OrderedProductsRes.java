package com.boot.demo.domain.orderedproducts.response;

import com.boot.demo.domain.orderingandbnsubscribing.request.Para;
import lombok.Data;

@Data
public class OrderedProductsRes {
    private String msg;
//    产品编码
    private String productId;
//    产品名称
    private String productName;
//    产品简单描述
    private String productDesc;
//    产品属性M-主产品，A-附加产品，C-营销活动附加产品
    private String productType;
//    产品起效时间YYYMMDDHH24MISS
    private String startTime;
//    产品失效时间YYYMMDDHH24MISS
    private String endTime;
//    产品所属集团品牌，见附录
    private String productGrpBand;
//集团品牌描述
    private String groupBandDesc;
//    产品所属省分品牌，见附录
    private String provinceBand;
//省分品牌描述
    private String provinceBandDesc;
//    包编码
    private String packageId;
//    包名称
    private String packageName;
//    包的起效时间YYYMMDDHH24MISS
    private String packageStartTime;
//    包的失效时间YYYMMDDHH24MISS
    private String packageEndTime;
//    元素编码
    private String elementId;
//    元素名称
    private String elementName;
//    元素类别S-服务D-优惠
    private String elementType;
//    元素的起效时间YYYYMMDDHH24MISS
    private String elementStartTime;
//    元素的失效时间YYYYMMDDHH24MISS
    private String elementEndTime;
//    元素属性编码
    private String elementPropertyId;
//    元素属性名称
    private String elementPropertyName;
//    元素属性值
    private String elementPropertyValue;
//   保留字段
    private Para para;
}
