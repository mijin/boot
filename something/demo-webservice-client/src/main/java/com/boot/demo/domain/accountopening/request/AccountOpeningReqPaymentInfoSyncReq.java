package com.boot.demo.domain.accountopening.request;

public
class AccountOpeningReqPaymentInfoSyncReq {
    //        业务类型：01 营业支付信息同步；02 资源支付信息同步
    private String tradeType;
    //        CBSS系统业务订单号
    private String tradeId;
    //        mispos刷卡标识值为'MPOS'
    private String mposValueCode;
    //消费（BUSI_AMT 交易金额 单位：分，）；退货（REFUND_AMT 本次退款交易金额）
    private String tradeAmt;
    //        消费（交易类型 1:扫码；2:刷卡;3:分期;4:微信扫码;5:支付宝扫码;6:招联信用付）；退货（原始交易类型 1:扫码；2:刷卡;3:分期;4:微信扫码;5:支付宝扫码;6:招联信用付）
    private String originalTransType;
    //        消费，退货（TRANS_CARD_NO 交易卡号）
    private String transCardNo;
    //        原始交易总金额 单位：分
    private String originalBusiAmt;
    //        支付流水号（消费），退货流水号（退货）
    private String busiOrderId;
    //        接口类型（1：消费 2：退货 3：撤销 ）
    private String busiTransType;
    //        用户号码
    private String serialNumber;
    //0 成功（消费，退货，撤销 调用后前存），1失败 （消费，退货，撤销 调用后前存），2是待处理（消费，退货，撤销 调用前存）
    private String modifyTag;
    //        POS终端号 消费和退货
    private String transPoint;
    //        POS商户号 消费和退货
    private String transMerchant;
    //        POS流水号 消费和退货
    private String transTrace;
    //        POS批次号 消费和退货
    private String transBatch;
    //        POS系统检索号 消费和退货
    private String transRetrieval;
    //        交易时间 （支付公司返回）刷卡，重订购，退货
    private String transTime;
    //        退货时间（退货时填写)
    private String refundTime;
    //业务侧支付订单号(原始订单号) --退货时填
    private String originalBusiOrderId;
    //        支付订单 的交易状态：（消费 调用前）2是待处理 ，（消费 调用后） 0 ??功，1失败 ，（退货接口调用成功后修改）3 已退货 （撤销接口调用成功后修改）4 已撤销 （冲正接口调用后）5 已冲正
    private String transStatus;
}