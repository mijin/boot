package com.boot.demo.domain.servicechange.request;

public
class ServiceChangeReqMsg {
    //        操作员ID
    private String operatorId;
    //        省份
    private String province;
    //        地区
    private String city;
    //        区县
    private String district;
    //        渠道编码
    private String channelId;
    //        渠道类型
    private String channelType;
    //        渠道标示
    private String eModeCode;
    //        外围系统订单ID
    private String ordersId;
    //        手机号码
    private String serialNumber;
    //        在网时长
    private String inNetsdura;
    //办理业务系统： 1：ESS 2：CBSS
    private Integer opeSysType;
    //        业务标示 001：退出原群组
    private String serType;
    //        变更类型： 0：套餐间变更，如A主套餐变更为B主套餐 1：套餐内变更，如取消流量封顶 不传默认套餐间变更
    private String changeType;
    //        扣款标示 0：不扣款 1：扣款 该字段没传的时候默认扣款
    private String deductionTag;
    //       产品
    private ProductInfo productInfo;
    //        发展人标识
    private String recomPersonId;
    //        发展人名称
    private String recomPersonName;
}

