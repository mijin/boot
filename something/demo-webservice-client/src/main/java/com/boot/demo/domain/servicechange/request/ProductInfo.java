package com.boot.demo.domain.servicechange.request;

public class ProductInfo {
    //            产品ID
    private String productId;
    //产品企业编码
    private String companyId;
    //            产品别名
    private String productNameX;
    //            0：可选产品；1：主产品
    private String productMode;
    //            00：订购；01：退订；02：变更
    private String optType;
    private PackageElementPlus packageElement;


}
