package com.boot.demo.intfaces;

import com.boot.demo.domain.accountopening.request.AccountOpeningReq;
import com.boot.demo.domain.accountopening.response.AccountOpeningRes;
import com.boot.demo.domain.freeresourcequery.request.FreeResourceQueryReq;
import com.boot.demo.domain.freeresourcequery.response.FreeResourceQueryRes;
import com.boot.demo.domain.orderedproducts.request.OrderedProductsReq;
import com.boot.demo.domain.orderedproducts.response.OrderedProductsRes;
import com.boot.demo.domain.orderingandbnsubscribing.request.UpdateOrProductDto;
import com.boot.demo.domain.servicechange.request.ServiceChangeReq;
import com.boot.demo.domain.servicechange.response.ServiceChangeRes;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "siChuangFigen",url = "https://scaop.169ol.com:8302")
public interface SiChuangFigen {

    /**变更/流量包订购
     210001 - CBSS移动用户流量包订购及退订(1.0)
     * @param data
     * @return
     */
    @PostMapping("/ability/210001")
    @ResponseBody
    String orderingAndUnsubscribing(UpdateOrProductDto data);

    /**ecaop.trades.sell.mob.oldu.product.chg - 套餐服务变更(1.0)
     * @param data
     * @return
     */
    @PostMapping("/sc3rd-aop-service/ecaop.trades.sell.mob.oldu.product.chg")
    @ResponseBody
    ServiceChangeRes packageServiceChange (ServiceChangeReq data);

    /**CBSS开户处理提交接口
     * @param data
     * @return
     */
    @PostMapping("/sc3rd-aop-service/ecaop.trades.sell.mob.newu.open.sub")
    @ResponseBody
    AccountOpeningRes accountOpeningProcessingSubmissionInterface(AccountOpeningReq data);

    /**免费资源查询
     * @param data
     * @return
     */
    @PostMapping("/ability/10N007")
    @ResponseBody
    FreeResourceQueryRes freeResourceQuery(FreeResourceQueryReq data);

    /**100005-CBSS用户已订购产品查询
     * @param data
     * @return
     */
    @PostMapping("/ability/10N007")
    @ResponseBody
    OrderedProductsRes userOrderedProductsCheck(OrderedProductsReq data);


}
