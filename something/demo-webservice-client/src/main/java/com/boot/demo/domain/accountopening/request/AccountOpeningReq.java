package com.boot.demo.domain.accountopening.request;

import lombok.Data;

@Data
public class AccountOpeningReq {
    private AccountOpeningReqMsg msg;
}
