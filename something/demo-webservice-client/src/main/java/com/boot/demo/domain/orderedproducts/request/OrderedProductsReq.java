package com.boot.demo.domain.orderedproducts.request;

import lombok.Data;

@Data
public class OrderedProductsReq {
//    能力平台产品编码，固定为100005
    private String aopId;
//    号码(固网含区号)
    private String serviceId;
}
