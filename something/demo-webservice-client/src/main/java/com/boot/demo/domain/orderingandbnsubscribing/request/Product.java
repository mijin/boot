package com.boot.demo.domain.orderingandbnsubscribing.request;

import lombok.Data;

@Data
//    产品
public class Product {
    //        产品ID
    private String productId;
    //        00：订购；01：退订
    private String optType;
    //        生效方式 1：立即生效 2：次月生效
    private String enableTag;
}
