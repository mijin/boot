
package com.boot.demo.service.impl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.boot.demo.service.impl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Get_QNAME = new QName("http://impl.service.demo.boot.com/", "get");
    private final static QName _GetResponse_QNAME = new QName("http://impl.service.demo.boot.com/", "getResponse");
    private final static QName _ToUpperCase_QNAME = new QName("http://impl.service.demo.boot.com/", "toUpperCase");
    private final static QName _ToUpperCaseResponse_QNAME = new QName("http://impl.service.demo.boot.com/", "toUpperCaseResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.boot.demo.service.impl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link ToUpperCase }
     * 
     */
    public ToUpperCase createToUpperCase() {
        return new ToUpperCase();
    }

    /**
     * Create an instance of {@link ToUpperCaseResponse }
     * 
     */
    public ToUpperCaseResponse createToUpperCaseResponse() {
        return new ToUpperCaseResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.service.demo.boot.com/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.service.demo.boot.com/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ToUpperCase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.service.demo.boot.com/", name = "toUpperCase")
    public JAXBElement<ToUpperCase> createToUpperCase(ToUpperCase value) {
        return new JAXBElement<ToUpperCase>(_ToUpperCase_QNAME, ToUpperCase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ToUpperCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.service.demo.boot.com/", name = "toUpperCaseResponse")
    public JAXBElement<ToUpperCaseResponse> createToUpperCaseResponse(ToUpperCaseResponse value) {
        return new JAXBElement<ToUpperCaseResponse>(_ToUpperCaseResponse_QNAME, ToUpperCaseResponse.class, null, value);
    }

}
