package com.boot.demo.domain;

import lombok.Data;

@Data
public class ReturnApi<T> {
    //    系统状态码,0000标识成功， 其他均为失败。
    private String code;
//    业务接口回执内容，各业务接口定义
    private T rep;
    private String prea;
    private String preb;
    private String prec;
    private String serialnumber;
    private String detail;
    private String busiCode;
    private String busiDetail;
}
