package com.boot.demo.domain.accountopening.request;

import lombok.Data;

@Data
public  class AccountOpeningReqPayInfo {
    //        支付方式 参考附录支付方式说明 现金，不填写支付机构和支付账号
    private String payType;
    //        支付机构名称
    private String payOrg;
    //        支付账号
    private String payNum;
    //        支付金额，单位：厘
    private String payFee;
    //        MISPOS支付信息（支付方式为MISPOS时该节点必传）
    private AccountOpeningReqPaymentInfoSyncReq paymentInfoSyncReq;

}
