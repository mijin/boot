package com.boot.demo.domain.freeresourcequery.response;

import com.boot.demo.domain.orderingandbnsubscribing.request.Para;
import lombok.Data;

@Data
public class FreeResourceQueryRes {
    //    套餐余量查询明细信息
    private FreeResourceQueryResInfo FEE_POLICY_ADDUP_INFO;

}
