package com.boot.demo.domain.orderingandbnsubscribing.request;

import lombok.Data;

/**
 * 变更/流量包订购
 * 请求报文
 * aopId	req	Y	String	能力平台产品编码，固定为200002
 * serviceId	req	Y	String	移动号码
 * extOrderId	req	Y	String	外围系统订单ID
 * productInfo	req	*		产品
 * productId	productInfo	Y	String(20)	产品ID
 * optType	productInfo	N	String(2)	00：订购；01：退订
 * packageId	packageElement	Y	String(20)	包编号
 * elementId	packageElement	Y	String(20)	元素编号
 * elementType	packageElement	Y	String(2)	元素类型 D资费,S服务,A活动，X S服务
 * enableTag	productInfo	Y	String(1)	生效方式 1：立即生效 2：次月生效
 * recomPersonId	req	N	String(30)	发展人标识
 * recomPersonName	req	N	String(30)	发展人名称
 * para	req	*		保留字段
 * paraId	para	Y	String(20)	保留字段ID
 * paraValue	para	Y	String(60)	保留字段值
 */
@Data
public class UpdateOrProductDto {
    //    能力平台产品编码，固定为200002
    private String aopId;
    //    移动号码
    private String serviceId;
    //    外围系统订单ID
    private String extOrderId;
    //    产品
    private Product product;
    //    发展人标识
    private String recomPersonId;
    //    发展人名称
    private String recomPersonName;
    //    保留字段
    private Para para;
}
