package com.boot.demo.domain.servicechange.request;

public //    服务资费属性（数据下发到tf_b_trade_sub_item表或者TRADE_SUB_ITEM 中）
class ServiceAttr {
    //        关键字
    private String code;
    //        值
    private String value;
}
