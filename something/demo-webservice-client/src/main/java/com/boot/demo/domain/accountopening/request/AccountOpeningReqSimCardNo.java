package com.boot.demo.domain.accountopening.request;

import lombok.Data;

@Data
public //        卡信息资料
class AccountOpeningReqSimCardNo {
    //        获取写卡数据业务流水
    private String cardDataProcId;
    //        如果是成卡是USIM卡号，如果是白卡是ICCID
    private String simId;
    //        新IMSI号
    private String imsi;
    //        白卡类型 参考附录白卡类型说明
    private String cardType;
    //        白卡数据
    private String cardData;
    //        卡费，单位：厘
    private String cardFee;
}