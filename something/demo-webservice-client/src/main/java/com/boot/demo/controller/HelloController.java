package com.boot.demo.controller;

import com.boot.demo.domain.ReturnApi;
import com.boot.demo.domain.freeresourcequery.request.FreeResourceQueryReq;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloController {

    @RequestMapping("/index")
    public Object index(@RequestBody FreeResourceQueryReq data, String token, HttpServletRequest request ) {
        ReturnApi<FreeResourceQueryReq> res = new ReturnApi<>();
        res.setRep(data);
        res.setCode("0000");
        res.setPrea(token);
        return res;
    }
}
