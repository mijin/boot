package com.boot.demo.domain.accountopening.request;

import com.boot.demo.domain.accountopening.request.AccountOpeningReqPayInfo;
import com.boot.demo.domain.accountopening.request.AccountOpeningReqSimCardNo;
import com.boot.demo.domain.accountopening.request.AccountOpeningReqSimCardNoFeeInfo;
import com.boot.demo.domain.orderingandbnsubscribing.request.Para;
import lombok.Data;

@Data
public class AccountOpeningReqMsg {
    //        操作员id
    private String operatorId;
    //        省份
    private String province;
    //        地市
    private String city;
    //        区县
    private String district;
    //        渠道编码
    private String channelId;
    //        渠道类型
    private String channelType;
    //        ESS订单交易流水 为正式提交时使用
    private String provOrderId;
    //外围系统订单ID
    private String ordersId;
    //     办理业务系统： 1：ESS 2：CBSS
    private Integer opeSysType;
    //订单来源： 00：集团电商 01：华盛天猫 02：百度糯米 03：乐视商城 04：华为商城 05：小米商城 06：北分卖场
    private String netType;
    //        卡信息资料
    private AccountOpeningReqSimCardNo simCardNo;
    //        收费信息*（ESS从BSS获取到的）
    private AccountOpeningReqSimCardNoFeeInfo feeInfo;
    //        总费用正整数，单位：厘
    private String origTotalFee;
    //发票号码
    private String invoiceNo;
    //        客户支付信息
    private AccountOpeningReqPayInfo payInfo;
    //        受理单要求标记 0：不要求受理单 1：要求受理单
    private String acceptanceReqTag;
    //        保留字段
    private Para para;
}
