
package com.boot.demo.domain.accountopening.response;

import com.boot.demo.domain.orderingandbnsubscribing.request.Para;

public class AccountOpeningRes {
//    ESS订单交易流水 为正式提交时使用
    private String provOrderId;
//    BSS订单ID
    private String bssOrderId;
//ESS总部订单ID
    private String essOrderId;
//税控码
    private String taxNo;
//    受理单模板编码
    private String acceptanceTp;
//    0：套打；1：白打
    private String acceptanceMode;
//    受理单打印内容
    private String acceptanceForm;
//    保留字段
    private Para para;
}
