package com.boot.demo.domain.servicechange.response;

public //    收费信息*（ESS从BSS获取到的）
class ServiceChangeResFeeInfo {
    //        收费项编码，以省分现有编码为准
    private String feeId;
    //        收费项科目
    private String feeCategory;
    //        收费项描述
    private String feeDes;
    //        最大减免金额正整数单位：厘
    private String maxRelief;
    //        应收费用正整数单位：厘
    private String origFee;
}
