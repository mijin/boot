package com.boot.demo.domain.accountopening.request;

import lombok.Data;

@Data
public
        //收费信息*（ESS从BSS获取到的）
class AccountOpeningReqSimCardNoFeeInfo {
    //        收费项编码，以省分现有编码为准
    private String feeId;
    //        收费项科目
    private String feeCategory;
    //        收费项描述
    private String feeDes;
    //        应收费用正整数，单位：厘
    private String origFee;
    //        减免金额，单位：厘
    private String reliefFee;
    //        减免原因
    private String reliefResult;
    //        实收金额，单位：厘
    private String realFee;
    //        收费方式 0：未支付 1：已支付
    private String payTag;
}