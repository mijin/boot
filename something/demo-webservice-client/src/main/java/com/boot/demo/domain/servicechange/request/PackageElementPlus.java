package com.boot.demo.domain.servicechange.request;

import com.boot.demo.domain.orderingandbnsubscribing.request.PackageElement;

public class PackageElementPlus extends PackageElement {
    //        0：订购；1：退订；
    private String modType;
    //        服务资费属性（数据下发到tf_b_trade_sub_item表或者TRADE_SUB_ITEM 中
    private ServiceAttr serviceAttr;
}
