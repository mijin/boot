package com.boot.demo.domain.freeresourcequery.response;

import lombok.Data;

@Data
public class FreeResourceQueryResInfo {
    //        免费资源名称
    private String ADDUP_ITEM_NAME;
    //        累计值上限
    private String ADDUP_UPPER;
    //        账期
    private String CYCLE_ID;
    //        累积量单位（0：金额（分）1:时长（分钟）2：短信条数（条）3：流量（KB）4:M个数 5: T个数）
    private String ELEM_TYPE;
    //套餐标识
    private String FEE_POLICY_ID;
    //        套餐名称
    private String FEE_POLICY_NAME;
    //        资源来源标志0当月套餐1结转资源
    private String RESOURCE_SOURCE;
    /**
     * 资源类型（01：普通上网流量；13：定向流量；18：国际漫游流量；20：闲时流量；21：订业务送手机省内流量；23：新春红包；24：全国日租型流量包；
     * 25：省内日租型流量包；26：全国节假日流量包；27：省内节假日流量包；28：省内流量月包（部分低消活动也是这个）；
     * 29：本地流量月包；30：全国流量月包；31：赠送包；46：流量日租宝（使用量分条展示）；47：米粉卡返还流量类型（账管根据营业传的参数31000013值展示）；
     * 48：语音日包（日累积不循环）；49：流量日包（日累积不循环）；S0：B2I套内不限量流量；S1：B2I套外放心用流量分条展示；
     * I0：流量不限量套餐或包（20170214冰激凌套餐需求新增）；I2：不限流量省内流量包（20170925新增）；
     * I3：不限流量国内流量包（20170925新增）；I4：1元XM3元不限量日租宝（20180524百度天圣卡3元不限量资费余量查询优化方案需求新增）；
     * M0：包天不限量（米粉卡新增）；02：普通通话语音；19：国际漫游语音；I1：语音不限量套餐或包（20170214冰激凌套餐需求新增）；
     * 54：跳档资费（免费段收费段交叉关联TD_B_CALC_COMMPARA a where para_code ='ASM_PARAM_TD';）；52：全国小时包；53：省内小时包；
     * 48：全国多日包；49：省内多日包；50：全国半年包；51：省内半年包；33：省分个性化包（所有省分未覆盖的都放这里）；34：全国闲时包；
     * 35：省内闲时包；36：全国加油包；37：省内加油包；38：全国周末包；39：省内周末包；40：全国小区包；41：省内小区包；
     * 42：全国单模包；43：省内单模包；44：全国赠送（免费）包；45：省内赠送（免费）包；D0：省内腾讯视频定向限量流量包；
     * D1：国内腾讯视频定向限量流量包；D2：省内PPTV视频定向限量流量包；D3：国内PPTV视频定向限量流量包；D4：省内乐视视频定向限量流量包；
     * D5：国内乐视视频定向限量流量包；A1：2G套餐流量；）
     */
    private String RESOURCE_TYPE;
    //        个人或共享查询
    private String TYPE_MARK;
    //        用户号码
    private String USER_NUMBER;
    //        剩余免费资源量
    private String X_CANUSE_VALUE;
    //        超出部分使用量
    private String X_EXCEED_VALUE;
    //        已经使用的免费资源量
    private String X_USED_VALUE;
    //        备用节点
    private String PARA;
}

