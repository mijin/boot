package com.boot.demo.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.14
 * 2020-11-05T15:28:41.929+08:00
 * Generated source version: 3.2.14
 *
 */
@WebServiceClient(name = "GetServiceImplService",
                  wsdlLocation = "http://127.0.0.1:12345/get?wsdl",
                  targetNamespace = "http://impl.service.demo.boot.com/")
public class GetServiceImplService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://impl.service.demo.boot.com/", "GetServiceImplService");
    public final static QName GetServiceImplPort = new QName("http://impl.service.demo.boot.com/", "GetServiceImplPort");
    static {
        URL url = null;
        try {
            url = new URL("http://127.0.0.1:12345/get?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(GetServiceImplService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://127.0.0.1:12345/get?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public GetServiceImplService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public GetServiceImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public GetServiceImplService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public GetServiceImplService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public GetServiceImplService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public GetServiceImplService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns GetServiceImpl
     */
    @WebEndpoint(name = "GetServiceImplPort")
    public GetServiceImpl getGetServiceImplPort() {
        return super.getPort(GetServiceImplPort, GetServiceImpl.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GetServiceImpl
     */
    @WebEndpoint(name = "GetServiceImplPort")
    public GetServiceImpl getGetServiceImplPort(WebServiceFeature... features) {
        return super.getPort(GetServiceImplPort, GetServiceImpl.class, features);
    }

}
