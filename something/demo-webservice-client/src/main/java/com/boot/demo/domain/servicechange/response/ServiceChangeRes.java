package com.boot.demo.domain.servicechange.response;

import com.boot.demo.domain.orderingandbnsubscribing.request.Para;

/**
 * 餐服务变更(1.0) 回执报文
 */
public class ServiceChangeRes {
    //    BSS订单ID
    private String bssOrderId;
    //    收费信息*（ESS从BSS获取到的）
    private ServiceChangeResFeeInfo feeInfo;
    //        总费用正整数单位：厘
    private String totalFee;
    //    保留字段
    private Para para;

}
