package com.boot.demo.intfaces;

import com.boot.demo.DemoApplicationTests;
import com.boot.demo.domain.orderingandbnsubscribing.request.Product;
import com.boot.demo.domain.orderingandbnsubscribing.request.UpdateOrProductDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class SiChuangFigenTest extends DemoApplicationTests {

    @Autowired
    private SiChuangFigen siChuangFigen;

    @Test
    void orderingAndUnsubscribing() {
        UpdateOrProductDto data = new UpdateOrProductDto();
        data.setAopId("2000002");
        data.setServiceId("13289898989");
        data.setExtOrderId("789789");

        Product product = new Product();
        product.setProductId("963");
        product.setOptType("00");

         siChuangFigen.orderingAndUnsubscribing(data);

    }

    @Test
    void packageServiceChange() {
    }

    @Test
    void accountOpeningProcessingSubmissionInterface() {
    }

    @Test
    void freeResourceQuery() {
    }

    @Test
    void userOrderedProductsCheck() {
    }
}