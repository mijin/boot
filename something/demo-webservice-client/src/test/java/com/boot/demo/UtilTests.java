package com.boot.demo;

import cn.com.webxml.ArrayOfString;
import cn.com.webxml.WeatherWebService;
import cn.com.webxml.WeatherWebServiceSoap;
import com.boot.demo.service.impl.GetServiceImpl;
import com.boot.demo.service.impl.GetServiceImplService;
import org.junit.jupiter.api.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

class UtilTests {

    @Test
    void contextLoads() {
        //创建服务视图，视图是从wsdl文件的service标签的name属性获取   <service>是整个webservice的服务视图，它包括了所有的服务端点
        GetServiceImplService getServiceImplService = new GetServiceImplService();
        //获取服务实现类，实现类从wsdl文件的portType的name属性获取    <portType>描述webservice可被执行的操作，以及相关的消息，通过binding指向portType
        GetServiceImpl port = getServiceImplService.getPort(GetServiceImpl.class);
        //获取查询方法，从portType的operation标签获取
        String ab = port.get("Ab");
        System.out.println(ab);
        String ab1 = port.toUpperCase("Ab");
        System.out.println(ab1);
    }


    /**
     * 该种方式使用简单，但一些关键的元素在代码生成时写死在生成代码中，不方便维护
     */
    @Test
    void test1() throws MalformedURLException {
        //创建WSDL文件的URL
        URL wsdlDocumentLocation = new URL("http://127.0.0.1:12345/get?wsdl");
        //创建服务名称
        //1.namespaceURI - 命名空间地址
        //2.localPart - 服务视图名
        QName serviceName = new QName("http://impl.service.demo.boot.com/", "GetServiceImplService");
        Service service = Service.create(wsdlDocumentLocation, serviceName);

//		//获取服务实现类
        GetServiceImpl port = service.getPort(GetServiceImpl.class);
//		//调用方法
        String ab = port.get("Ab");
        System.out.println(ab);
        String ab1 = port.toUpperCase("Ab");
        System.out.println(ab1);
    }

    /**
     * 第一步：创建服务地址
     * <p>
     * 第二步：打开一个通向服务地址的连接
     * <p>
     * 第三步：设置参数
     * <p>
     * 第四步：组织SOAP数据，发送请求
     * <p>
     * 第五步：接收服务端响应
     */
    @Test
    void test2() throws IOException {
        //第一步：创建服务地址，不是WSDL地址
        URL url = new URL("http://ws.webxml.com.cn/WebServices/MobileCodeWS.asmx");
        //第二步：打开一个通向服务地址的连接
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        //第三步：设置参数
        //3.1发送方式设置：POST必须大写
        connection.setRequestMethod("POST");
        //3.2设置数据格式：content-type
        connection.setRequestProperty("content-type", "text/xml;charset=UTF-8");
        //3.3设置输入输出，因为默认新创建的connection没有读写权限，
        connection.setDoInput(true);
        connection.setDoOutput(true);

        //第四步：组织SOAP数据，发送请求
        String soapXML = getXML("13986862569");
        OutputStream os = connection.getOutputStream();
        os.write(soapXML.getBytes());
        //第五步：接收服务端响应，打印
        int responseCode = connection.getResponseCode();
        if (200 == responseCode) {//表示服务端响应成功
            InputStream is = connection.getInputStream();
            //将字节流转换为字符流
            InputStreamReader isr = new InputStreamReader(is, "utf-8");
            //使用缓存区
            BufferedReader br = new BufferedReader(isr);

            StringBuilder sb = new StringBuilder();
            String temp = null;
            while (null != (temp = br.readLine())) {
                sb.append(temp);
            }
            System.out.println(sb.toString());

            is.close();
            isr.close();
            br.close();
        }
        os.close();
    }

    private String getXML(String phoneNum) {
        String soapXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<soap:Body>"
                + "<getMobileCodeInfo xmlns=\"http://WebXml.com.cn/\">"
                + "<mobileCode>" + phoneNum + "</mobileCode>"
                + "<userID></userID>"
                + "</getMobileCodeInfo>"
                + "</soap:Body>"
                + "</soap:Envelope>";
        return soapXML;
    }


    @Test
    void test() {
        WeatherWebService weatherWebService = new WeatherWebService();

        WeatherWebServiceSoap port = weatherWebService.getPort(WeatherWebServiceSoap.class);

        ArrayOfString array = port.getWeatherbyCityName("北京");
        List<String> list = array.getString();
        for (int index = 0; index < list.size(); index++) {
            System.out.println(index + ": " + list.get(index));

        }
    }

//    @Test
//    void test3() {
//        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
//        // 设置代理地址
//        jaxWsProxyFactoryBean.setAddress("http://127.0.0.1:12345/get?wsdl");
//        // 设置接口类型
//        jaxWsProxyFactoryBean.setServiceClass(GetServiceImpl.class);
//        // 创建一个代理接口实现
//        GetServiceImpl weatherWebServiceSoap = (GetServiceImpl) jaxWsProxyFactoryBean.create();
//        String as = weatherWebServiceSoap.toUpperCase("Ab");
//        System.out.println(as);
//    }

}
