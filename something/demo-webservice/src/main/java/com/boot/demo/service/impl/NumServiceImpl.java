package com.boot.demo.service.impl;

import com.boot.demo.service.INumService;

import javax.jws.WebService;

@WebService
public class NumServiceImpl implements INumService {
    @Override
    public Integer add(Integer min) {
        return min++;
    }
}
