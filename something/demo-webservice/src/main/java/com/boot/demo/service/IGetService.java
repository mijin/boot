package com.boot.demo.service;

public interface IGetService {
    String toLowerCase(String arg);
    Integer toLowerCaseNum(String arg, Integer index);
    String toUpperCase(String arg);
}
