package com.boot.demo.service.impl;

import com.boot.demo.service.IGetService;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(serviceName = "StringToDo")
public class GetServiceImpl implements IGetService {
    @WebMethod
    @Override
    public String toLowerCase(String arg) {
        if (arg == null) {
            return null;
        } else {
            return arg.toLowerCase();
        }
    }
    @WebMethod
    @Override
    public Integer toLowerCaseNum(String arg, Integer index) {
        return index;
    }
    @WebMethod
    @Override
    public String toUpperCase(String arg) {
        return arg.toUpperCase();
    }
}
