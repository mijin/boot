package com.boot.demo;

import com.boot.demo.service.impl.GetServiceImpl;

import javax.xml.ws.Endpoint;

public class UtilsTests {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Endpoint.publish("http://localhost:12345/get", new GetServiceImpl());
        System.out.println(String.format("启动成花费时间为%sms", System.currentTimeMillis()-start));
    }

}
